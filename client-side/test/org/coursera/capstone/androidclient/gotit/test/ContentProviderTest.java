package org.coursera.capstone.androidclient.gotit.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.coursera.capstone.androidclient.gotit.repository.Contracts;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.URI;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbCheckIn;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbFeedback;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbFeedbackShared;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbQuestion;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbUser;
import org.coursera.capstone.androidclient.gotit.repository.GotItBaseColumns;
import org.coursera.capstone.androidclient.gotit.repository.GotItContentProvider;
import org.coursera.capstone.androidclient.gotit.repository.GotItRepository;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.FeedbackType;
import org.coursera.capstone.androidclient.gotit.rest.QuestionEnterType;
import org.coursera.capstone.androidclient.gotit.rest.QuestionGeneratedFeedbackMode;
import org.coursera.capstone.androidclient.gotit.rest.SecuredRestException;
import org.coursera.capstone.androidclient.gotit.rest.User;
import org.coursera.capstone.androidclient.gotit.rest.UserDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowContentResolver;

import android.accounts.Account;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import static org.junit.Assert.*;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.*;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.*;

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk = 16)
public class ContentProviderTest {

    private ContentResolver contentResolver;
    private ContentProvider contentProvider;
    private ShadowContentResolver shadowContentResolver;

    @Before
    public void setup() {
        contentResolver = Robolectric.application.getContentResolver();
        contentProvider = new GotItContentProvider();
        shadowContentResolver = Robolectric.shadowOf(contentResolver);
        contentProvider.onCreate();
        ShadowContentResolver.registerProvider(Contracts.URI.AUTHORITY, contentProvider);
    }

    @Test
    public void testig_repository() throws SecuredRestException, RemoteException, IOException {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, buildPathOneItem(true, ANY_STRING, ANY_NUMBER), ONE);
        
        System.out.println(Uri.parse(buildPathOneItem(false, USER_PATH, URI.DEFAULT_ID)));
        System.out.println(Uri.parse(buildPathOneItem(false, USER_PATH, ""+1)));
        // %s/general/*/one/#
        assertNotEquals(UriMatcher.NO_MATCH,
                matcher.match(Uri.parse(buildPathOneItem(false, USER_PATH, URI.DEFAULT_ID))));
        assertNotEquals(UriMatcher.NO_MATCH,
                matcher.match(Uri.parse(buildPathOneItem(false, USER_PATH, ""+1))));
       

       

        /*Context ctx = Robolectric.application;
        UserDetails ud = new UserDetails();
        ud.setFirstName("aaa");
        ud.setLastName("aaa");
        User user = new User(false, "aaaa", "aaa", ud);
        user.setModificationDate(new Date(0));

        Account account = new Account("aaaa", "aaa");
        GotItRepository rep = new GotItRepository(ctx, account,
                shadowContentResolver.acquireContentProviderClient(AUTHORITY));
        rep.saveSignedUserLocally(user);

        user = rep.getLocallySavedUser(user.getUserName());
        System.out.println(user);*/

    }

    @Test
    public void testing_adding_and_retrieving_a_user() {

        // insert
        ContentValues user1 = buildUser(true);
        Uri requestUri1 = Uri.parse(buildPathOneItem(false, USER_PATH, URI.DEFAULT_ID));
        Uri resultUri1 = shadowContentResolver.insert(requestUri1, user1);
        int user1IdTeen = URI.getIdFromPath(resultUri1);

        // read
        Uri uri = Uri.parse(URI.buildPathManyItems(false, TbUser.TB_USER));
        Cursor cursor = shadowContentResolver.query(uri, null, TbUser.USER_NAME + "=?",
                new String[] { user1.getAsString(TbUser.USER_NAME) }, null);
        assertTrue(cursor != null);
        cursor.close();

        // read
        cursor = shadowContentResolver.query(resultUri1, null, null, null, null);
        System.out.println(resultUri1);
        System.out.println();
        assertTrue(cursor != null);
        assertEquals(1, cursor.getCount());
        if (cursor.moveToFirst()) {
            assertEquals(1, cursor.getLong(cursor.getColumnIndex(TbUser._ID)));
            assertEquals(user1.get(TbUser.USER_NAME), cursor.getString(cursor.getColumnIndex(TbUser.USER_NAME)));
            assertEquals("Doe", cursor.getString(cursor.getColumnIndex(TbUser.LAST_NAME)));
            assertEquals(1, cursor.getInt(cursor.getColumnIndex(TbUser.IS_TEEN)));
        }
        cursor.close();

        // update
        user1.put(TbUser.MRN, 1234);
        int updated = shadowContentResolver.update(resultUri1, user1, null, null);
        assertNotEquals(-1, updated);
        cursor = shadowContentResolver.query(resultUri1, null, null, null, null);
        cursor.moveToFirst();
        assertEquals(1234, cursor.getInt(cursor.getColumnIndex(TbUser.MRN)));
        cursor.close();

        String id = URI.getStringIdFromPath(resultUri1);
        String table = URI.getTableNameFromPath(resultUri1);
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathOneByCreationDate(false, table, id)), null, null,
                null, null);
        assertEquals(1, cursor.getColumnCount());
        cursor.moveToFirst();
        System.out.println(cursor.getLong(cursor.getColumnIndex(TbUser.CREATION_DATE)));
        cursor.close();

        // create a follower
        ContentValues user2 = buildUser(false);
        Uri resultUri2 = shadowContentResolver.insert(Uri.parse(buildPathOneItem(false, USER_PATH, URI.DEFAULT_ID)),
                user2);
        int user2IdFollower = URI.getIdFromPath(resultUri2);
        assertEquals(2, user2IdFollower);
        // follow user1
        Uri resultUriFollower2 = shadowContentResolver
                .insert(Uri.parse(URI.buildPathOneFollower(false, "" + user1IdTeen, "" + user2IdFollower)),
                        new ContentValues());
        assertEquals(user2IdFollower, URI.getIdFromPath(resultUriFollower2));

        // create a follower user3 and follow user1
        ContentValues user3 = buildUser(false);
        Uri resultUri3 = shadowContentResolver.insert(Uri.parse(buildPathOneItem(false, USER_PATH, URI.DEFAULT_ID)),
                user3);
        int user3IdFollower = URI.getIdFromPath(resultUri3);
        Uri resultUriFollower3 = shadowContentResolver
                .insert(Uri.parse(URI.buildPathOneFollower(false, "" + user1IdTeen, "" + user3IdFollower)),
                        new ContentValues());
        assertEquals(user3IdFollower, URI.getIdFromPath(resultUriFollower3));

        // get followers
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathManyFollowers(false, "" + user1IdTeen)), null,
                null, null, null);
        assertTrue(cursor != null);
        cursor.moveToFirst();
        assertEquals(user2IdFollower, cursor.getLong(cursor.getColumnIndex(TbFollower.FOLLOWER_ID)));
        assertEquals(0, cursor.getLong(cursor.getColumnIndex(TbFollower.CONFIRMED)));
        cursor.close();

        // get followed for user2
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathManyFollowedTeens(false, "" + user2IdFollower)),
                null, null, null, null);
        assertTrue(cursor != null);
        cursor.moveToFirst();
        assertEquals(user1IdTeen, cursor.getLong(cursor.getColumnIndex(TbFollower.TEEN_ID)));
        cursor.close();

        // get followed for user3
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathManyFollowedTeens(false, "" + user3IdFollower)),
                null, null, null, null);
        assertTrue(cursor != null);
        cursor.moveToFirst();
        assertEquals(user1IdTeen, cursor.getLong(cursor.getColumnIndex(TbFollower.TEEN_ID)));
        cursor.close();

        // getting all users
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathManyItems(false, USER_PATH)), new String[] {
                TbUser.USER_NAME, TbUser.CREATION_DATE }, null, null, null);
        assertTrue(cursor != null);
        assertEquals(2, cursor.getColumnCount());
        assertEquals(3, cursor.getCount());// 1 teen and 2 followers

        // adding a question
        ContentValues question = buildQuestion();
        Uri resultQUri = shadowContentResolver.insert(
                Uri.parse(buildPathOneItem(false, TbQuestion.TB_QUESTION, DEFAULT_ID)), question);
        System.out.println(resultQUri);
        String questionId = getStringIdFromPath(resultQUri);
        // assertTrue(question.get(TbQuestion._ID).equals(questionId));

        // adding a checkin
        ContentValues checkin = buildCheckIn(user1IdTeen);
        Uri resultChkUri = shadowContentResolver.insert(
                Uri.parse(buildPathOneItem(false, TbCheckIn.TB_CHECKIN, DEFAULT_ID)), checkin);
        System.out.println(resultChkUri);
        String checkInId = getStringIdFromPath(resultChkUri);

        // share checkin
        int resultShareChk = shadowContentResolver.bulkInsert(
                Uri.parse(buildPathOneItem(false, TbCheckInShared.TB_CHECKIN_SHARED, DEFAULT_ID)),
                buildShared(TbCheckInShared.CHECKIN_ID, checkInId, new String[] { "" + user2IdFollower,
                        "" + user3IdFollower }));
        assertEquals(2, resultShareChk);

        // get user1 checkins from user2 -follower perspective
        cursor = shadowContentResolver.query(
                Uri.parse(URI.buildPathManyCheckIns(false, "" + user1IdTeen, "" + user2IdFollower)), null, null, null,
                null);
        assertTrue(cursor != null);
        cursor.moveToFirst();
        assertEquals(checkin.get(TbCheckIn.TEEN_ID), cursor.getInt(cursor.getColumnIndex(TbCheckIn.TEEN_ID)));
        assertEquals(1, cursor.getCount());
        assertEquals(checkin.get(TbCheckInShared.TEEN_ID), user1IdTeen);

        // get user1 checkins from user3 -follower perspective
        cursor = shadowContentResolver.query(
                Uri.parse(URI.buildPathManyCheckIns(false, "" + user1IdTeen, "" + user3IdFollower)), null, null, null,
                null);
        assertTrue(cursor != null);
        cursor.moveToFirst();
        assertEquals(checkin.get(TbCheckIn.TEEN_ID), cursor.getInt(cursor.getColumnIndex(TbCheckIn.TEEN_ID)));
        assertEquals(1, cursor.getCount());
        cursor.close();

        // getting checkins from user1 - teen perpective;
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathManyCheckIns(false, "" + user1IdTeen, DEFAULT_ID)),
                null, null, null, null);
        assertTrue(cursor != null);
        cursor.moveToFirst();
        assertEquals(checkin.get(TbCheckIn.TEEN_ID), cursor.getInt(cursor.getColumnIndex(TbCheckIn.TEEN_ID)));
        assertEquals(1, cursor.getCount());
        cursor.close();

        // adding feedback1 for checkin
        ContentValues feedback = buildFeedback(checkInId, questionId);
        Uri feedbackUri = shadowContentResolver.insert(
                Uri.parse(URI.buildPathOneItem(false, TbFeedback.TB_FEEDBACK, DEFAULT_ID)), feedback);
        String feedbackId = getStringIdFromPath(feedbackUri);
        // share feedback
        int feedbackShared = shadowContentResolver.bulkInsert(
                Uri.parse(buildPathOneItem(false, TbFeedbackShared.TB_FEEDBACK_SHARED, DEFAULT_ID)),
                buildShared(TbFeedbackShared.FEEDBACK_ID, feedbackId, new String[] { "" + user2IdFollower,
                        "" + user3IdFollower }));
        assertEquals(2, feedbackShared);

        // getting feedback from user2 follower perspective
        cursor = shadowContentResolver.query(
                Uri.parse(URI.buildPathManyFeedback(false, feedbackId, "" + user2IdFollower)), null, null, null, null);
        assertTrue(cursor != null);
        assertEquals(1, cursor.getCount());
        cursor.moveToFirst();
        assertEquals(question.get(TbQuestion.BODY), cursor.getString(cursor.getColumnIndex(TbQuestion.BODY)));
        assertEquals(feedback.get(TbFeedback.ANSWER), cursor.getString(cursor.getColumnIndex(TbFeedback.ANSWER)));
        assertEquals(feedback.get(TbFeedback.TYPE), cursor.getString(cursor.getColumnIndex(TbFeedback.TYPE)));
        cursor.close();

        // /getting their own feedback from user1 teen perspective
        cursor = shadowContentResolver.query(Uri.parse(URI.buildPathManyFeedback(false, feedbackId, DEFAULT_ID)), null,
                null, null, null);
        assertTrue(cursor != null);
        assertEquals(1, cursor.getCount());
        cursor.moveToFirst();
        assertEquals(question.get(TbQuestion.BODY), cursor.getString(cursor.getColumnIndex(TbQuestion.BODY)));
        cursor.close();
    }

    private static ContentValues buildUser(boolean isTeen) {
        ContentValues cv = new ContentValues();
        cv.put(TbUser.CREATION_DATE, new Date().getTime());
        cv.put(TbUser.USER_NAME, UUID.randomUUID().toString());
        cv.put(TbUser.PASSWORD, 1234);
        cv.put(TbUser.IS_TEEN, isTeen);
        cv.put(TbUser.BIRTH_DATE, new Date(0).getTime());
        cv.put(TbUser.MRN, 123456);
        cv.put(TbUser.FIRST_NAME, "John");
        cv.put(TbUser.LAST_NAME, "Doe");
        return cv;
    }

    public static ContentValues buildCheckIn(int teenId) {
        ContentValues cv = new ContentValues();
        cv.put(TbCheckIn.CREATION_DATE, new Date().getTime());
        cv.put(TbCheckIn.TEEN_ID, teenId);
        return cv;
    }

    public static ContentValues buildQuestion() {
        ContentValues cv = new ContentValues();
        cv.put(TbQuestion._ID, 2);
        cv.put(TbQuestion.BODY, "What was your glycemic value?");
        cv.put(TbQuestion.ENTERABLE, true);
        cv.put(TbQuestion.ENTER_TYPE, QuestionEnterType.INT.toString());
        cv.put(TbQuestion.GEN_FEEDBACK_MODE, QuestionGeneratedFeedbackMode.MANUAL.toString());
        return cv;
    }

    public static ContentValues[] buildShared(String idKey, String id, String[] follower_ids) {
        List<ContentValues> cvs = new ArrayList<>();
        for (String fi : follower_ids) {
            ContentValues cv = new ContentValues();
            cv.put(idKey, id);
            cv.put(GotItBaseColumns.FOLLOWER_ID, fi);
            cvs.add(cv);
        }
        return cvs.toArray(new ContentValues[cvs.size()]);
    }

    public static ContentValues buildFeedback(String checkinId, String questionID) {
        ContentValues cv = new ContentValues();
        cv.put(TbFeedback.CHECKIN_ID, checkinId);
        cv.put(TbFeedback.QUESTION_ID, questionID);
        cv.put(TbFeedback.ANSWER, UUID.randomUUID().toString());
        cv.put(TbFeedback.TYPE, FeedbackType.POSITIVE.toString());
        return cv;
    }
}
