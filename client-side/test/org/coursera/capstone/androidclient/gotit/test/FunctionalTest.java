package org.coursera.capstone.androidclient.gotit.test;

import org.coursera.capstone.androidclient.gotit.rest.GotItConnection;
import org.coursera.capstone.androidclient.gotit.rest.GotItURLMapping;
import org.junit.Test;

import static org.junit.Assert.*;

public class FunctionalTest {

   
    @Test 
    public void test_stringify_url(){
        String url =  GotItURLMapping.SIGN_UP_PATH + "avatar/uzzrrr/";
        String path = GotItConnection.stringifyPath(GotItURLMapping.AVATAR_PATH, new String[]{url});
        System.out.println(path);
    }

}
