package org.coursera.capstone.androidclient.gotit.gift;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.rest.Gift;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class GiftActivity extends BaseActivity implements GiftView, OnItemClickListener {

    private GiftPresenter presenter;
    private ListView listGifts;
    private static final int ACTIVITY_ID = 2000;

    public GiftActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Gifts");
        // TODO(gotit) mark this .. i'm reusing layout from check history
        setContentView(R.layout.layout_check_in_hist);
        presenter = new GiftPresenter(this);
        setPresenter(presenter);
        listGifts = (ListView) findViewById(R.id.listCheckInHistory);
    }

    @Override
    public void onAllGiftsRetrieved(List<String> giftDetails) {
        listGifts.setAdapter(new ArrayAdapter<String>(this, R.layout.layout_profile_item, giftDetails));
        listGifts.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Gift gift = presenter.getGift(position);
        startActivityForResult(GiftPhotoActivity.createIntent(this, gift), ACTIVITY_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == ACTIVITY_ID) {
            String giftFileName = data.getStringExtra(GiftPhotoActivity.KEY_GIFT_FILE_NAME);
            long hashKey = data.getLongExtra(GiftPhotoActivity.KEY_GIFT_HASH_KEY, -1);
            presenter.setGiftLocallySaved(hashKey, giftFileName);
        }
    }

}
