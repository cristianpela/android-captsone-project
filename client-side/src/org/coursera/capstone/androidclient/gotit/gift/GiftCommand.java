package org.coursera.capstone.androidclient.gotit.gift;

import java.io.Serializable;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.Gift;

import android.os.Bundle;

public class GiftCommand implements ProcessingCommand {

    public static final String KEY_GET_ALL_GIFTS = "KEY_GET_ALL_GIFTS";

    public static final String KEY_POST_GIFT = "KEY_POST_GIFT";

    public GiftCommand() {
    }

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        try {
            if (incomeParams.getString(KEY_POST_GIFT) != null && incomeParams.getString(KEY_USER_NAME) != null) {
                repository.postGiftPhoto(incomeParams.getString(KEY_USER_NAME), incomeParams.getString(KEY_POST_GIFT));
                outcomeParams.putBoolean(KEY_POST_GIFT, true);
            }

            if (incomeParams.getBoolean(KEY_GET_ALL_GIFTS, false)) {
                List<Gift> gifts = repository.getAllGifts();
                outcomeParams.putSerializable(KEY_GET_ALL_GIFTS, (Serializable) gifts);
            }
        } catch (Exception ex) {
            outcomeParams.putString(KEY_ERROR, ex.getMessage());
        }

    }
}
