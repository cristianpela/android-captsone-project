package org.coursera.capstone.androidclient.gotit.gift;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.common.GotItUtils;
import org.coursera.capstone.androidclient.gotit.common.InteractorCallback;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.repository.GotItRepository;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.Gift;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;

public class GiftPhotoActivity extends BaseActivity {

    private static final String KEY_GIFT = "KEY_GIFT";
    static final String KEY_GIFT_FILE_NAME = "KEY_GIFT_FILE_NAME";
    static final String KEY_GIFT_HASH_KEY = "KEY_GIFT_HASH_KEY";

    private ImageView imageGift;

    private FloatingActionButton downloadGiftButton;

    private Gift gift;

    private boolean isLocallySavedNow;

    public GiftPhotoActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gift = (Gift) getIntent().getSerializableExtra(KEY_GIFT);

        String photoFileName = gift.getPhotoFileName();
        boolean isLocallySaved = gift.isLocallySaved();

        getSupportActionBar().setTitle("Gift From: " + gift.getFollowerFullName());
        getSupportActionBar().setSubtitle("Sent On: " + GotItUtils.getStandardFormatedDate(gift.getDate()));

        setContentView(R.layout.layout_photo_gift);
        imageGift = (ImageView) findViewById(R.id.imageGiftPhoto);
        loadImage(photoFileName, isLocallySaved);
    }

    private void loadImage(final String giftFileName, final boolean isLocallySaved) {
        InteractorIntentServiceHelper.getInstance().startAsyncService(new InteractorCallback<String, Bitmap>() {

            @Override
            public Bitmap doInBackground(String source) {
                Repository repository = new GotItRepository(
                        GotItRepository.createClientProvider(getApplicationContext()));
                try {
                    InputStream is = repository.getGiftPhotoData(source, isLocallySaved);
                    return BitmapFactory.decodeStream(is);
                } catch (RemoteException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void onPostExecute(Bitmap result) {
                imageGift.setImageBitmap(result);
                if (!isLocallySaved) {
                    createFloatSaveButton();
                }
            }

        }, giftFileName);
    }

    @SuppressLint("InlinedApi")
    @SuppressWarnings("deprecation")
    private void createFloatSaveButton() {
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        final int position = (metrics.widthPixels / 4) - 30;

        downloadGiftButton = new FloatingActionButton.Builder(this)
                .withDrawable(getResources().getDrawable(R.drawable.ic_file_download_white_24dp))
                .withButtonColor(getResources().getColor(android.support.v7.appcompat.R.color.primary_material_dark))
                .withGravity(Gravity.BOTTOM | Gravity.END).withMargins(0, 0, position, 0).create();

        downloadGiftButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                InteractorIntentServiceHelper.getInstance().startAsyncService(
                        new InteractorCallback<String, Boolean>() {

                            @Override
                            public Boolean doInBackground(String source) {
                                File storageDir = Environment
                                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                                File giftFile = new File(storageDir, gift.getPhotoFileName());
                                try {

                                    // save on disk
                                    giftFile.createNewFile();
                                    FileOutputStream fos = new FileOutputStream(giftFile);
                                    Bitmap imageGiftBitmap = ((BitmapDrawable) imageGift.getDrawable()).getBitmap();
                                    imageGiftBitmap.compress(CompressFormat.PNG, 100, fos);
                                    fos.close();

                                    // save on media database
                                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                    Uri contentUri = Uri.fromFile(giftFile);
                                    mediaScanIntent.setData(contentUri);
                                    sendBroadcast(mediaScanIntent);

                                    gift.setPhotoFileName(giftFile.toString());
                                    
                                    return true;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return false;
                            }

                            @Override
                            public void onPostExecute(Boolean result) {
                                if (result) {
                                    showInfo("Gift was added to your photo gallery");
                                    downloadGiftButton.setVisibility(View.GONE);
                                    isLocallySavedNow = result;
                                }
                            }

                        }, gift.getPhotoFileName());
            }
        });
    }

    @Override
    public void finish() {
        if (isLocallySavedNow)
            setResult(
                    RESULT_OK,
                    new Intent().putExtra(KEY_GIFT_FILE_NAME, gift.getPhotoFileName()).putExtra(KEY_GIFT_HASH_KEY,
                            gift.getDate().getTime()));
        super.finish();
    }

    public static Intent createIntent(Context context, Gift gift) {
        return new Intent(context, GiftPhotoActivity.class).putExtra(KEY_GIFT, gift);

    }
}
