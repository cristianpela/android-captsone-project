package org.coursera.capstone.androidclient.gotit.gift;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.GotItView;

public interface GiftView extends GotItView {
    
    public void onAllGiftsRetrieved(List<String> giftDetails);

}
