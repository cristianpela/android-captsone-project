package org.coursera.capstone.androidclient.gotit.gift;

import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.GotItUtils;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.rest.Gift;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class GiftPresenter extends BasePresenter<GiftView> {

    private List<Gift> gifts;

    public GiftPresenter(GiftView view) {
        super(view);
        Bundle extras = new Bundle();
        extras.putBoolean(GiftCommand.KEY_GET_ALL_GIFTS, true);
        InteractorIntentServiceHelper.getInstance().setProcessingTarget(GiftCommand.class.getName())
                .addProcessingArgs(extras).returnAction(InteractorIntentServiceHelper.ACTION_GIFT).startService();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onReturnAction(Intent intent) {
        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_GIFT)) {
            Bundle extras = intent.getExtras();
            if (extras.getSerializable(GiftCommand.KEY_GET_ALL_GIFTS) != null) {
                gifts = (List<Gift>) extras.getSerializable(GiftCommand.KEY_GET_ALL_GIFTS);
                sendDataToView();
            }
        }
    }

    private void sendDataToView() {
        List<String> data = new ArrayList<String>();
        for (Gift gift : gifts) {
            String date = GotItUtils.getStandardFormatedDate(gift.getDate());
            data.add(date + " " + gift.getFollowerFullName());
        }
        getView().onAllGiftsRetrieved(data);
    }

    public Gift getGift(int position) {
        return gifts.get(position);
    }
    
    public void setGiftLocallySaved(long hashKey, String giftfileName){
        Gift gift = getGiftByLongDate(hashKey, giftfileName);
        gift.setLocallySaved(true);
        gift.setPhotoFileName(giftfileName);
    }

    private Gift getGiftByLongDate(long hashKey, String giftFileName) {
        for(Gift gift : gifts){
            long currentLongDate = gift.getDate().getTime();
            if(currentLongDate == hashKey)
                return gift;
        }
        return null;
    }

}
