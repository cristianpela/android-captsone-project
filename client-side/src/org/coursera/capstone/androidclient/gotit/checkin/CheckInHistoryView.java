package org.coursera.capstone.androidclient.gotit.checkin;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;

public interface CheckInHistoryView extends GotItView {

    public void onCheckInsRetrieved(List<String> checkInsDates, String fullName);
    
}
