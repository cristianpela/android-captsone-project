package org.coursera.capstone.androidclient.gotit.checkin;

import java.io.Serializable;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.rest.Feedback;
import org.coursera.capstone.androidclient.gotit.rest.FeedbackType;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CheckInDetailFragment extends Fragment {

    private static final String KEY_FEEDBACKS = "KEY_FEEDBACKS";

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_check_in_rep_details, container, false);
        ListView list = (ListView) rootView.findViewById(R.id.listReportQuestions);
        List<Feedback> feedbacks = (List<Feedback>) getArguments().getSerializable(KEY_FEEDBACKS);
        list.setAdapter(new CheckInDetailListAdapter(feedbacks, inflater));
        return rootView;
    }

    private static class CheckInDetailListAdapter extends BaseAdapter {

        private List<Feedback> feedbacks;

        private LayoutInflater inflater;

        CheckInDetailListAdapter(List<Feedback> feedbacks, LayoutInflater inflater) {
            this.feedbacks = feedbacks;
            this.inflater = inflater;
        }

        @Override
        public int getCount() {
            return feedbacks.size();
        }

        @Override
        public Object getItem(int position) {
            return feedbacks.get(position);
        }

        @Override
        public long getItemId(int position) {
            return feedbacks.get(position).getQuestionId();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            Holder holder;
            if (view == null) {
                view = inflater.inflate(R.layout.layout_feedback_item, parent, false);
                holder = new Holder();
                view.setTag(holder);
                holder.txtQuestionBody = (TextView) view.findViewById(R.id.txtFeedbackQuestionBody);
                holder.txtAnswer = (TextView) view.findViewById(R.id.textFeedbackAnswer);
            } else
                holder = (Holder) view.getTag();

            Feedback feedback = feedbacks.get(position);
            holder.txtQuestionBody.setText(feedback.getQuestionBody());
            holder.txtAnswer.setText(feedback.getAnswer());
            setAnswerColor(holder, feedback.getFeedbackType());
            return view;
        }

        private void setAnswerColor(Holder holder, FeedbackType feedbackType) {
            String colorString = (feedbackType == null) ? "#ffe082"
                    : (feedbackType == FeedbackType.POSITIVE) ? "#a5d6a7" : "#ef9a9a";
            holder.txtAnswer.setBackgroundColor(Color.parseColor(colorString));
        }

        private static class Holder {
            TextView txtQuestionBody, txtAnswer;
        }

    }

    public static Fragment createFragment(List<Feedback> feedbacks) {
        Fragment fragment = new CheckInDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_FEEDBACKS, (Serializable) feedbacks);
        fragment.setArguments(args);
        return fragment;
    }

}
