package org.coursera.capstone.androidclient.gotit.checkin;

import org.coursera.capstone.androidclient.gotit.common.GotItView;

public interface CheckInReportView extends GotItView{
    
    public void onDataParsed(CheckInReportPresenter.Data data);

}
