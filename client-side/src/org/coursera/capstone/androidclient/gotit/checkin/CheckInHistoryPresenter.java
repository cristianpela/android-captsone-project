package org.coursera.capstone.androidclient.gotit.checkin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.GotItUtils;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class CheckInHistoryPresenter extends BasePresenter<CheckInHistoryView> implements OnItemClickListener {
    
    private List<CheckIn> checkIns;

    public CheckInHistoryPresenter(CheckInHistoryView view, String userName) {
        super(view);
        Bundle bundle = new Bundle();
        bundle.putString(CheckInCommand.KEY_USER_NAME, userName);
        bundle.putBoolean(CheckInCommand.KEY_GET_CHECK_IN, true);
        InteractorIntentServiceHelper.getInstance().addProcessingArgs(bundle)
                .returnAction(InteractorIntentServiceHelper.ACTION_CHECK_IN)
                .setProcessingTarget(CheckInCommand.class.getName()).startService();

    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onReturnAction(Intent intent) {
        if(!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_CHECK_IN)){
            Serializable data = intent.getSerializableExtra(CheckInCommand.KEY_GET_CHECK_IN);
            if(data != null){
                checkIns = (List<CheckIn>) data;
                createDataForView();
            }
        }
    }

    private void createDataForView() {
        List<String> formatedDates = new ArrayList<String>();
        String fullName = null;
        for(CheckIn ch : checkIns){
            if(fullName == null)
                fullName = ch.getAuthorFullName();
            formatedDates.add(GotItUtils.getStandardFormatedDate(ch.getCreationDate()));
        }
        getView().onCheckInsRetrieved(formatedDates, fullName);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CheckIn checkIn = checkIns.get(position);
        Context context = parent.getContext();
        context.startActivity(CheckInReportActivity.createIntent(context, checkIn, -1));
    }

}
