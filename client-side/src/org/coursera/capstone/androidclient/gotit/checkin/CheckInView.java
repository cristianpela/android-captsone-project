package org.coursera.capstone.androidclient.gotit.checkin;

import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.rest.FeedbackType;

public interface CheckInView extends GotItView {

    public void onPreviousEnd();

    public void onNextEnd();

    public void onQuestionTracking(boolean enterable, String questionPosition, String questionBody, FeedbackType feedbackType,
            String answer);

    public void onCheckInComplete();

    public void onPostReportComplete();
}
