package org.coursera.capstone.androidclient.gotit.checkin;

import java.io.Serializable;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;

import android.os.Bundle;

public class CheckInCommand implements ProcessingCommand {

    public static final String KEY_REQUEST_CHECK_IN = "KEY_REQUEST_CHECK_IN";
    public static final String KEY_GET_CHECK_IN = "KEY_GET_CHECK_IN";
    public static final String KEY_POST_CHECK_IN = "KEY_POST_CHECK_IN";

    public CheckInCommand() {
    }

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        try {

            if (incomeParams.getBoolean(KEY_REQUEST_CHECK_IN, false) && incomeParams.getString(KEY_USER_NAME) != null) {
                String teenUserName = incomeParams.getString(KEY_USER_NAME);
                repository.requestCheckIn(teenUserName);
            }

            if (incomeParams.getBoolean(KEY_GET_CHECK_IN, false) && incomeParams.getString(KEY_USER_NAME) != null) {
                String teenUserName = incomeParams.getString(KEY_USER_NAME);
                List<CheckIn> checkIns = repository.getTeenCheckInHistroy(teenUserName);
                outcomeParams.putSerializable(KEY_GET_CHECK_IN, (Serializable) checkIns);
            }

            if (incomeParams.getSerializable(KEY_POST_CHECK_IN) != null) {
                repository.postCheckIn((CheckIn) incomeParams.getSerializable(KEY_POST_CHECK_IN));
                outcomeParams.putBoolean(KEY_POST_CHECK_IN, true);
            }

        } catch (Exception ex) {
            outcomeParams.putString(KEY_ERROR, ex.getMessage());
        }
    }

}
