package org.coursera.capstone.androidclient.gotit.checkin;

import java.util.Date;
import java.util.List;

import org.apache.http.client.utils.DateUtils;
import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInPresenter.ShareScope;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.common.GotItUtils;
import org.coursera.capstone.androidclient.gotit.rest.FeedbackType;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CheckInActivity extends BaseActivity implements CheckInView, OnGesturePerformedListener {

    private static final String CHECK_IN_USER_NAME_PARAM = "check_in_user_name";

    private CheckInPresenter presenter;

    private GestureOverlayView gesturetOverlayView;

    private GestureLibrary gestureLibrary;

    private LinearLayout layoutQuestionTracker, layoutCheckInMain;
    
    private FrameLayout layoutLoading;

    private TextView txtQuestionBody, txtQuestionNo;

    private ImageButton btnNext, btnPrev, btnReport, btnUploadCheckin, btnQuestionShare;

    private EditText editAnswer;

    private int defaultFeedbackColor = Color.TRANSPARENT;

    public CheckInActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_check_in);
        String userName = getIntent().getStringExtra(CHECK_IN_USER_NAME_PARAM);
        presenter = new CheckInPresenter(this, userName);
        setPresenter(presenter);
        initFields();
        setCheckInTitle();
    }

    private void setCheckInTitle() {
        getSupportActionBar().setTitle("Check In");
        getSupportActionBar().setSubtitle(GotItUtils.getStandardFormatedDate(new Date()));
    }

    private void initFields() {
        
        layoutCheckInMain = (LinearLayout) findViewById(R.id.layoutCheckInMain);
        layoutCheckInMain.setVisibility(View.INVISIBLE);
        layoutLoading = (FrameLayout) findViewById(R.id.progLoadingLayout);
        layoutLoading.setVisibility(View.VISIBLE);

        gesturetOverlayView = (GestureOverlayView) findViewById(R.id.gestureOverlay);
        gesturetOverlayView.addOnGesturePerformedListener(this);
        gestureLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
        gestureLibrary.load();

        layoutQuestionTracker = (LinearLayout) findViewById(R.id.layoutQuestionTracker);
        // defaultFeedbackColor = ((ColorDrawable)
        // layoutQuestionTracker.getBackground()).getColor();
        txtQuestionNo = (TextView) findViewById(R.id.txtQuestionNo);
        txtQuestionBody = (TextView) findViewById(R.id.txtQuestionBody);

        btnNext = (ImageButton) findViewById(R.id.btnQuestionNext);
        btnNext.setBackgroundColor(defaultFeedbackColor);
        btnPrev = (ImageButton) findViewById(R.id.btnQuestionPrev);
        btnPrev.setBackgroundColor(defaultFeedbackColor);

        btnReport = (ImageButton) findViewById(R.id.btnGenReport);
        btnReport.setVisibility(View.INVISIBLE);
        btnReport.setBackgroundColor(defaultFeedbackColor);
        btnUploadCheckin = (ImageButton) findViewById(R.id.btnUploadCheckin);
        btnUploadCheckin.setBackgroundColor(defaultFeedbackColor);
        btnUploadCheckin.setVisibility(View.INVISIBLE);

        btnQuestionShare = (ImageButton) findViewById(R.id.btnQuestionShare);
        btnQuestionShare.setBackgroundColor(defaultFeedbackColor);

        editAnswer = (EditText) findViewById(R.id.editAnswer);

    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        List<Prediction> predictions = gestureLibrary.recognize(gesture);
        Prediction bestPrediction = null;
        for (Prediction prediction : predictions) {
            if (bestPrediction == null || prediction.score > bestPrediction.score) {
                bestPrediction = prediction;
            }
        }

        FeedbackType feedbackType = null;
        if (bestPrediction.name.equalsIgnoreCase("check")) {
            feedbackType = FeedbackType.POSITIVE;
            layoutQuestionTracker.setBackgroundColor(Color.parseColor("#a5d6a7"));
        } else if (bestPrediction.name.equalsIgnoreCase("nope")) {
            layoutQuestionTracker.setBackgroundColor(Color.parseColor("#ef9a9a"));
            feedbackType = FeedbackType.NEGATIVE;
        }

        if (feedbackType != null) {
            presenter.addFeedbackAnswer(feedbackType, editAnswer.getText().toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.check_in_share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        createShareDialog(ShareScope.CHECK_IN, "Check In Shared With:");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckInComplete() {
        btnReport.setVisibility(View.VISIBLE);
        btnUploadCheckin.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPostReportComplete() {
        finish();
    }

    @Override
    public void onPreviousEnd() {
        btnPrev.setEnabled(false);
    }

    @Override
    public void onNextEnd() {
        btnNext.setEnabled(false);
    }

    @Override
    public void onQuestionTracking(boolean enterable, String questionPosition, String questionBody, FeedbackType feedbackType,
            String answer) {
        layoutCheckInMain.setVisibility(View.VISIBLE);
        layoutLoading.setVisibility(View.GONE);
        
        txtQuestionNo.setText(questionPosition);
        txtQuestionBody.setText(questionBody);
        
        if(enterable){
            editAnswer.setVisibility(View.VISIBLE);
        }else
            editAnswer.setVisibility(View.GONE);

        if (feedbackType != null) {
            if (feedbackType == FeedbackType.POSITIVE) {
                layoutQuestionTracker.setBackgroundColor(Color.parseColor("#a5d6a7"));
            } else if (feedbackType == FeedbackType.NEGATIVE)
                layoutQuestionTracker.setBackgroundColor(Color.parseColor("#ef9a9a"));
        } else {
            layoutQuestionTracker.setBackgroundColor(defaultFeedbackColor);
        }

        if (answer != null)
            editAnswer.setText(answer);
    }

    @Override
    public void showError(String error) {
        super.showError(error);
        layoutQuestionTracker.setBackgroundColor(defaultFeedbackColor);
    }

    public void actionPrevious(View button) {
        transitionState();
        presenter.previous();
    }

    public void actionNext(View button) {
        transitionState();
        presenter.next();
    }

    public void actionReport(View button) {
        startActivity(CheckInReportActivity.createIntent(this, presenter.getCachedModel(), -1));
    }

    public void actionUpload(View button) {
        presenter.postCheckIn();
    }

    public void actionFeedbackQuestionShare(View button) {
        createShareDialog(ShareScope.QUESTION, "Feedback Shared With:");
    }

    private void transitionState() {
        btnNext.setEnabled(true);
        btnPrev.setEnabled(true);
        editAnswer.setText("");
    }

    private void createShareDialog(final CheckInPresenter.ShareScope shareScope, String title) {
        CheckInPresenter.PreparedSharing preparedSharing = presenter.getPreparedSharing(shareScope);
        if(preparedSharing.getFullNames().length == 0){
            showInfo("You don't have any followers!");
            return;
        };
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMultiChoiceItems(preparedSharing.getFullNames(), preparedSharing.getCheckedFollowers(),
                        new OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                presenter.decideSharing(shareScope, which, isChecked);
                            }
                        }).setPositiveButton("Close", null).create().show();

    }

    public static Intent startingIntent(Context context, String userName) {
        return new Intent(context, CheckInActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra(
                CHECK_IN_USER_NAME_PARAM, userName);
    }

}
