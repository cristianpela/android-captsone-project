package org.coursera.capstone.androidclient.gotit.checkin;

import java.io.Serializable;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.Question;

import android.os.Bundle;

public class QuestionCommand implements ProcessingCommand {

    public static final String KEY_QUESTIONS = "KEY_QUESTIONS";

    public QuestionCommand() {
    }

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        try{
            List<Question> questions = repository.getAllQuestions();
            outcomeParams.putSerializable(KEY_QUESTIONS, (Serializable)questions);
        }catch (Exception ex){
            outcomeParams.putString(KEY_ERROR, ex.getMessage());
        }
    }

    
}
