package org.coursera.capstone.androidclient.gotit.checkin;

import java.io.Serializable;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInPresenter.ShareScope;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInReportPresenter.Data;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class CheckInReportActivity extends BaseActivity implements CheckInReportView {

    private static final int NUM_PAGES = 2;

    private static final String CHECK_IN_REPORT_FULL_DATA_MODEL = "data_model";
    private static final String CHECK_IN_ID = "check_in_id";

    private CheckInReportPresenter presenter;

    private ViewPager viewPager;

    public CheckInReportActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Serializable model = getIntent().getSerializableExtra(CHECK_IN_REPORT_FULL_DATA_MODEL);
        long checkInId = getIntent().getLongExtra(CHECK_IN_ID, -1);
       
        setContentView(R.layout.layout_check_in_report);
        viewPager = (ViewPager) findViewById(R.id.pager);
        
        presenter = new CheckInReportPresenter(this, model, checkInId);
        setPresenter(presenter);
    }

    @Override
    public void onDataParsed(CheckInReportPresenter.Data data) {
        getSupportActionBar().setTitle(data.getAuthorFullName());
        getSupportActionBar().setSubtitle(data.getFormatedCreationDate());
        viewPager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager(), data));
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.check_in_report, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int currentSlideItem = viewPager.getCurrentItem();
        if(item.getItemId() == R.id.menu_next && currentSlideItem < NUM_PAGES - 1){
            viewPager.setCurrentItem(currentSlideItem + 1);
            return true;
        }else if(item.getItemId() == R.id.menu_back  && currentSlideItem > 0){
            viewPager.setCurrentItem(currentSlideItem - 1);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onBackPressed() {
        int currentSlideItem = viewPager.getCurrentItem();
        if (currentSlideItem == 0) {
            super.onBackPressed();
        } else {
            viewPager.setCurrentItem(currentSlideItem - 1);
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private CheckInReportPresenter.Data data;

        public ScreenSlidePagerAdapter(FragmentManager fm, CheckInReportPresenter.Data data) {
            super(fm);
            this.data = data;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
            case 0:
                fragment = CheckInChartFragment.createFragment(data.getX(), data.getY());
                break;
            case 1:
                fragment = CheckInDetailFragment.createFragment(data.getFeedbackList());
                break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public static Intent createIntent(Context context, Serializable dataModel, long checkInId) {
        return new Intent(context, CheckInReportActivity.class).putExtra(CHECK_IN_REPORT_FULL_DATA_MODEL, dataModel)
                .putExtra(CHECK_IN_ID, checkInId).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }

}
