package org.coursera.capstone.androidclient.gotit.checkin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.profile.ProfileListItem;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;
import org.coursera.capstone.androidclient.gotit.rest.Feedback;
import org.coursera.capstone.androidclient.gotit.rest.FeedbackType;
import org.coursera.capstone.androidclient.gotit.rest.Question;
import org.coursera.capstone.androidclient.gotit.rest.QuestionEnterType;
import org.coursera.capstone.androidclient.gotit.rest.QuestionGeneratedFeedbackMode;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

public class CheckInPresenter extends BasePresenter<CheckInView> {

    public enum ShareScope {
        CHECK_IN, QUESTION;
    }

    private CheckInModel model;

    public CheckInPresenter(CheckInView view, String userName) {
        super(view);
        String findUserName = (userName == null) ? getCurrentLoggedUserName() : userName;
        InteractorIntentServiceHelper.PredefinedInteractions.find(findUserName, false);
        model = new CheckInModel();
        setCacheModel(model.checkIn);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onReturnAction(Intent intent) {
        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_SEARCH)) {
            User user = ((List<User>) intent.getExtras().getSerializable(ProcessingCommand.KEY_USERS)).get(0);
            model.checkIn.setAuthor(user.getUserName());
            model.checkIn.setAuthorFullName(user.getUserDetails().getFirstName() + " "
                    + user.getUserDetails().getLastName());
            for (String fol : user.getFollowers()) {
                ProfileListItem pli = ProfileListItem.create(fol);
                model.followers.add(pli);
                model.checkIn.getSharedWith().add(pli.userName);
            }
            Collections.sort(model.followers);
            Collections.sort(model.checkIn.getSharedWith());
            InteractorIntentServiceHelper.getInstance().returnAction(InteractorIntentServiceHelper.ACTION_QUESTION)
                    .setProcessingTarget(QuestionCommand.class.getName()).startService();
        }

        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_QUESTION)) {
            model.questions.addAll((List<Question>) intent.getExtras().getSerializable(QuestionCommand.KEY_QUESTIONS));
            for (int i = 0; i < model.questions.size(); i++) {
                Feedback emptyFeedback = new Feedback();
                emptyFeedback.setQuestionId(model.questions.get(i).getId());
                emptyFeedback.getSharedWith().addAll(model.checkIn.getSharedWith());
                emptyFeedback.setQuestionBody(model.questions.get(i).getBody());
                model.checkIn.getFeedback().add(emptyFeedback);
            }
            viewOnQuestionTracking();
            getView().onPreviousEnd();
        }

        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_CHECK_IN)) {
            if (intent.getExtras().getBoolean(CheckInCommand.KEY_POST_CHECK_IN, false)) {
                getView().onPostReportComplete();
            }
        }
    }

    private String questionTrackingOutput() {
        return (model.currentQuestionNo + 1) + "/" + model.questions.size();
    }

    private Question currentQuestion() {
        return model.questions.get(model.currentQuestionNo);
    }

    private Feedback currentQuestionFeedback() {
        for (Feedback feedback : model.checkIn.getFeedback()) {
            if (feedback.getQuestionId() == currentQuestion().getId())
                return feedback;
        }
        return null;
    }

    private void viewOnQuestionTracking() {
        FeedbackType feedbackType = null;
        String answer = null;
        Feedback feedback = currentQuestionFeedback();
        if (feedback.getAnswer() != null) {
            feedbackType = feedback.getFeedbackType();
            answer = feedback.getAnswer();
        }
        boolean enterable = currentQuestion().isEnterable();
        getView().onQuestionTracking(enterable, questionTrackingOutput(), currentQuestion().getBody(), feedbackType,
                answer);
    }

    public void addFeedbackAnswer(FeedbackType feedbackType, String answer) {
        Question question = currentQuestion();
        Feedback currentFeedback = currentQuestionFeedback();

        if (question.isEnterable()) {
            if (TextUtils.isEmpty(answer)) {
                getView().showError("Please enter your answer first!");
                return;
            }
            if ((question.getEnterType() == QuestionEnterType.INT || question.getEnterType() == QuestionEnterType.DOUBLE)
                    && !TextUtils.isDigitsOnly(answer)) {
                getView().showError("Please enter a numeric value!");
                return;
            }
            currentFeedback.setAnswer(answer);
        } else {
            currentFeedback.setAnswer((feedbackType == FeedbackType.POSITIVE) ? "YES" : "NO");

        }
        currentFeedback.setFeedbackType(feedbackType);

        if (isCheckinComplete())
            getView().onCheckInComplete();
    }

    public void previous() {
        model.currentQuestionNo--;
        if (model.currentQuestionNo == 0)
            getView().onPreviousEnd();
        viewOnQuestionTracking();
    }

    public void next() {
        model.currentQuestionNo++;
        if (model.currentQuestionNo == model.questions.size() - 1)
            getView().onNextEnd();
        viewOnQuestionTracking();
    }

    public CheckIn getCheckIn() {
        return model.checkIn;
    }

    private boolean isCheckinComplete() {
        for (Feedback feedback : model.checkIn.getFeedback())
            if (feedback.getAnswer() == null)
                return false;
        return true;
    }

    private static class CheckInModel {
        List<Question> questions = new ArrayList<Question>();
        CheckIn checkIn = new CheckIn();
        List<ProfileListItem> followers = new ArrayList<>();
        int currentQuestionNo = 0;
    }

    static class PreparedSharing {

        private String[] fullNames;
        private boolean[] checkedFollowers;
        private CheckInModel model;

        private PreparedSharing(ShareScope shareLevel, CheckInModel model) {
            this.model = model;
            if (shareLevel == ShareScope.CHECK_IN) {
                setShareFollowersForCheckIn();
            } else {
                setShareFollowersForQuestion();
            }
        }

        private void setShareFollowersForCheckIn() {
            int size = model.followers.size();
            fullNames = new String[size];
            checkedFollowers = new boolean[size];
            List<String> sharedWith = model.checkIn.getSharedWith();
            for (int i = 0; i < size; i++) {
                ProfileListItem pli = model.followers.get(i);
                fullNames[i] = pli.name;
                checkedFollowers[i] = sharedWith.contains(pli.userName);
            }
        }

        private void setShareFollowersForQuestion() {
            List<String> sharedWithCheckIn = model.checkIn.getSharedWith();
            int size = sharedWithCheckIn.size();
            fullNames = new String[size];
            checkedFollowers = new boolean[size];
            Feedback feedback = model.checkIn.getFeedback().get(model.currentQuestionNo);
            List<String> sharedWithFeedback = feedback.getSharedWith();
            for (int i = 0; i < size; i++) {
                ProfileListItem pli = getProfileListItemByUserName(sharedWithCheckIn.get(i));
                fullNames[i] = pli.name;
                checkedFollowers[i] = sharedWithFeedback.contains(pli.userName);
            }
        }

        private ProfileListItem getProfileListItemByUserName(String userName) {
            for (ProfileListItem pli : model.followers) {
                if (pli.userName.equals(userName))
                    return pli;
            }
            return null;
        }

        public String[] getFullNames() {
            return fullNames;
        }

        public boolean[] getCheckedFollowers() {
            return checkedFollowers;
        }

    }

    public void decideSharing(ShareScope shareLevel, int followerPosition, boolean isChecked) {
        ProfileListItem pli = model.followers.get(followerPosition);
        if (shareLevel == ShareScope.CHECK_IN) {
            List<String> sharedWithCheckIn = model.checkIn.getSharedWith();
            if (isChecked && !sharedWithCheckIn.contains(pli.userName))
                sharedWithCheckIn.add(pli.userName);
            else
                sharedWithCheckIn.remove(pli.userName);
            for (Feedback feedback : model.checkIn.getFeedback()) {
                List<String> sharedWithFeedback = feedback.getSharedWith();
                if (isChecked && !sharedWithFeedback.contains(pli.userName))
                    sharedWithFeedback.add(pli.userName);
                else
                    sharedWithFeedback.remove(pli.userName);
                Collections.sort(sharedWithFeedback);
            }
            Collections.sort(sharedWithCheckIn);
        } else {
            Feedback feedback = currentQuestionFeedback();
            List<String> sharedWithFeedback = feedback.getSharedWith();
            if (isChecked && !sharedWithFeedback.contains(pli.userName))
                sharedWithFeedback.add(pli.userName);
            else
                sharedWithFeedback.remove(pli.userName);
            Collections.sort(sharedWithFeedback);
        }
    }

    public PreparedSharing getPreparedSharing(ShareScope shareScope) {
        return new PreparedSharing(shareScope, model);
    }

    public void postCheckIn() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CheckInCommand.KEY_POST_CHECK_IN, model.checkIn);
        InteractorIntentServiceHelper.getInstance().returnAction(InteractorIntentServiceHelper.ACTION_CHECK_IN)
                .addProcessingArgs(bundle).setProcessingTarget(CheckInCommand.class.getName()).startService();
    }

}
