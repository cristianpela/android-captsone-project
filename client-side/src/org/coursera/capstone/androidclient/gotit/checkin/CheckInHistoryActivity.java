package org.coursera.capstone.androidclient.gotit.checkin;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class CheckInHistoryActivity extends BaseActivity implements CheckInHistoryView {

    private static final String KEY_USER_NAME = "KEY_USER_NAME";
    
    private CheckInHistoryPresenter presenter;
    
    private ListView listCheckInHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_check_in_hist);
        listCheckInHistory = (ListView) findViewById(R.id.listCheckInHistory);

        String userName = getIntent().getStringExtra(KEY_USER_NAME);
        presenter = new CheckInHistoryPresenter(this, userName);
        setPresenter(presenter);
    }
    
    @Override
    public void onCheckInsRetrieved(List<String> checkInsDates, String fullName) {
        listCheckInHistory.setAdapter(new ArrayAdapter<String>(this, R.layout.layout_profile_item, checkInsDates));
        listCheckInHistory.setOnItemClickListener(presenter);
        getSupportActionBar().setTitle(fullName);
        getSupportActionBar().setSubtitle("Check In History");
    }

    public static Intent createIntent(Context context, String userName) {
        return new Intent(context, CheckInHistoryActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra(
                KEY_USER_NAME, userName);
    }

}
