package org.coursera.capstone.androidclient.gotit.checkin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class CheckInChartFragment extends Fragment {

    private static final String CHART_X_DATA = "CHART_X_DATA";
    private static final String CHART_Y_DATA = "CHART_Y_DATA";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_check_in_rep_chart, container, false);
        createChart(rootView);
        return rootView;
    }

    private void createChart(ViewGroup rootView) {

        String[] xData = getArguments().getStringArray(CHART_X_DATA);
        float[] yData = getArguments().getFloatArray(CHART_Y_DATA);

        PieChart chart = new PieChart(rootView.getContext());
        chart.setUsePercentValues(true);
        chart.setDescription("Feedback Proportions");

        Legend legend = chart.getLegend();
        legend.setPosition(LegendPosition.LEFT_OF_CHART);

        PieDataSet chartDataSet = new PieDataSet(yEntries(yData), "Feedback Type");
        chartDataSet.setColors(new int[]{Color.parseColor("#a5d6a7"), Color.parseColor("#ef9a9a"), Color.parseColor("#ffe082")});
        chartDataSet.setSliceSpace(3f);

        PieData chartData = new PieData(Arrays.asList(xData), chartDataSet);
        chartData.setValueFormatter(new PercentFormatter());

        chart.setData(chartData);
        chart.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        
        rootView.addView(chart);

    }

    private List<Entry> yEntries(float[] yData) {
        List<Entry> entries = new ArrayList<Entry>();
        for (int i = 0; i < yData.length; i++) {
            entries.add(new Entry(yData[i], i));
        }
        return entries;
    }

    public static Fragment createFragment(String[] xData, float[] yData) {
        Fragment fragment = new CheckInChartFragment();
        Bundle args = new Bundle();
        args.putCharSequenceArray(CHART_X_DATA, xData);
        args.putFloatArray(CHART_Y_DATA, yData);
        fragment.setArguments(args);
        return fragment;
    }
}
