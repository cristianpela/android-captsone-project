package org.coursera.capstone.androidclient.gotit.checkin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.utils.DateUtils;
import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.rest.Answer;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;
import org.coursera.capstone.androidclient.gotit.rest.Feedback;
import org.coursera.capstone.androidclient.gotit.rest.FeedbackType;
import org.coursera.capstone.androidclient.gotit.rest.Question;

import android.content.Intent;

public class CheckInReportPresenter extends BasePresenter<CheckInReportView> {

    private CheckIn checkIn;

    public CheckInReportPresenter(CheckInReportView view, Serializable model, long checkInId) {
        super(view, model);
        if (model != null) {
            checkIn = (CheckIn) model;
            InteractorIntentServiceHelper.getInstance().returnAction(InteractorIntentServiceHelper.ACTION_QUESTION)
                    .setProcessingTarget(QuestionCommand.class.getName()).startService();

        } else if (checkInId != -1) {
            // TODO(gotit): call server for
        }

    }

    private Feedback getFeedBackByQuestionId(long questionId) {
        for (Feedback feedback : checkIn.getFeedback()) {
            if (feedback.getQuestionId() == questionId)
                return feedback;
        }
        return null;
    }

    private void createDataForFollower() {
        // aggregators indices: 0 - Positive, 1 - Negative, 2 - Private
        float[] aggregators = new float[3];
        for (Feedback feedback : checkIn.getFeedback()) {
            if (feedback.getFeedbackType() == FeedbackType.POSITIVE) {
                aggregators[0]++;
            } else if (feedback.getFeedbackType() == FeedbackType.NEGATIVE) {
                aggregators[1]++;
            } else
                aggregators[2]++;
        }
        getView().onDataParsed(new Data(checkIn, new String[] { "Positive", "Negative", "Private" }, aggregators));
    }

    private void createDataForCurrentLoggedTeen() {
        float[] aggregators = new float[2];
        for (Feedback feedback : checkIn.getFeedback()) {
            if (feedback.getFeedbackType() == FeedbackType.POSITIVE) {
                aggregators[0]++;
            } else if (feedback.getFeedbackType() == FeedbackType.NEGATIVE) {
                aggregators[1]++;
            }
        }
        getView().onDataParsed(new Data(checkIn, new String[] { "Positive", "Negative" }, aggregators));
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onReturnAction(Intent intent) {
        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_QUESTION)) {
            makeCheckInExpanded((List<Question>) intent.getExtras().getSerializable(QuestionCommand.KEY_QUESTIONS));
        }
    }

    private void makeCheckInExpanded(List<Question> questions) {
        List<Feedback> expFeedbacks = new ArrayList<Feedback>();
        for (Question question : questions) {
            Feedback feedback = getFeedBackByQuestionId(question.getId());
            if (feedback == null) {
                feedback = new Feedback(Answer.PRIVATE);
            }
            feedback.setQuestionBody(question.getBody());
            expFeedbacks.add(feedback);
        }
        checkIn.setFeedback(expFeedbacks);
        if (checkIn.getAuthor().equals(getCurrentLoggedUserName()))
            createDataForCurrentLoggedTeen();
        else {
            createDataForFollower();
        }
    }

    static class Data {

        private String[] xData;

        private float[] yData;

        private List<Feedback> feedbackList;

        private String formatedDate;

        private String authorFullName;

        public Data(CheckIn checkIn, String[] xData, float[] yData) {
            this.feedbackList = checkIn.getFeedback();
            this.xData = xData;
            this.yData = yData;
            Date creationDate = (checkIn.getCreationDate() == null) ? new Date() : checkIn.getCreationDate();
            formatedDate = DateUtils.formatDate(creationDate, "MM.dd.yyyy hh:mm");
            authorFullName = checkIn.getAuthorFullName();
        }

        public List<Feedback> getFeedbackList() {
            return feedbackList;
        }

        public String[] getX() {
            return xData;
        }

        public float[] getY() {
            return yData;
        }

        public String getFormatedCreationDate() {
            return formatedDate;
        }

        public String getAuthorFullName() {
            return authorFullName;
        }

    }
}
