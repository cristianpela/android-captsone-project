package org.coursera.capstone.androidclient.gotit.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

public abstract class ProfileImageLoader implements InteractorCallback<String, Bitmap> {

    public ProfileImageLoader() { 
    }

    @Override
    public Bitmap doInBackground(String source) {
        Bitmap bitmap = BitmapFactory.decodeFile(source);
        return ThumbnailUtils.extractThumbnail(bitmap, 122, 122);
    }

}
