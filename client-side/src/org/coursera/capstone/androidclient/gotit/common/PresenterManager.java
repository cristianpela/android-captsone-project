package org.coursera.capstone.androidclient.gotit.common;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

//TODO(gotit): Drop down this class, use onConfigurationChange instead for screen rotation
public class PresenterManager {

    private WeakReference<FragmentManager> fragmentManagerRef;

    private PresenterModelRetainer modelRetainer;

    public PresenterManager(FragmentManager fragmentManager) {
        fragmentManagerRef = new WeakReference<FragmentManager>(fragmentManager);
    }

    public void manage(BasePresenter<? extends GotItView> presenter){
        presenter.setCacheModel(getModel(presenter.getClass().getName()));
    }

    private Serializable getModel(String tag) {
        FragmentManager fragmentManager = fragmentManagerRef.get();
        if (fragmentManager != null) {
            modelRetainer = (PresenterModelRetainer) fragmentManager.findFragmentByTag(tag);
            if (modelRetainer == null) {
                modelRetainer = new PresenterModelRetainer();
                fragmentManager.beginTransaction().add(modelRetainer, tag).commit();
            }
        }
        return modelRetainer.getModel();
    }

    public void saveModel(Serializable model) {
        modelRetainer.setModel(model);
    }

    private static class PresenterModelRetainer extends Fragment {

        private Serializable model;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public Serializable getModel() {
            return model;
        }

        public void setModel(Serializable model) {
            this.model = model;
        }

    }
}
