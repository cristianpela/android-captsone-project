package org.coursera.capstone.androidclient.gotit.common;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import android.app.Application;

public class GotItApplication extends Application {

    @Override
    public void onCreate() {
        InteractorIntentServiceHelper.createInstance(this);
        ImageLoader.getInstance().init(
                new ImageLoaderConfiguration.Builder(this).imageDownloader(new SecuredImageDownloader(this)).build());
    }

}
