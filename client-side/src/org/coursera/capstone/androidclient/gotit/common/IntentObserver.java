/**
 * 
 */
package org.coursera.capstone.androidclient.gotit.common;

import java.util.Observable;
import java.util.Observer;

import android.content.Intent;


public abstract class IntentObserver implements Observer {

	@Override
	public void update(Observable observable, Object data) {
		onReturnAction((Intent) data);
	}

	protected abstract void onReturnAction(Intent intent);

}
