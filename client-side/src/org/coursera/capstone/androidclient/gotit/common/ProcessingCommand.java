package org.coursera.capstone.androidclient.gotit.common;

import org.coursera.capstone.androidclient.gotit.repository.Repository;

import android.os.Bundle;

public interface ProcessingCommand {
    
    public static final String KEY_USER_NAME = "key_user_name";
    public static final String KEY_PASSWORD = "key_password";
    public static final String KEY_ERROR = "key_error";
    public static final String KEY_USERS = "key_users";
    public static final String KEY_MANY = "key_users_many";
    public static final String KEY_MESSAGE = "key_message";
    
	public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository);

}
