package org.coursera.capstone.androidclient.gotit.common;

public interface InteractorCallback<S, R> {

    public R doInBackground(S source);

    public void onPostExecute(R result);

}
