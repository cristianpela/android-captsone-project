package org.coursera.capstone.androidclient.gotit.common;

import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.repository.Contracts;
import org.coursera.capstone.androidclient.gotit.repository.GotItRepository;

import android.accounts.Account;
import android.app.IntentService;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class InteractorIntentService extends IntentService {
    
	protected static final String TAG = InteractorIntentService.class
			.getSimpleName();

	public InteractorIntentService() {
		super(InteractorIntentService.class.getSimpleName());
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String processCommandClass = intent
				.getStringExtra(InteractorIntentServiceHelper.EXTRA_TARGET_COMMAND_CLASS);
		try {
			ProcessingCommand command = (ProcessingCommand) Class.forName(
					processCommandClass).newInstance();
			Bundle incomeParams = intent.getExtras();
			Bundle outcomeParams = new Bundle();
			String resultTarget = intent
					.getStringExtra(InteractorIntentServiceHelper.EXTRA_TARGET_BROADCAST_ACTION);
			
			Context context = getApplicationContext();
			Account account = new AccountManagerHelper(context).getCurrentAccount();
			ContentProviderClient providerClient = context.getContentResolver().acquireContentProviderClient(Contracts.URI.AUTHORITY);
			command.process(resultTarget, incomeParams, outcomeParams, new GotItRepository(context, account, providerClient));
			
			if (resultTarget != null) {
			    Log.d(TAG, "Send Broadcast Action Result: "+resultTarget);
				context.sendBroadcast(
						new Intent(resultTarget).putExtras(outcomeParams));
			}
			providerClient.release();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			Log.e(TAG, "Error creating class");
		}
	}
	
	
}
