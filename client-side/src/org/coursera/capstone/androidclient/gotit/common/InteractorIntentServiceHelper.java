package org.coursera.capstone.androidclient.gotit.common;

import java.lang.ref.WeakReference;

import org.coursera.capstone.androidclient.gotit.find.FindUserCommand;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

public class InteractorIntentServiceHelper {

    public static final String ACTION_SEARCH = "org.coursera.gotit.diab.mngmnt.action.SEARCH";
    public static final String ACTION_LOG_IN = "org.coursera.gotit.diab.mngmnt.action.LOG_IN";
    public static final String ACTION_SIGN_UP = "org.coursera.gotit.diab.mngmnt.action.SIGN_UP";
    public static final String ACTION_REQUEST_FOLLOW = "org.coursera.gotit.diab.mngmnt.action.REQUEST_FOLLOW";
    public static final String ACTION_NOTIFICATION = "org.coursera.gotit.diab.mngmnt.action.NOTIFICATION";
    public static final String ACTION_CHECK_IN = "org.coursera.gotit.diab.mngmnt.action.CHECK_IN";
    public static final String ACTION_QUESTION = "org.coursera.gotit.diab.mngmnt.action.QUESTION";
    public static final String ACTION_GIFT = "org.coursera.gotit.diab.mngmnt.action.GIFT";

    private static InteractorIntentServiceHelper INSTANCE;

    static final String EXTRA_TARGET_COMMAND_CLASS = "extra_target_id_command_class";
    static final String EXTRA_TARGET_BROADCAST_ACTION = "extra_target_id_return_result";

    private WeakReference<Context> contextRef;
    private Context context;
    private Intent startServiceIntent;

    public InteractorIntentServiceHelper(Context context) {
        contextRef = new WeakReference<Context>(context);
        this.context = contextRef.get();
        startServiceIntent = new Intent(context, InteractorIntentService.class);
    }

    public static void createInstance(Context context) {
        INSTANCE = new InteractorIntentServiceHelper(context);
    }

    public static InteractorIntentServiceHelper getInstance() {
        return INSTANCE;
    }

    public InteractorIntentServiceHelper setProcessingTarget(String targetCommandClass) {
        startServiceIntent.putExtra(EXTRA_TARGET_COMMAND_CLASS, targetCommandClass);
        return this;
    }

    public InteractorIntentServiceHelper returnAction(String targetBroadcastAction) {
        startServiceIntent.putExtra(EXTRA_TARGET_BROADCAST_ACTION, targetBroadcastAction);
        return this;
    }

    public InteractorIntentServiceHelper addProcessingArgs(Bundle extras) {
        startServiceIntent.putExtras(extras);
        return this;
    }

    public void startService() {
        context.startService(startServiceIntent);
        startServiceIntent = new Intent(context, InteractorIntentService.class);//refresh intent
    }

    @SuppressWarnings("unchecked")
    public <S, R> void startAsyncService(final InteractorCallback<S, R> callback, final S source) {
        new AsyncTask<S, Void, R>() {
            @Override
            protected R doInBackground(S... params) {
                return callback.doInBackground(params[0]);
            }

            @Override
            protected void onPostExecute(R result) {
                callback.onPostExecute(result);
            }

        }.execute(source);
    }
    
    public static final class PredefinedInteractions{
        
        public static void find(String userName, boolean many){
            Bundle args = new Bundle();
            args.putString(ProcessingCommand.KEY_USER_NAME, userName);
            args.putBoolean(ProcessingCommand.KEY_MANY, many);
            InteractorIntentServiceHelper.getInstance().setProcessingTarget(FindUserCommand.class.getName())
                    .addProcessingArgs(args).returnAction(InteractorIntentServiceHelper.ACTION_SEARCH).startService();
        }
    }
}
