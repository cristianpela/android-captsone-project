package org.coursera.capstone.androidclient.gotit.common;

import org.coursera.capstone.androidclient.gotit.main.DashboardActivity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class BaseFragment extends Fragment implements GotItView {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String ARG_LAYOUT_ID = "layout_id";

    private static final String TAG = BaseFragment.class.getSimpleName();

    private BasePresenter<? extends GotItView> presenter;

    public BaseFragment() {

    }

    protected void setPresenter(BasePresenter<? extends GotItView> presenter) {
        this.presenter = presenter;
    }

    protected boolean isPresenterAttached() {
        return presenter != null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((DashboardActivity) activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(getArguments().getInt(ARG_LAYOUT_ID), container, false);
        onViewInflated(rootView);
        return rootView;
    }

    protected void onViewInflated(View rootView) {

    }

    @Override
    public void onPause() {
        if (presenter != null)
            presenter.setDormant(true);
        super.onPause();
    }

    @Override
    public void onResume() {
        if (presenter != null)
            presenter.setDormant(false);
        super.onResume();
    }

    @Override
    public void onDetach() {
        if (presenter != null)
            presenter.detachView();
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        presenter = null;
        super.onDestroy();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getViewContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInfo(String info) {
        Toast.makeText(getViewContext(), info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getViewContext() {
        return getActivity().getApplicationContext();
    }

    public static BaseFragment createFragment(Class<? extends BaseFragment> clazz, int sectionNumber, int layoutId) {
        try {
            BaseFragment fragment = clazz.newInstance();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putInt(ARG_LAYOUT_ID, layoutId);
            fragment.setArguments(args);
            return fragment;
        } catch (Exception e) {
            Log.e(TAG, "Error creating fragment");
        }
        return null;
    }
}
