package org.coursera.capstone.androidclient.gotit.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class ObservableBroadcastReceiver extends
		BroadcastReceiver {

	private static final String TAG = ObservableBroadcastReceiver.class.getSimpleName();
	
    private ObservableIntentDataPipe observableIntentData;

	public ObservableBroadcastReceiver() {
		observableIntentData = ObservableIntentDataPipe.getInstance();
	}

	@Override
	public void onReceive(Context context, Intent intent) {
	    if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
	        System.out.println("Boot completed");
	        Toast.makeText(context, "Boot completed! Must sync", Toast.LENGTH_LONG).show();
	        return;
	    }
	    Log.d(TAG, "Notifying presenters with action: " + intent.getAction());
		observableIntentData.setIntent(intent);
	}
}
