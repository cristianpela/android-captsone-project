package org.coursera.capstone.androidclient.gotit.common;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;

import android.util.Log;
import android.view.View.OnAttachStateChangeListener;

//TODO(gotit): make this persistent during config changes, screen rotations? or edit AdroidManifest.xml for that?
public abstract class BasePresenter<V extends GotItView> extends IntentObserver {

    private WeakReference<V> viewReference;

    protected Serializable model;

    private StringBuilder errorReport;

    private boolean dormant;

    public BasePresenter(V view, Serializable model) {
        viewReference = new WeakReference<>(view);
        ObservableIntentDataPipe.getInstance().addObserver(this);
        this.model = model;
        errorReport = new StringBuilder();
    }

    public BasePresenter() {
        this(null, null);
    }

    public BasePresenter(V view) {
        this(view, null);
    }

    public void setView(V view) {
        viewReference = new WeakReference<>(view);
        if (model != null)
            onReatachView();
    }

    public boolean hasCachedModel() {
        return model != null;
    }

    public void setCacheModel(Serializable model) {
        this.model = model;
    }

    public Serializable getCachedModel() {
        return model;
    }

    protected V getView() {
        return viewReference.get();
    }

    protected void onReatachView() {

    }

    public void detachView() {
        viewReference.clear();
        ObservableIntentDataPipe.getInstance().deleteObserver(this);
    }

    public void appendError(String error) {
        errorReport.append(error);
    }

    public void showError() {
        getView().showError(errorReport.toString());
        errorReport.setLength(0);
    }

    public void showError(String error) {
        appendError(error);
        showError();
    }

    public boolean hasErrors() {
        return errorReport.length() > 0;
    }

    protected void debug(String debugMessage) {
        Log.d(this.getClass().getSimpleName(), debugMessage);
    }

    public boolean isDormant() {
        return dormant;
    }

    public void setDormant(boolean dormant) {
        this.dormant = dormant;
    }

    protected String getCurrentLoggedUserName() {
        return new AccountManagerHelper(getView().getViewContext()).getCurrentAccount().name;
    }

    protected boolean isCurrentLoggedUserTeen() {
        return new AccountManagerHelper(getView().getViewContext()).isCurrentAccountTeen();
    }
    
    protected void stopObserving(){
        ObservableIntentDataPipe.getInstance().deleteObserver(this);
    }

}
