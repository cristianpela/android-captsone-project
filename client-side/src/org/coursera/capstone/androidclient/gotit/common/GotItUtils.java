package org.coursera.capstone.androidclient.gotit.common;

import java.util.Calendar;
import java.util.Date;

import org.apache.http.client.utils.DateUtils;

public final class GotItUtils {

    private GotItUtils() {
    }

    public static String getStandardFormatedDate(Date date) {
        return DateUtils.formatDate(date, "MM.dd.yyyy hh:mm");
    }

    public static int getHourOfTheDay(){
        return Calendar.getInstance().get(Calendar.HOUR);
    }
    
    public static int getMinuteOfTheHour(){
        return Calendar.getInstance().get(Calendar.MINUTE);
    }
    
}
