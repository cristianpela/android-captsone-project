package org.coursera.capstone.androidclient.gotit.common;

import java.util.Observable;

import android.content.Intent;

public class ObservableIntentDataPipe extends Observable {
	
	public static final class ObservableIntentDataPipeOnDemand{
		static final ObservableIntentDataPipe INSTANCE = new ObservableIntentDataPipe();
	}
	
	private ObservableIntentDataPipe(){}
	
	public static ObservableIntentDataPipe getInstance(){
		return ObservableIntentDataPipeOnDemand.INSTANCE;
	}

	public void setIntent(Intent intent) {
		synchronized (this) {
			setChanged();
		}
		notifyObservers(intent);
	}
}
