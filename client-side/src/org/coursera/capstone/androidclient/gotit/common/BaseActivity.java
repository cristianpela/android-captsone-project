package org.coursera.capstone.androidclient.gotit.common;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

public class BaseActivity extends ActionBarActivity implements GotItView {

    protected static final String TAG = BaseActivity.class.getSimpleName();

    private BasePresenter<?> presenter;

    private PresenterManager presenterManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // presenterManager = new PresenterManager(getFragmentManager());
    }

    protected void setPresenter(BasePresenter<?> presenter) {
        this.presenter = presenter;
    }

    // TODO(gotit): Better design?
    protected BasePresenter<?> managePresenter(BasePresenter<?> presenter) {
        presenterManager.manage(presenter);
        this.presenter = presenter;
        return this.presenter;
    }

    @Override
    protected void onPause() {
        if (presenter != null)
            presenter.setDormant(true);
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (presenter != null)
            presenter.setDormant(false);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // presenterManager.saveModel(presenter.getCachedModel());
        if (presenter != null)
            presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInfo(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getViewContext() {
        return getApplicationContext();
    }
 
}
