package org.coursera.capstone.androidclient.gotit.common;

import java.io.IOException;
import java.io.InputStream;

import org.coursera.capstone.androidclient.gotit.rest.GotItConnection;

import android.content.Context;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class SecuredImageDownloader extends BaseImageDownloader {

    public SecuredImageDownloader(Context context) {
        super(context);
    }

    public SecuredImageDownloader(Context context, int connectTimeout, int readTimeout) {
        super(context, connectTimeout, readTimeout);
    }

    @Override
    protected InputStream getStreamFromOtherSource(String imageUri, Object extra) throws IOException {
        return GotItConnection.getInstance().getRESTInterface().getAvatarData(imageUri).getBody().in();
    }
        
}
