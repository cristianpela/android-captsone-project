package org.coursera.capstone.androidclient.gotit.common;

import android.content.Context;

public interface GotItView {
    
    public void showError(String error);
    
    public void showInfo(String info);
    
    public Context getViewContext();
    
}
