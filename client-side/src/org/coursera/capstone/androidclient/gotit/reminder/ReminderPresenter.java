package org.coursera.capstone.androidclient.gotit.reminder;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;

import android.content.Intent;

public class ReminderPresenter extends BasePresenter<ReminderView> {

    private ReminderStorage reminderStorage;

    private List<Integer> hours;

    public ReminderPresenter(ReminderView view) {
        super(view);
        reminderStorage = new ReminderStorage(getView().getViewContext(), getCurrentLoggedUserName());
        hours = reminderStorage.loadHours();
        getView().onReminderStorageLoaded(hours);
    }

    public List<Integer> getHours() {
        return hours;
    }
    
    public void saveHours(){
        debug(hours.toString());
        reminderStorage.saveHours(hours);
    }

    @Override
    protected void onReturnAction(Intent intent) {
    }

}
