package org.coursera.capstone.androidclient.gotit.reminder;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.GotItView;

public interface ReminderView extends GotItView {
    
    public void onReminderStorageLoaded(List<Integer> reminderHours);
}
