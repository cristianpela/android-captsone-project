package org.coursera.capstone.androidclient.gotit.reminder;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;

public class ReminderListAdapter extends BaseAdapter {

    private LayoutInflater inflater;

    private List<Integer> hours;

    public ReminderListAdapter(Context context, List<Integer> hours) {
        inflater = LayoutInflater.from(context);
        this.hours = hours;
    }

    @Override
    public int getCount() {
        return hours.size();
    }

    @Override
    public Object getItem(int position) {
        return hours.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder = null;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_reminder_item, parent, false);
            holder = new Holder();
            view.setTag(holder);
            holder.picker = (NumberPicker) view.findViewById(R.id.numReminderHour);
            holder.picker.setMinValue(0);
            holder.picker.setMaxValue(23);
            holder.reminderRemove = (Button) view.findViewById(R.id.btnReminderRemove);
        } else
            holder = (Holder) view.getTag();
        holder.picker.setValue(hours.get(position));
        holder.reminderRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hours.remove(position);
                notifyDataSetChanged();
            }
        });
        holder.picker.setOnValueChangedListener(new OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hours.remove(position);
                hours.add(position, newVal);
            }
        });
        return view;
    }
    
    public void addHour(){
        hours.add(0);
        notifyDataSetChanged();
    }

    private static class Holder {
        NumberPicker picker;
        Button reminderRemove;
    }

}
