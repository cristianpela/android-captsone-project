package org.coursera.capstone.androidclient.gotit.reminder;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseFragment;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class ReminderFragment extends BaseFragment implements ReminderView {

    private ReminderPresenter presenter;

    private ListView listReminder;

    private ReminderListAdapter adapterReminder;

    private Button btnAddReminder, btnSaveReminder;

    public ReminderFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ReminderPresenter(this);
        setPresenter(presenter);
    }
    
    @Override
    protected void onViewInflated(View rootView) {
        listReminder = (ListView) rootView.findViewById(R.id.listReminder);
        btnAddReminder = (Button) rootView.findViewById(R.id.btnReminderAdd);
        btnAddReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterReminder.addHour();
            }
        });
        btnSaveReminder = (Button) rootView.findViewById(R.id.btnReminderSave);
        btnSaveReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.saveHours();
            }
        });
        
        adapterReminder = new ReminderListAdapter(getActivity(), presenter.getHours());
        listReminder.setAdapter(adapterReminder);
    }

    @Override
    public void onReminderStorageLoaded(List<Integer> reminderHours) {
       
    }

}
