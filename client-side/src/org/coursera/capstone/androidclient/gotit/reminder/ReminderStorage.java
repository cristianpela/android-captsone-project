package org.coursera.capstone.androidclient.gotit.reminder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class ReminderStorage {

    private static final String KEY_SUFFIX = "_reminder";

    private SharedPreferences storage;

    private String key;

    public ReminderStorage(Context context, String teenUserName) {
        storage = PreferenceManager.getDefaultSharedPreferences(context);
        key = teenUserName + KEY_SUFFIX;
    }

    public List<Integer> loadHours() {
        Set<String> storedSet = storage.getStringSet(key, null);
        return (storedSet == null) ? defaultHours() : prepareToLoad(storedSet);
    }

    private List<Integer> defaultHours() {
        List<Integer> defaultHours = new ArrayList<Integer>(Arrays.asList(new Integer[]{9, 15, 22}));
        return defaultHours;
    }

    public void saveHours(List<Integer> hoursToSave) {
        Set<String> hoursSet = prepareToSave((hoursToSave.isEmpty() ? defaultHours() : hoursToSave));
        storage.edit().putStringSet(key, hoursSet).commit();
    }

    private Set<String> prepareToSave(List<Integer> hours) {
        Set<String> set = new HashSet<String>();
        for (Integer hour : hours) {
            set.add(Integer.toString(hour));
        }
        return set;
    }

    private List<Integer> prepareToLoad(Set<String> storedSet) {
        List<Integer> hours = new ArrayList<Integer>();
        for (String storedHour : storedSet) {
            hours.add(Integer.parseInt(storedHour));
        }
        return hours;
    }
}
