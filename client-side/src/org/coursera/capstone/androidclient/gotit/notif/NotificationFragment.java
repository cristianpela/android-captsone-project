package org.coursera.capstone.androidclient.gotit.notif;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseFragment;
import org.coursera.capstone.androidclient.gotit.rest.NotificationType;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

public class NotificationFragment extends BaseFragment implements NotificationView, OnClickListener {

    private NotificationPresenter presenter;

    private Button btnAknowledge;

    private ListView listNotifFollowReq, listNotifCheckInReq;
    private NotificationListAdapter listNotifFollowReqAdapter, listNotifCheckInReqAdapter;

    public NotificationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new NotificationPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        btnAknowledge = (Button) view.findViewById(R.id.btnNotifAcknod);
        btnAknowledge.setOnClickListener(this);
        listNotifCheckInReq = (ListView) view.findViewById(R.id.listNotifCheckInReq);
        listNotifFollowReq = (ListView) view.findViewById(R.id.listNotifFollowReq);
        return view;
    }

    @Override
    public void onNotificationsComplete() {
        listNotifCheckInReqAdapter = new NotificationListAdapter(getViewContext(), NotificationType.CHECK_IN_REQUEST,
                presenter);
        listNotifFollowReqAdapter = new NotificationListAdapter(getViewContext(), NotificationType.FOLLOW_REQUEST,
                presenter);
        listNotifCheckInReq.setAdapter(listNotifCheckInReqAdapter);
        listNotifFollowReq.setAdapter(listNotifFollowReqAdapter);
        if(listNotifFollowReqAdapter.isEmpty())
            btnAknowledge.setVisibility(View.GONE);
        
    }

    @Override
    public void onClick(View v) {
        presenter.acknowledgeCheckInRequests();
        if(listNotifFollowReqAdapter.isEmpty())
            btnAknowledge.setVisibility(View.GONE);
    }

    @Override
    public void onFollowConfirmed(String followerToBeConfirmed) {
        listNotifFollowReqAdapter.remove(followerToBeConfirmed);
    }

    @Override
    public void onAcknowledgeCheckInRequests() {
        listNotifCheckInReqAdapter.removeAll();
    }

}
