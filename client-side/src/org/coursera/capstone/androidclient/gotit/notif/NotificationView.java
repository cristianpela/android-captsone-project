package org.coursera.capstone.androidclient.gotit.notif;

import org.coursera.capstone.androidclient.gotit.common.GotItView;

public interface NotificationView extends GotItView {

    void onNotificationsComplete();

    void onFollowConfirmed(String followerToBeConfirmed);

    void onAcknowledgeCheckInRequests();

}
