package org.coursera.capstone.androidclient.gotit.notif;

import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.profile.FollowingCommand;
import org.coursera.capstone.androidclient.gotit.rest.Notification;
import org.coursera.capstone.androidclient.gotit.rest.NotificationType;

import android.content.Intent;
import android.os.Bundle;

public class NotificationPresenter extends BasePresenter<NotificationView> {

    private List<Notification> checkInRequests, followRequests;

    private String followerToBeConfirmed;

    public NotificationPresenter(NotificationView view) {
        super(view);
        InteractorIntentServiceHelper.getInstance().returnAction(InteractorIntentServiceHelper.ACTION_NOTIFICATION)
                .setProcessingTarget(NotificationCommand.class.getName()).startService();
        checkInRequests = new ArrayList<Notification>();
        followRequests = new ArrayList<Notification>();
    }

    @Override
    protected void onReturnAction(Intent intent) {
        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_NOTIFICATION)) {
            if (intent.getExtras().getBoolean(NotificationCommand.KEY_ACKNOWLEDGE_NOTIFICATIONS)) {
                getView().onAcknowledgeCheckInRequests();
            } else {
                @SuppressWarnings("unchecked")
                List<Notification> notifications = (List<Notification>) intent.getExtras().get(
                        NotificationCommand.KEY_NOTIFICATIONS);
                if (notifications != null)
                    sendDataToView(notifications);
            }
        }

        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_REQUEST_FOLLOW)) {
            getView().onFollowConfirmed(followerToBeConfirmed);
            followerToBeConfirmed = null;
        }

    }

    private void sendDataToView(List<Notification> notifications) {
        for (Notification notification : notifications) {
            if (notification.getType() == NotificationType.CHECK_IN_REQUEST)
                checkInRequests.add(notification);
            else
                followRequests.add(notification);
        }
        getView().onNotificationsComplete();
    }

    public List<Notification> getCheckInRequests() {
        return checkInRequests;
    }

    public List<Notification> getFollowRequests() {
        return followRequests;
    }

    public void acknowledgeCheckInRequests() {
        Bundle extras = new Bundle();
        extras.putBoolean(NotificationCommand.KEY_ACKNOWLEDGE_NOTIFICATIONS, true);
        InteractorIntentServiceHelper.getInstance().returnAction(InteractorIntentServiceHelper.ACTION_NOTIFICATION)
                .setProcessingTarget(NotificationCommand.class.getName()).addProcessingArgs(extras).startService();
    }

    public void confirmFollower(String followerUserName) {
        followerToBeConfirmed = followerUserName;
        Bundle extras = new Bundle();
        extras.putString(FollowingCommand.KEY_CONFIRM_FOLLOWER_USER_NAME, followerUserName);
        InteractorIntentServiceHelper.getInstance().addProcessingArgs(extras)
                .returnAction(InteractorIntentServiceHelper.ACTION_REQUEST_FOLLOW)
                .setProcessingTarget(FollowingCommand.class.getName()).startService();
        ;
    }
}
