package org.coursera.capstone.androidclient.gotit.notif;

import java.util.List;

import org.apache.http.client.utils.DateUtils;
import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.rest.Notification;
import org.coursera.capstone.androidclient.gotit.rest.NotificationType;

import android.content.Context;
import android.opengl.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class NotificationListAdapter extends BaseAdapter {

    private List<Notification> notifications;

    private LayoutInflater inflater;

    private NotificationPresenter presenter;

    private NotificationType notificationType;

    public NotificationListAdapter(Context context, NotificationType notificationType, NotificationPresenter presenter) {
        this.notifications = (notificationType == NotificationType.CHECK_IN_REQUEST) ? presenter.getCheckInRequests()
                : presenter.getFollowRequests();
        inflater = LayoutInflater.from(context);
        this.presenter = presenter;
        this.notificationType = notificationType;
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    /**
     * Returns the follower user name and not the whole Notification Object
     */
    @Override
    public Object getItem(int position) {
        return notifications.get(position).getFromUserName();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove(String followerUserName) {
        Notification toRemove = null;
        for (Notification notification : notifications) {
            if (notification.getFromUserName().equals(followerUserName))
                toRemove = notification;
        }
        notifications.remove(toRemove);
        notifyDataSetChanged();
    }

    public void removeAll() {
        notifications.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_notification_item, parent, false);
            holder = new Holder();
            view.setTag(holder);
            holder.txtDate = (TextView) view.findViewById(R.id.textNotifDate);
            holder.txtFullName = (TextView) view.findViewById(R.id.textNotifFullName);
            holder.btnAprove = (Button) view.findViewById(R.id.btnNotifApprove);
        } else
            holder = (Holder) view.getTag();

        Notification notification = notifications.get(position);
        if (notificationType == NotificationType.CHECK_IN_REQUEST)
            holder.btnAprove.setVisibility(View.GONE);
        holder.txtDate.setText(DateUtils.formatDate(notification.getDate(), DateUtils.PATTERN_ASCTIME));
        holder.txtFullName.setText(notification.getFromFullName());
        holder.btnAprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                presenter.confirmFollower(getItem(position).toString());
            }
        });
        return view;
    }

    private static class Holder {
        TextView txtDate, txtFullName;
        Button btnAprove;
    }
}
