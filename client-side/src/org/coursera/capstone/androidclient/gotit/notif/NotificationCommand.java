package org.coursera.capstone.androidclient.gotit.notif;

import java.io.Serializable;

import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;

import android.os.Bundle;

public class NotificationCommand implements ProcessingCommand {

    public static final String KEY_NOTIFICATIONS = "key_notifications";

    public static final String KEY_ACKNOWLEDGE_NOTIFICATIONS = "key_ack_notifications";

    public NotificationCommand() {
    }

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        try {
            if (incomeParams.getBoolean(KEY_ACKNOWLEDGE_NOTIFICATIONS)) {
                repository.acknowledgeCheckInRequests();
                outcomeParams.putBoolean(KEY_ACKNOWLEDGE_NOTIFICATIONS, true);
            } else
                outcomeParams.putSerializable(KEY_NOTIFICATIONS, (Serializable) repository.getNotifications());
        } catch (Exception ex) {
            outcomeParams.putString(KEY_ERROR, "Error while processing notifications:\n" + ex.getMessage());
        }
    }

}
