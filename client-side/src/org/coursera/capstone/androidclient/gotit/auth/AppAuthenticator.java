package org.coursera.capstone.androidclient.gotit.auth;

import java.io.IOException;

import org.coursera.capstone.androidclient.gotit.rest.GotItConnection;
import org.coursera.capstone.androidclient.gotit.rest.SecuredRestException;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AppAuthenticator extends AbstractAccountAuthenticator {

    protected static final String TAG = AppAuthenticator.class.getSimpleName();

    private Context context;

    public AppAuthenticator(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        return null;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType,
            String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        Bundle result = new Bundle();
        Intent intent = new Intent(context, AppAuthenticatorActivity.class).putExtra(
                AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response).putExtra("TOKEN_TYPE", authTokenType).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        result.putParcelable(AccountManager.KEY_INTENT, intent);
        return result;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options)
            throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType,
            Bundle options) throws NetworkErrorException {
        
        Bundle result = new Bundle();
        GotItConnection service = GotItConnection.getInstance();
        String userName = account.name;
        String password = AccountManager.get(context).getPassword(account);
        Log.d(TAG, "Getting token: "+ userName+" "+password);
        if (password != null) {
            String authToken;
            try {
                authToken = service.logIn(userName, password);
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, AppAuthenticatorConstants.ACCOUNT_TYPE);
                result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                return result;

            } catch (SecuredRestException | IOException e) {
                e.printStackTrace();
            }
        }
        // something went wrong must show the login activity;
        Intent intent = new Intent(context, AppAuthenticatorActivity.class).putExtra(
                AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response).putExtra(
                AppAuthenticatorActivity.PARAM_USER_NAME, userName);
        result.putParcelable(AccountManager.KEY_INTENT, intent);
        return result;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType,
            Bundle options) throws NetworkErrorException {
        Bundle result = new Bundle();
        Intent intent = new Intent(context, AppAuthenticatorActivity.class).putExtra(
                AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response).putExtra("TOKEN_TYPE", authTokenType);
        result.putParcelable(AccountManager.KEY_INTENT, intent);
        return result;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features)
            throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAccountRemovalAllowed(AccountAuthenticatorResponse response, Account account)
            throws NetworkErrorException {
        return super.getAccountRemovalAllowed(response, account);
    }

    
}
