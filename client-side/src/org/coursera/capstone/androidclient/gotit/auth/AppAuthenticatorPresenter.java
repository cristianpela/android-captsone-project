package org.coursera.capstone.androidclient.gotit.auth;

import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.rest.User;

import static org.coursera.capstone.androidclient.gotit.common.ProcessingCommand.*;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

public class AppAuthenticatorPresenter extends BasePresenter<AppAuthenticatorView> {

    private static final String PROCESSING_COMMAND_TARGET = "org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorCommand";

    protected static final String TAG = AppAuthenticatorPresenter.class.getSimpleName();

    private String userName, password;

    public AppAuthenticatorPresenter(AppAuthenticatorView view) {
        super(view);
    }

    public void logIn(String userName, String password) {
        this.userName = userName;
        this.password = password;

        if (!areFieldsValid()) {
            getView().onInvalidFields();
            return;
        }

        Bundle args = new Bundle();
        args.putString(KEY_USER_NAME, userName);
        args.putString(KEY_PASSWORD, password);
        InteractorIntentServiceHelper.getInstance().setProcessingTarget(PROCESSING_COMMAND_TARGET)
                .addProcessingArgs(args).returnAction(AppAuthenticatorConstants.LOG_IN_ACTION).startService();
    }

    @Override
    protected void onReturnAction(Intent intent) {
        if (intent.getAction().equalsIgnoreCase(AppAuthenticatorConstants.LOG_IN_ACTION)) {
            String error = intent.getExtras().getString(KEY_ERROR);
            if (error != null)
                getView().onLoginFailure(error);
            else {
                userName = intent.getExtras().getString(KEY_USER_NAME);
                password = intent.getExtras().getString(KEY_PASSWORD);
                AccountManagerHelper accHelper = new AccountManagerHelper(getView().getViewContext());
                if (!accHelper.isCurrentAccountTypeStored(userName))
                    InteractorIntentServiceHelper.PredefinedInteractions.find(userName, false);
                else{
                    getView().onLoginSuccess(userName, password);
                    stopObserving();
                }
            }
        }

        if (intent.getAction().equals(InteractorIntentServiceHelper.ACTION_SEARCH)) {
            @SuppressWarnings("unchecked")
            List<User> users = (List<User>) intent.getExtras().get(KEY_USERS);
            User user = users.get(0);
            AccountManagerHelper accHelper = new AccountManagerHelper(getView().getViewContext());
            accHelper.storeCurrentAccountType(userName, user.isTeen());
            getView().onLoginSuccess(userName, password);
            stopObserving();
        }
        
        
    }

    private boolean areFieldsValid() {
        return !TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password);
    }

}
