package org.coursera.capstone.androidclient.gotit.auth;

import org.coursera.capstone.androidclient.gotit.common.GotItView;

import android.graphics.Bitmap;

public interface SignUpView extends GotItView {

    public void onBirthDateChosen(String formatedDate);

    public void changeProfilePicture(Bitmap result);
    
    public void onSignUpComplete();

}
