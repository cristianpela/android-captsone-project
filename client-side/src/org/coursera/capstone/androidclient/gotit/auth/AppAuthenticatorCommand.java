package org.coursera.capstone.androidclient.gotit.auth;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.os.Bundle;
import android.util.Log;

public class AppAuthenticatorCommand implements ProcessingCommand {

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        if (requestAction.equals(InteractorIntentServiceHelper.ACTION_LOG_IN)) {
            logIn(incomeParams, outcomeParams, repository);
        } else if (requestAction.equals(InteractorIntentServiceHelper.ACTION_SIGN_UP)) {
            signUp(incomeParams, outcomeParams, repository);
        }
    }

    private void signUp(Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        User user = (User) incomeParams.getSerializable(KEY_USERS);
        try {
            repository.signUp(user);
        } catch (Exception ex) {
            outcomeParams.putString(KEY_ERROR, "Error: \n" + ex.getMessage());
        }
    }

    private void logIn(Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        String userName = incomeParams.getString(KEY_USER_NAME);
        String password = incomeParams.getString(KEY_PASSWORD);
        try {
            repository.logIn(userName, password);
            outcomeParams.putString(KEY_USER_NAME, userName);
            outcomeParams.putString(KEY_PASSWORD, password);
        } catch (Exception ex) {
            outcomeParams.putString(KEY_ERROR, "User or pasword are incorrect! \n" + ex.getMessage());
        }
    }

}
