package org.coursera.capstone.androidclient.gotit.auth;

import android.accounts.AbstractAccountAuthenticator;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AppAuthenticatorService extends Service {
    
    private AbstractAccountAuthenticator authenticator;

    @Override
    public void onCreate() {
        authenticator = new AppAuthenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder() ;
    }

}
