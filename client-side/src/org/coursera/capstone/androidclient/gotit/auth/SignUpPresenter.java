package org.coursera.capstone.androidclient.gotit.auth;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorCallback;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.DatePicker;

public class SignUpPresenter extends BasePresenter<SignUpView> implements OnDateSetListener,
        InteractorCallback<String, Bitmap> {

    private User userModel;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
    private Calendar calendar = Calendar.getInstance();
    private InteractorIntentServiceHelper interactor = InteractorIntentServiceHelper.getInstance();

    public SignUpPresenter(SignUpView view, Serializable model) {
        super(view, model);
        userModel = new User();
    }

    public SignUpPresenter(SignUpView view) {
        super(view);
        userModel = new User();
    }

    @Override
    protected void onReturnAction(Intent intent) {
        if (intent.getAction().equals(InteractorIntentServiceHelper.ACTION_SIGN_UP)) {
            getView().onSignUpComplete();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (isDateValid(year, monthOfYear, dayOfMonth)) {
            String formatedDate = (monthOfYear + 1) + "/" + dayOfMonth + "/" + year;
            userModel.getUserDetails().setBirthDate(parseDate(formatedDate));
            getView().onBirthDateChosen(formatedDate);
        } else {
            showError();
        }
    }

    private Date parseDate(String date) {
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bitmap doInBackground(String source) {
        Bitmap bitmap = BitmapFactory.decodeFile(source);
        return ThumbnailUtils.extractThumbnail(bitmap, 64, 64);
    }

    @Override
    public void onPostExecute(Bitmap result) {
        getView().changeProfilePicture(result);
    }

    public void changeProfilePicture(Uri uri) {
        String profilePicturePath = getProfilePicturePath(uri);
        userModel.setImageProfilePath(profilePicturePath);
        interactor.startAsyncService(this, profilePicturePath);
    }

    private String getProfilePicturePath(Uri imgUri) {
        String pathColumn = MediaStore.Images.ImageColumns.DATA;
        Cursor cursor = getView().getViewContext().getContentResolver()
                .query(imgUri, new String[] { pathColumn }, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            String path = cursor.getString(cursor.getColumnIndex(pathColumn));
            cursor.close();
            return path;
        }
        return null;
    }

    public void signUp(String userName, String password, String firstName, String lastName, boolean isTeen,
            String birthDate, String mrn) {
        userModel.setTeen(isTeen);
        boolean hasError = checkForEmptyFields(userName, password, firstName, lastName, birthDate, mrn);
        if (!hasError) {
            Bundle extras = new Bundle();
            userModel.setUserName(userName);
            userModel.setUserName(userName);
            userModel.setPassword(password);
            userModel.setTeen(isTeen);
            userModel.getUserDetails().setFirstName(firstName);
            userModel.getUserDetails().setLastName(lastName);
            userModel.getUserDetails().setMedicalRecordNumber((isTeen) ? Long.parseLong(mrn) : -1);

            extras.putSerializable(AppAuthenticatorCommand.KEY_USERS, userModel);
            interactor.setProcessingTarget(AppAuthenticatorCommand.class.getName()).addProcessingArgs(extras)
                    .returnAction(InteractorIntentServiceHelper.ACTION_SIGN_UP).startService();
        } else {
            showError();
        }

    }

    private boolean isDateValid(int birthYear, int monthOfYear, int dayOfMonth) {

        calendar.set(birthYear, monthOfYear, dayOfMonth);
        long currentTime = System.currentTimeMillis();
        if (calendar.getTime().getTime() > System.currentTimeMillis()) {
            appendError("We're sorry, but time travelers are not accepted!");
            return false;
        }

        calendar.setTimeInMillis(currentTime);
        int currentYear = calendar.get(Calendar.YEAR);
        if (currentYear - birthYear > 20) {
            appendError("You're too old to be a teen anymore!");
            return false;
        }

        return true;
    }

    private boolean checkForEmptyFields(String userName, String password, String firstName, String lastName,
            String birthDate, String mrn) {
        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password) || TextUtils.isEmpty(firstName)
                || TextUtils.isEmpty(lastName) || (userModel.isTeen() && TextUtils.isEmpty(birthDate))
                || (userModel.isTeen() && TextUtils.isEmpty(mrn))) {
            appendError("There are empty fields!");
            return true;
        }
        return false;
    }
}
