package org.coursera.capstone.androidclient.gotit.auth;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.Signer;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.common.PresenterManager;
import org.coursera.capstone.androidclient.gotit.rest.User;
import org.coursera.capstone.androidclient.gotit.rest.UserDetails;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class SignUpActivity extends BaseActivity implements SignUpView {

    private static final int DATE_DIALOGC_CODE = 8008;

    private static final int PICTURE_PROFILE_CODE = 1001;

    private int dayOfMonth, monthOfYear, year;

    private SignUpPresenter presenter;

    private EditText editUserName, editPassword, editFirstName, editLastName, editMrn;
    private Button buttonBirthDate;
    private ImageView imageProfile;
    private TextView txtBirthDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        presenter = new SignUpPresenter(this);
        setPresenter(presenter);
        initFields();
    }

    private void initFields() {
        editUserName = (EditText) findViewById(R.id.editUserName);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editMrn = (EditText) findViewById(R.id.editMrn);

        buttonBirthDate = (Button) findViewById(R.id.buttonBirthDate);
        txtBirthDate = (TextView) findViewById(R.id.txtBirthDate);
        imageProfile = (ImageView) findViewById(R.id.imageProfile);
    }

    public void actionTeenCheck(View checkTeen) {
        int visibility = (((CheckBox) checkTeen).isChecked()) ? View.VISIBLE : View.GONE;
        buttonBirthDate.setVisibility(visibility);
        txtBirthDate.setVisibility(visibility);
        editMrn.setVisibility(visibility);
    }

    public void actionSignUp(View button) {
        boolean isTeen = editMrn.getVisibility() == View.VISIBLE;
        presenter.signUp(editUserName.getText().toString(),
                         editPassword.getText().toString(),
                         editFirstName.getText().toString(),
                         editLastName.getText().toString(),
                         isTeen, 
                         txtBirthDate.getText().toString(),
                         editMrn.getText().toString());
    }

   
    /**
     * 
     *  BIRTH DAY CHOOSING SECTION
     * 
     */

    
    @SuppressWarnings("deprecation")
    public void actionOpenDialog(View button) {
        showDialog(DATE_DIALOGC_CODE);
    }

    // TODO(gotit): implement a dialog fragment instead?
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DATE_DIALOGC_CODE)
            return new DatePickerDialog(this, presenter, year, monthOfYear, dayOfMonth);
        return null;
    }

    @Override
    public void onBirthDateChosen(String formatedDate) {
        txtBirthDate.setText(formatedDate);
    }
    
    /**
     * 
     *  PROFILE PICTURE SECTION
     * 
     */

    public void actionChangeProfilePicture(View image) {
        startActivityForResult(new Intent(Intent.ACTION_PICK).setType("image/*"), PICTURE_PROFILE_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICTURE_PROFILE_CODE)
           presenter.changeProfilePicture(data.getData());
    }
    
    @Override
    public void changeProfilePicture(Bitmap result) {
        imageProfile.setImageBitmap(result);
    }

    @Override
    public void onSignUpComplete() {
        new AccountManagerHelper(this).addOrUpdateAccount(null, null);
        finish();
    }

}
