package org.coursera.capstone.androidclient.gotit.auth;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.main.DashboardActivity;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AppAuthenticatorActivity extends AccountAuthenticatorActivity implements AppAuthenticatorView {

    public static final String PARAM_USER_NAME = "user_name";

    protected static final String TAG = AppAuthenticatorActivity.class.getName();

    private static final int FINISHED_CODE = 1337;

    private AppAuthenticatorPresenter presenter;

    private EditText userNameEdit;
    private EditText passwordEdit;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        presenter = new AppAuthenticatorPresenter(this);
        setContentView(R.layout.activity_log_in);
        userNameEdit = (EditText) findViewById(R.id.txtUsername);
        passwordEdit = (EditText) findViewById(R.id.txtPassword);
        userNameEdit.setText("user0");
        passwordEdit.setText("pass");
        loginButton = (Button) findViewById(R.id.btnLogin);

    }

    public void loginAction(View button) {
        loginButton.setEnabled(false);
        presenter.logIn(userNameEdit.getText().toString(), passwordEdit.getText().toString());
    }

    public void signUpAction(View button) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    public void onLoginSuccess(String userName, String password) {
        loginButton.setEnabled(true);
        AccountManagerHelper managerHelper = new AccountManagerHelper(this);
        managerHelper.addOrUpdateAccount(userName, password);

        Intent intent = new Intent();
        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, password);
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AppAuthenticatorConstants.ACCOUNT_TYPE);
        setAccountAuthenticatorResult(intent.getExtras());
        setResult(RESULT_OK, intent);

        
        startActivityForResult(new Intent(this, DashboardActivity.class).addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK)
                , FINISHED_CODE);
       
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == FINISHED_CODE)
            finish();
    }

    @Override
    public void onLoginFailure(String error) {
        loginButton.setEnabled(true);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInvalidFields() {
        Toast.makeText(this, "Some fields are empty", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finish() {
        presenter.detachView();
        super.finish();
    }

    @Override
    public void showError(String error) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void showInfo(String info) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Context getViewContext() {
        return getApplicationContext();
    }

}
