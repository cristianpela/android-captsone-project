package org.coursera.capstone.androidclient.gotit.auth;

public final class AppAuthenticatorConstants {

    private AppAuthenticatorConstants() {
    }

    public static final String ACCOUNT_TYPE = "org.coursera.gotit.diab.mngmnt.acc";
    
    public static final String AUTH_TOKEN_TYPE = "gotit";
    
    public static final String LOG_IN_ACTION = "org.coursera.gotit.diab.mngmnt.action.LOG_IN";
    
    public static final String AUTHORITY = "org.coursera.gotit.diab.mngmnt.authority";
    
    public static final String CURRENT_LOGGED_USER = "current_logged_user";
    
}
