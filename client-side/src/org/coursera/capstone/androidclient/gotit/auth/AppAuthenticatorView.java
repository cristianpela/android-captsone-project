package org.coursera.capstone.androidclient.gotit.auth;

import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.rest.User;

public interface AppAuthenticatorView extends GotItView {
    
    public void onLoginSuccess(String userName, String password);

    public void onInvalidFields();

    public void onLoginFailure(String error);

}
