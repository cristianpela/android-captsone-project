package org.coursera.capstone.androidclient.gotit.auth;

import static org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants.ACCOUNT_TYPE;
import static org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants.AUTHORITY;
import static org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants.AUTH_TOKEN_TYPE;
import static org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants.CURRENT_LOGGED_USER;

import java.lang.ref.WeakReference;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

//DONE(gotit): design this class better please
public class AccountManagerHelper {

    // private static final Object lock = new Object();

    private WeakReference<Context> contextRef;

    private WeakReference<Activity> activityRef;

    private AccountManager accountManager;

    private SharedPreferences prefs;

    public AccountManagerHelper(Context context) {
        if (context instanceof Activity)
            activityRef = new WeakReference<Activity>((Activity) context);
        contextRef = new WeakReference<Context>(context);
        accountManager = AccountManager.get(contextRef.get());
        prefs = PreferenceManager.getDefaultSharedPreferences(contextRef.get());
    }

    public void addOrUpdateAccount(String userName, String password) {
        Account account = getAccountByName(userName);
        if (account != null) {
            accountManager.setPassword(account, password);
            storeAccountName(userName);
        } else {
            if (userName == null) {
                // force login form to show
                storeAccountName(null);
                accountManager.addAccount(ACCOUNT_TYPE, AUTH_TOKEN_TYPE, null, null, activityRef.get(), null, null);
            } else {
                // NOTE(gotit): this order is crucial in order that sync to
                // work;
                account = new Account(userName, ACCOUNT_TYPE);
                ContentResolver.setIsSyncable(account, AUTHORITY, 1);
                ContentResolver.setSyncAutomatically(account, AUTHORITY, true);
                accountManager.addAccountExplicitly(account, password, null);
                storeAccountName(userName);
            }
        }
    }

    public void logIn(AccountManagerCallback<Bundle> callback) {
        Account account = getCurrentAccount();
        checkCredentials(account, callback);
    }

    public void logOut(AccountManagerCallback<Bundle> callback) {
        Account account = getCurrentAccount();
        accountManager.setPassword(account, null);
        checkCredentials(account, callback);
        storeAccountName(account.name);
    }

    private void checkCredentials(Account account, AccountManagerCallback<Bundle> callback) {
        accountManager.invalidateAuthToken(ACCOUNT_TYPE, accountManager.peekAuthToken(account, AUTH_TOKEN_TYPE));
        accountManager.getAuthToken(account, AUTH_TOKEN_TYPE, null, activityRef.get(), callback, null);
    }

    public boolean existsAccounts() {
        return accountManager.getAccountsByType(ACCOUNT_TYPE).length > 0;
    }

    public Account getCurrentAccount() {
        String currentUserName = prefs.getString(CURRENT_LOGGED_USER, null);
        Account account = getAccountByName(currentUserName);
        if (account == null) {
            account = getDefaultAccount();
            if (account != null)
                synchronized (this) {
                    storeAccountName(account.name);
                }
        }
        return account;
    }

    public Account getAccountByName(String userName) {
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        for (Account account : accounts) {
            if (account.name.equals(userName))
                return account;
        }
        return null;
    }

    public boolean existsAccount(Account account) {
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        for (Account acc : accounts) {
            if (acc.name.equals(account.name))
                return true;
        }
        return false;
    }

    @SuppressWarnings("deprecation")
    public void removeAllAccounts() {
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        for (Account account : accounts) {
            accountManager.removeAccount(account, null, null);
        }
        storeAccountName(null);
    }

    private Account getDefaultAccount() {
        return (existsAccounts()) ? accountManager.getAccountsByType(ACCOUNT_TYPE)[0] : null;
    }

    private void storeAccountName(String name) {
        prefs.edit().putString(CURRENT_LOGGED_USER, name).commit();
    }

    /**
     * by type means here teen or follower not the general application
     * ACCOUNT_TYPE
     */
    boolean isCurrentAccountTypeStored(String name) {
        return prefs.getString(name, null) != null;
    }

    void storeCurrentAccountType(String name, boolean isTeen) {
        prefs.edit().putString(name, (isTeen) ? "teen" : "follower").commit();
    }

    public synchronized boolean isCurrentAccountTeen() {
        return prefs.getString(getCurrentAccount().name, null).equals("teen");
    }

    public void requestSync() {
        Account account = getCurrentAccount();
        Bundle extras = new Bundle();
        extras.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(account, AUTHORITY, extras);
    }

}
