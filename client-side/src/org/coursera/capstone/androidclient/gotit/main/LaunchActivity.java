package org.coursera.capstone.androidclient.gotit.main;

import java.io.IOException;

import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInActivity;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.sync.GotItSyncBroadcastReceiver;

import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class LaunchActivity extends Activity {

    private static String ACTIVITY_TO_LAUNCH = "activity_to_launch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final AccountManagerHelper managerHelper = new AccountManagerHelper(this);
        if (!managerHelper.existsAccounts()) {
            managerHelper.addOrUpdateAccount(null, null);
            finish();
        } else {
            managerHelper.logIn(new AccountManagerCallback<Bundle>() {
                @Override
                public void run(AccountManagerFuture<Bundle> future) {
                    try {
                        if (future.getResult() != null) {
                            sendBroadcast(new Intent(GotItSyncBroadcastReceiver.SYNC_AUTO));
                            launchActivity(managerHelper);
                            finish();
                        }
                    } catch (OperationCanceledException | AuthenticatorException | IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    // TODO(gotit) make a baseactivity startactivity "general". For now it
    // launch either dashboard either checkin..
    private void launchActivity(AccountManagerHelper managerHelper) {
        String activityName = getIntent().getStringExtra(ACTIVITY_TO_LAUNCH);
        if (activityName == null) {
            startActivity(new Intent(this, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK));
        } else {
            startActivity(CheckInActivity.startingIntent(this, managerHelper.getCurrentAccount().name));
        }
    }

    public static Intent securedIntent(Context context, Class<? extends BaseActivity> activityClass) {
        return new Intent(context, LaunchActivity.class).putExtra(ACTIVITY_TO_LAUNCH, activityClass.getName())
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    }
}
