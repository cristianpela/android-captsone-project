package org.coursera.capstone.androidclient.gotit.main;

import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.rest.User;

public interface TestView extends GotItView {

    public void showUser(User user);
    
}
