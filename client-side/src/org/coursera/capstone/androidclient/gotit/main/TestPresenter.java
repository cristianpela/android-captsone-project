package org.coursera.capstone.androidclient.gotit.main;

import java.io.Serializable;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.find.FindUserCommand;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.content.Intent;
import android.os.Bundle;

public class TestPresenter extends BasePresenter<TestView> {

    public TestPresenter(TestView view, Serializable model) {
        super(view, model);
        if(hasCachedModel()){
            getView().showUser((User)getCachedModel());
        }
    }
    
   
    public TestPresenter(TestView view) {
        super(view);
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onReturnAction(Intent intent) {
        if (intent.getAction().equals(InteractorIntentServiceHelper.ACTION_SEARCH)) {
            String error = intent.getExtras().getString(ProcessingCommand.KEY_ERROR);
            if (error == null) {
                List<User> result = (List<User>) intent.getExtras().getSerializable(ProcessingCommand.KEY_USERS);
                getView().showUser(result.get(0));
                setCacheModel(result.get(0));
            }else
                getView().showError(error);
        }
    }

    public void findCurrenUserDetails() {
        if (!hasCachedModel()) {
            Bundle args = new Bundle();
            InteractorIntentServiceHelper.getInstance().setProcessingTarget(FindUserCommand.class.getName())
                    .addProcessingArgs(args).returnAction(InteractorIntentServiceHelper.ACTION_SEARCH).startService();
        }else
            getView().showUser((User)getCachedModel());
    }
}
