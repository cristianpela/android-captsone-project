package org.coursera.capstone.androidclient.gotit.main;

import java.io.IOException;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInActivity;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.common.BaseFragment;
import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.find.FindTeenFragment;
import org.coursera.capstone.androidclient.gotit.gift.GiftActivity;
import org.coursera.capstone.androidclient.gotit.notif.NotificationFragment;
import org.coursera.capstone.androidclient.gotit.profile.ProfileActivity;
import org.coursera.capstone.androidclient.gotit.profile.ProfileFragment;
import org.coursera.capstone.androidclient.gotit.reminder.ReminderFragment;
import org.coursera.capstone.androidclient.gotit.rest.User;
import org.coursera.capstone.androidclient.gotit.sync.GotItSyncBroadcastReceiver;

import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class DashboardActivity extends BaseActivity implements DashboardDrawerFragment.DrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the
     * navigation drawer.
     */
    private DashboardDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in
     * {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mNavigationDrawerFragment = (DashboardDrawerFragment) getSupportFragmentManager().findFragmentById(
                R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        // update the main content by replacing fragments
        //TODO(gotit): make switch
        if (position == R.string.title_search_teen) {
            replaceFragment(FindTeenFragment.class, position, R.layout.layout_find_teen);
        } else if (position == R.string.title_checkins) {
            startActivity(new Intent(this, CheckInActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (position == R.string.title_notifications) {
            replaceFragment(NotificationFragment.class, position, R.layout.layout_notifications);
        }else if(position == R.string.title_gifts){
            startActivity(new Intent(this, GiftActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } else if (position == R.string.title_profile) {
            replaceFragment(ProfileFragment.class, position, R.layout.layout_profile);
        } else if (position == R.string.title_reminder) {
            replaceFragment(ReminderFragment.class, position, R.layout.fragment_reminder);
        }else if(position == R.string.title_sync){
            sendBroadcast(new Intent(GotItSyncBroadcastReceiver.SYNC_MANUAL));
        } else if (position == R.string.title_logout) {
            // startActivity(new Intent(this, TestActivity.class));
            new AccountManagerHelper(this).logOut(new AccountManagerCallback<Bundle>() {
                @Override
                public void run(AccountManagerFuture<Bundle> future) {
                    try {
                        if (future.isCancelled()) {
                            finish();
                            return;
                        }
                        future.getResult();
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void replaceFragment(Class<? extends BaseFragment> clazz, int position, int layoutId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, BaseFragment.createFragment(clazz, position, layoutId)).commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
        case 0:
            mTitle = getString(R.string.title_search_teen);
            break;
        case 1:
            mTitle = getString(R.string.title_checkins);
            break;
        case 2:
            mTitle = getString(R.string.title_notifications);
            break;
        case 3:
            mTitle = getString(R.string.title_reminder);
            break;
        case 4:
            mTitle = getString(R.string.title_profile);
            break;
        case 5:
            mTitle = getString(R.string.title_logout);
            break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
        AccountManagerHelper helper = new AccountManagerHelper(this);
        String accountType = helper.isCurrentAccountTeen() ? "teen" : "follower";
        actionBar.setSubtitle(helper.getCurrentAccount().name + " (" + accountType + ")");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            // getMenuInflater().inflate(R.menu.dashboard, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
