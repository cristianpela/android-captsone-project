package org.coursera.capstone.androidclient.gotit.main;

import java.io.IOException;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class TestActivity extends BaseActivity implements TestView {

    protected static final String TAG = DashboardActivity.class.getSimpleName();
    
    private static final int FINISH_CODE = 1338;

    private TextView loggedUserTxtView;

    private TestPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test);
        loggedUserTxtView = (TextView) findViewById(R.id.lblLoggedUsername);

        presenter = new TestPresenter(this);
        setPresenter(presenter);
       // presenter.findCurrenUserDetails();
        Log.d(TAG, "created");
    }

    public void actionDetails(View button) {
        presenter.findCurrenUserDetails();
    }

    public void actionDeleteAccounts(View button) {
        new AccountManagerHelper(this).removeAllAccounts();
        finish();
    }

    public void actionSync(View button) {
        new AccountManagerHelper(this).requestSync();
    }

    public void actionLogOut(View button) {
        new AccountManagerHelper(this).logOut(new AccountManagerCallback<Bundle>() {
            @Override
            public void run(AccountManagerFuture<Bundle> future) {
                try {
                    /*if(future.isCancelled()){
                        finish();
                        return;
                    }*/
                    future.getResult();
                    finish();
                } catch (OperationCanceledException | AuthenticatorException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void showUser(User user) {
        loggedUserTxtView.setText(user.toString() + user.getUserDetails().toString());
    }
}
