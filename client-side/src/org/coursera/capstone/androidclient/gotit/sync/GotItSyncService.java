package org.coursera.capstone.androidclient.gotit.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class GotItSyncService extends Service {

    private static final Object lock = new Object();

    private static GotItSyncAdapter syncAdapter;;

    @Override
    public void onCreate() {
        synchronized (lock) {
            if (syncAdapter == null) {
                syncAdapter = new GotItSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }

}
