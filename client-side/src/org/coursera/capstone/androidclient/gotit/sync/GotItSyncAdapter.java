package org.coursera.capstone.androidclient.gotit.sync;

import static org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants.AUTHORITY;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInActivity;
import org.coursera.capstone.androidclient.gotit.common.GotItUtils;
import org.coursera.capstone.androidclient.gotit.main.LaunchActivity;
import org.coursera.capstone.androidclient.gotit.reminder.ReminderStorage;
import org.coursera.capstone.androidclient.gotit.repository.GotItRepository;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.SecuredRestException;

import android.accounts.Account;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.TimeUtils;
import android.text.format.DateUtils;
import android.util.Log;

//TODO(gotit) for now replaces the alarm manager
public class GotItSyncAdapter extends AbstractThreadedSyncAdapter {

    public static enum SyncType {
        MANUAL, AUTO;
    }

    private static final String TAG = GotItSyncAdapter.class.getSimpleName();

    public static void performSync(Context context, SyncType syncType) {
        AccountManagerHelper helper = new AccountManagerHelper(context);
        Account account = helper.getCurrentAccount();
        if (syncType == SyncType.AUTO) {
            ContentResolver.removePeriodicSync(account, AppAuthenticatorConstants.AUTHORITY, Bundle.EMPTY);
            ContentResolver.addPeriodicSync(account, AppAuthenticatorConstants.AUTHORITY, Bundle.EMPTY,
                    TimeUnit.MINUTES.toSeconds(5));
        } else {
            Bundle extras = new Bundle();
            extras.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            ContentResolver.requestSync(account, AUTHORITY, extras);
        }
    }

    public GotItSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider,
            SyncResult syncResult) {
        AccountManagerHelper helper = new AccountManagerHelper(getContext());
        if (helper.getCurrentAccount().name.equals(account.name)) {
            handleDatabaseSync(account, provider);
            handleReminder(getContext(), account.name);
        }
    }

    private void handleReminder(Context context, String teenUserName) {
        ReminderStorage reminderStorage = new ReminderStorage(context, teenUserName);
        int hourDay = GotItUtils.getHourOfTheDay();
        int minuteOfHour = GotItUtils.getMinuteOfTheHour(); // tolerance
        List<Integer> hours = reminderStorage.loadHours();
        for (int hour : hours) {
            if (hourDay == hour && minuteOfHour < 5) {
                Log.d(TAG, "Reminder: Time for a new Check In " + teenUserName + "!");
                Intent resultIntent = LaunchActivity.securedIntent(getContext(), CheckInActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 1984, resultIntent, 0);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.drawable.ic_share_white_24dp).setContentTitle("Got It Reminder")
                        .setContentText("It's time for a new Check In!").setContentIntent(pendingIntent);
                NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(
                        Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0, builder.build());
                break;
            }
        }
    }

    private void handleDatabaseSync(Account account, ContentProviderClient client) {
        Repository repository = new GotItRepository(getContext(), account, client);
        Log.d(TAG, "Performing sync");
        try {
            repository.sync();
            getContext().sendBroadcast(new Intent(GotItSyncBroadcastReceiver.SYNC_COMPLETED));
        } catch (SecuredRestException | RemoteException e) {
            e.printStackTrace();
        }
    }
}
