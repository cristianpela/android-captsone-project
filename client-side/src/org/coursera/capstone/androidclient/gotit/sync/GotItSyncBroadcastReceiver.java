package org.coursera.capstone.androidclient.gotit.sync;

import java.util.Date;

import org.coursera.capstone.androidclient.gotit.common.GotItUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class GotItSyncBroadcastReceiver extends BroadcastReceiver {

    public static final String SYNC_AUTO = "org.coursera.gotit.diab.mngmnt.action.SYNC_AUTO";
    public static final String SYNC_MANUAL = "org.coursera.gotit.diab.mngmnt.action.SYNC_MANUAL";
    public static final String SYNC_COMPLETED = "org.coursera.gotit.diab.mngmnt.action.SYNC_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(SYNC_AUTO) || action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            toast(context, "Starting periodic sync");
            GotItSyncAdapter.performSync(context, GotItSyncAdapter.SyncType.AUTO);
        } else if (action.equals(SYNC_MANUAL)) {
            toast(context, "Starting requested sync");
            GotItSyncAdapter.performSync(context, GotItSyncAdapter.SyncType.MANUAL);
        } else if (action.equals(SYNC_COMPLETED))
            toast(context, "Sync completed");

    }

    private void toast(Context context, String message) {
        String header = "Got It [" + GotItUtils.getStandardFormatedDate(new Date()) + "]: ";
        Toast.makeText(context, header + message, Toast.LENGTH_SHORT).show();
    }

}
