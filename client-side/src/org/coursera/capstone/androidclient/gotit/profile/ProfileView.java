package org.coursera.capstone.androidclient.gotit.profile;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.GotItView;

import android.graphics.Bitmap;

public interface ProfileView extends GotItView {

    public void onProfileImageLoaded(Bitmap bitmap);

    public void onProfileDataLoaded(String userName, String name, String birthDate, String mrn,
            List<ProfileListItem> followers, List<ProfileListItem> following);

    public void onProfileLinkingDataLoaded(boolean isTeen, boolean isFollowedByCurrentUser, boolean isCurrentLoggedUser);

}
