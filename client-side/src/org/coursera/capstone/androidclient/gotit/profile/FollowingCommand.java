package org.coursera.capstone.androidclient.gotit.profile;

import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;

import android.os.Bundle;

public class FollowingCommand implements ProcessingCommand {

    public static final String KEY_REQUEST_TEEN_USER_NAME = "req_teen_user_name";

    public static final String KEY_CONFIRM_FOLLOWER_USER_NAME = "conf_follower_user_name";

    public FollowingCommand() {
    }

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        try {
            String teenUserName = incomeParams.getString(KEY_REQUEST_TEEN_USER_NAME);
            String confirmFollowerUserName = incomeParams.getString(KEY_CONFIRM_FOLLOWER_USER_NAME);
            if (teenUserName != null) {
                repository.requestFollowing(teenUserName);
                outcomeParams.putString(KEY_MESSAGE, "Request to follow " + teenUserName + " has been sent.");
            }else if (confirmFollowerUserName != null){
                repository.confirmFollowing(confirmFollowerUserName);
            }
        } catch (Exception ex) {
            outcomeParams.putString(KEY_ERROR, "Error " + ex.getCause().getMessage());
        }
    }

}
