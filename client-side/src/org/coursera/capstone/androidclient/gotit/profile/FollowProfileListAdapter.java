package org.coursera.capstone.androidclient.gotit.profile;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

public class FollowProfileListAdapter extends ArrayAdapter<ProfileListItem> {

    public FollowProfileListAdapter(Context context, int resource, List<ProfileListItem> objects) {
        super(context, resource, objects);
    } 
}
