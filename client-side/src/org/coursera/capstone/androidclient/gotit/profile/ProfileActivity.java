package org.coursera.capstone.androidclient.gotit.profile;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

public class ProfileActivity extends BaseActivity implements ProfileView {

    public static final String USER_NAME_PARAM = "user_name_param_profile";
    
    private static final int ACTIVITY_ID = 999;

    private ProfileView profileViewAdapter;
    private ProfilePresenter presenter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);
        setTitle("Profile");
        initPresenter();
    }

    private void initPresenter() {
        Intent startedIntent = getIntent();
        String userName = startedIntent.getStringExtra(USER_NAME_PARAM);
        presenter = new ProfilePresenter(this, userName);
        profileViewAdapter = new ProfileViewAdapter(getWindow().getDecorView(), presenter);
        setPresenter(presenter);
    }
    
    public void actionGift(View button){
        startActivityForResult(new Intent(Intent.ACTION_PICK).setType("image/*"), ACTIVITY_ID);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ACTIVITY_ID)
           presenter.sendGift(data.getData());
    }

    @Override
    public void onProfileLinkingDataLoaded(boolean isTeen, boolean isFollowedByCurrentUser, boolean isCurrentLoggedUser) {
        profileViewAdapter.onProfileLinkingDataLoaded(isTeen, isFollowedByCurrentUser, isCurrentLoggedUser);
    }
    
    @Override
    public void onProfileImageLoaded(Bitmap bitmap) {
        profileViewAdapter.onProfileImageLoaded(bitmap);
    }

    @Override
    public void onProfileDataLoaded(String userName, String name, String birthDate, String mrn,
            List<ProfileListItem> followers, List<ProfileListItem> following) {
        profileViewAdapter.onProfileDataLoaded(userName, name, birthDate, mrn, followers, following);
        setTitle(name+"'s profile");
    }

    public static Intent startingIntent(Context context, String userName) {
        return new Intent(context, ProfileActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra(
                USER_NAME_PARAM, userName);
    }

   
}
