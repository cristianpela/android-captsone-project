package org.coursera.capstone.androidclient.gotit.profile;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.common.BaseFragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ProfileFragment extends BaseFragment implements ProfileView {

    public static final String TAG = ProfileFragment.class.getSimpleName();

    private ProfileView profileViewAdapter;

    private ProfilePresenter presenter;

    public ProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ProfilePresenter(this, null);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View superView = super.onCreateView(inflater, container, savedInstanceState);
        profileViewAdapter = new ProfileViewAdapter(superView, presenter);
        presenter.setView(this);
        return superView;
    }

    @Override
    public void onProfileLinkingDataLoaded(boolean isTeen, boolean isFollowedByCurrentUser, boolean isCurrentLoggedUser) {
        profileViewAdapter.onProfileLinkingDataLoaded(isTeen, isFollowedByCurrentUser, isCurrentLoggedUser);
    }

    public void onProfileImageLoaded(Bitmap bitmap) {
        profileViewAdapter.onProfileImageLoaded(bitmap);
    }

    public void onProfileDataLoaded(String userName, String name, String birthDate, String mrn,
            List<ProfileListItem> followers, List<ProfileListItem> following) {
        profileViewAdapter.onProfileDataLoaded(userName, name, birthDate, mrn, followers, following);
    }

}
