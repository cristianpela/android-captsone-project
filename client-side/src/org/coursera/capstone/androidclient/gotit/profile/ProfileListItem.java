package org.coursera.capstone.androidclient.gotit.profile;

public class ProfileListItem implements Comparable<ProfileListItem> {

    public final String userName;

    public final String name;

    public ProfileListItem(String userName, String name) {
        this.userName = userName;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ProfileListItem create(String data) {
        String[] split = data.split(";");
        return new ProfileListItem(split[0], split[1]);
    }

    protected static String[] getParsedData(String data) {
        return data.split(";");
    }

    public static ProfileListItem create(String userName, String firstAndLastName) {
        return new ProfileListItem(userName, firstAndLastName);
    }

    @Override
    public int compareTo(ProfileListItem another) {
        return this.userName.compareTo(another.userName);
    }

}
