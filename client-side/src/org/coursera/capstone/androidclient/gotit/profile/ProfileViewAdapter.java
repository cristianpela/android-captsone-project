package org.coursera.capstone.androidclient.gotit.profile;

import java.lang.ref.WeakReference;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.Visibility;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;

public class ProfileViewAdapter implements ProfileView {

    private WeakReference<View> androidViewRef;

    private TextView txtAccountType, txtUserName, txtName, _txtBirthDate, txtBirthDate, _txtMRN, txtMRN;

    private ListView listFollowers, listFollowing;

    private Button btnRequestFollow, btnRequestCheckIn, btnCheckIn, btnCheckInHistory, btnGift;

    private ImageView imageProfile;

    private LinearLayout layoutFollowers, layoutFollowing, layoutMain;

    private FrameLayout layoutLoading;

    private ProfilePresenter presenter;

    public ProfileViewAdapter(View androidView, ProfilePresenter profilePresenter) {

        this.androidViewRef = new WeakReference<View>(androidView);
        presenter = profilePresenter;

        txtAccountType = (TextView) getView().findViewById(R.id.textAccountType);
        txtUserName = (TextView) getView().findViewById(R.id.txtUserName);
        txtName = (TextView) getView().findViewById(R.id.textFirstAndLastName);
        txtBirthDate = (TextView) getView().findViewById(R.id.textProfileBirthDate);
        _txtBirthDate = (TextView) getView().findViewById(R.id._textProfileBirthDate);
        txtMRN = (TextView) getView().findViewById(R.id.textMRN);
        _txtMRN = (TextView) getView().findViewById(R.id._textMRN);

        imageProfile = (ImageView) getView().findViewById(R.id.imageProfile2);

        btnRequestFollow = (Button) getView().findViewById(R.id.btnRequestFollow);
        btnRequestFollow.setTag(ProfilePresenter.ACTION_TAG_REQUEST_FOLLOW);
        btnRequestFollow.setOnClickListener(presenter);
        
        btnGift = (Button) getView().findViewById(R.id.btnSendGift);

        btnRequestCheckIn = (Button) getView().findViewById(R.id.btnRequestCheckIn);
        btnRequestCheckIn.setTag(ProfilePresenter.ACTION_TAG_REQUEST_CHECK_IN);
        btnRequestCheckIn.setOnClickListener(presenter);

        btnCheckIn = (Button) getView().findViewById(R.id.btnCheckIn);
        btnCheckIn.setTag(ProfilePresenter.ACTION_TAG_CHECK_IN);
        btnCheckIn.setOnClickListener(presenter);

        btnCheckInHistory = (Button) getView().findViewById(R.id.btnCheckInHistory);
        btnCheckInHistory.setTag(ProfilePresenter.ACTION_TAG_CHECK_IN_HISTORY);
        btnCheckInHistory.setOnClickListener(presenter);

        
        listFollowers = (ListView) getView().findViewById(R.id.listFollowers);
        listFollowers.setOnItemClickListener(presenter);
        listFollowing = (ListView) getView().findViewById(R.id.listFollowing);
        listFollowing.setOnItemClickListener(presenter);

        
        layoutLoading = (FrameLayout) getView().findViewById(R.id.progLoadingLayout);
        layoutLoading.setVisibility(View.VISIBLE);
        layoutMain = (LinearLayout) getView().findViewById(R.id.layoutProfileMain);
        layoutMain.setVisibility(View.INVISIBLE);
        
        layoutFollowers = (LinearLayout) getView().findViewById(R.id.layoutFollowers);
        layoutFollowers.setVisibility(View.GONE);
        layoutFollowing = (LinearLayout) getView().findViewById(R.id.layoutFollowing);
        
        btnCheckIn.setVisibility(View.GONE);
        btnRequestCheckIn.setVisibility(View.GONE);
        btnCheckInHistory.setVisibility(View.GONE);
        btnGift.setVisibility(View.GONE);

        txtBirthDate.setVisibility(View.GONE);
        _txtBirthDate.setVisibility(View.GONE);
        txtMRN.setVisibility(View.GONE);
        _txtMRN.setVisibility(View.GONE);

        btnRequestFollow.setVisibility(View.GONE);

    }

    private View getView() {
        return androidViewRef.get();
    }

    @Override
    public void onProfileImageLoaded(Bitmap bitmap) {
        imageProfile.setImageBitmap(bitmap);
    }

    @Override
    public void onProfileLinkingDataLoaded(boolean isTeen, boolean isFollowedByCurrentUser, boolean isCurrentLoggedUser) {
        if (isTeen) {
            txtAccountType.setText("Teen");
            txtBirthDate.setVisibility(View.VISIBLE);
            _txtBirthDate.setVisibility(View.VISIBLE);
            txtMRN.setVisibility(View.VISIBLE);
            _txtMRN.setVisibility(View.VISIBLE);
            layoutFollowers.setVisibility(View.VISIBLE);
        } else
            txtAccountType.setText("Follower");

        if ((isTeen && isCurrentLoggedUser) || isFollowedByCurrentUser) {
            btnCheckInHistory.setVisibility(View.VISIBLE);
        }

        if (isTeen && isFollowedByCurrentUser) {
            btnRequestCheckIn.setVisibility(View.VISIBLE);
            btnGift.setVisibility(View.VISIBLE);
        }

        if (isTeen && isCurrentLoggedUser) {
            btnCheckIn.setVisibility(View.VISIBLE);
        }

        if (isTeen && !isCurrentLoggedUser && !isFollowedByCurrentUser) {
            btnRequestFollow.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onProfileDataLoaded(String userName, String name, String birthDate, String mrn,
            List<ProfileListItem> followers, List<ProfileListItem> following) {

        layoutLoading.setVisibility(View.GONE);
        layoutMain.setVisibility(View.VISIBLE);

        txtUserName.setText(userName);
        txtName.setText(name);
        txtBirthDate.setText(birthDate);
        txtMRN.setText(mrn);
        if (!followers.isEmpty())
            listFollowers.setAdapter(new ArrayAdapter<ProfileListItem>(getView().getContext(),
                    R.layout.layout_profile_item, followers));
        if (!following.isEmpty())
            listFollowing.setAdapter(new ArrayAdapter<ProfileListItem>(getView().getContext(),
                    R.layout.layout_profile_item, following));
    }

    @Override
    public void showError(String error) {
        throw new UnsupportedOperationException("This should be dealt by base activity/fragment");
    }

    @Override
    public void showInfo(String info) {
        throw new UnsupportedOperationException("This should be dealt by base activity/fragment");
    }

    @Override
    public Context getViewContext() {
        throw new UnsupportedOperationException("This should be dealt by base activity/fragment");
    }

}
