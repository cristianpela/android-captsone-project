package org.coursera.capstone.androidclient.gotit.profile;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.apache.http.client.utils.DateUtils;
import org.coursera.capstone.androidclient.gotit.auth.AccountManagerHelper;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInActivity;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInCommand;
import org.coursera.capstone.androidclient.gotit.checkin.CheckInHistoryActivity;
import org.coursera.capstone.androidclient.gotit.common.BaseActivity;
import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.common.ProfileImageLoader;
import org.coursera.capstone.androidclient.gotit.gift.GiftCommand;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.sax.StartElementListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class ProfilePresenter extends BasePresenter<ProfileView> implements OnClickListener, OnItemClickListener {

    public static final int ACTION_TAG_REQUEST_FOLLOW = 1;

    public static final int ACTION_TAG_REQUEST_CHECK_IN = 2;

    public static final int ACTION_TAG_CHECK_IN = 3;

    public static final int ACTION_TAG_CHECK_IN_HISTORY = 4;

    public static final int ACTION_TAG_GIFT = 5;

    private String userName;

    public ProfilePresenter(ProfileView view, String userName) {
        super(view);
        this.userName = (userName == null) ? getCurrentLoggedUserName() : userName;
        InteractorIntentServiceHelper.PredefinedInteractions.find(userName, false);
    }

    @Override
    protected void onReturnAction(Intent intent) {
        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_SEARCH)) {
            model = intent.getExtras().getSerializable(ProcessingCommand.KEY_USERS);
            sendDataToView();
        }

        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_REQUEST_FOLLOW)) {
            // debug(intent.getExtras().getString(ProcessingCommand.KEY_MESSAGE));
            getView().showInfo(intent.getExtras().getString(ProcessingCommand.KEY_MESSAGE));
        }
    }

    private void sendDataToView() {

        User user = getUser();
        if (user.getImageProfilePath() != null)
            InteractorIntentServiceHelper.getInstance().startAsyncService(new ProfileImageLoader() {
                @Override
                public void onPostExecute(Bitmap result) {
                    getView().onProfileImageLoaded(result);
                }
            }, user.getImageProfilePath());

        getView().onProfileLinkingDataLoaded(user.isTeen(), isFollowedByCurrentUser(), isCurrentLoggedUser());

        String name = user.getUserDetails().getFirstName() + " " + user.getUserDetails().getLastName();
        String birthDate = DateUtils.formatDate(user.getUserDetails().getBirthDate(), "MM.dd.yyyy");
        String mrn = "" + user.getUserDetails().getMedicalRecordNumber();

        List<ProfileListItem> followers = new ArrayList<ProfileListItem>();
        for (String follower : user.getFollowers()) {
            followers.add(ProfileListItem.create(follower));
        }

        List<ProfileListItem> following = new ArrayList<ProfileListItem>();
        for (String follower : user.getFollowing()) {
            following.add(ProfileListItem.create(follower));
        }
        debug(followers.toString() + " " + following);
        getView().onProfileDataLoaded(user.getUserName(), name, birthDate, mrn, followers, following);
    }

    @Override
    protected void onReatachView() {
        sendDataToView();
    }

    private boolean isFollowedByCurrentUser() {
        User user = getUser();
        String currentLoggedUserName = getCurrentLoggedUserName();
        for (String follower : user.getFollowers()) {
            if (follower.split(";")[0].equals(currentLoggedUserName))
                return true;
        }
        return false;
    }

    private boolean isCurrentLoggedUser() {
        String currentLoggedUserName = getCurrentLoggedUserName();
        return currentLoggedUserName.equals(userName);
    }

    @Override
    public void onClick(View v) {
        User user = getUser();
        int actionTag = (int) v.getTag();
        switch (actionTag) {
        case ACTION_TAG_REQUEST_FOLLOW:
            Bundle extras1 = new Bundle();
            extras1.putString(FollowingCommand.KEY_REQUEST_TEEN_USER_NAME, user.getUserName());
            InteractorIntentServiceHelper.getInstance()
                    .returnAction(InteractorIntentServiceHelper.ACTION_REQUEST_FOLLOW).addProcessingArgs(extras1)
                    .setProcessingTarget(FollowingCommand.class.getName()).startService();
            break;
        case ACTION_TAG_REQUEST_CHECK_IN:
            Bundle extras2 = new Bundle();
            extras2.putBoolean(CheckInCommand.KEY_REQUEST_CHECK_IN, true);
            extras2.putString(CheckInCommand.KEY_USER_NAME, user.getUserName());
            InteractorIntentServiceHelper.getInstance().addProcessingArgs(extras2)
                    .setProcessingTarget(CheckInCommand.class.getName()).startService();
            break;
        case ACTION_TAG_CHECK_IN:
            getView().getViewContext().startActivity(
                    CheckInActivity.startingIntent(getView().getViewContext(), user.getUserName()));
            break;
        case ACTION_TAG_CHECK_IN_HISTORY:
            Context context = getView().getViewContext();
            context.startActivity(CheckInHistoryActivity.createIntent(context, user.getUserName()));
            break;
        }
    }

    private User getUser() {
        @SuppressWarnings("unchecked")
        User user = ((List<User>) model).get(0);
        return user;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProfileListItem item = (ProfileListItem) parent.getItemAtPosition(position);
        getView().getViewContext().startActivity(
                ProfileActivity.startingIntent(getView().getViewContext(), item.userName));
    }

    public void sendGift(Uri data) {
        String picturePath = getGiftPicturePath(data);
        debug(picturePath);
        Bundle extras = new Bundle();
        extras.putString(GiftCommand.KEY_POST_GIFT, picturePath);
        extras.putString(GiftCommand.KEY_USER_NAME, getUser().getUserName());
        getView().showInfo("Sending gift to: "+ getUser().getUserName());
        InteractorIntentServiceHelper.getInstance().addProcessingArgs(extras)
                .returnAction(InteractorIntentServiceHelper.ACTION_GIFT)
                .setProcessingTarget(GiftCommand.class.getName()).startService();
    }

    private String getGiftPicturePath(Uri imgUri) {
        String pathColumn = MediaStore.Images.ImageColumns.DATA;
        Cursor cursor = getView().getViewContext().getContentResolver()
                .query(imgUri, new String[] { pathColumn }, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            String path = cursor.getString(cursor.getColumnIndex(pathColumn));
            cursor.close();
            return path;
        }
        return null;
    }

}
