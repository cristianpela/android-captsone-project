package org.coursera.capstone.androidclient.gotit.find;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.common.BaseFragment;
import org.coursera.capstone.androidclient.gotit.profile.ProfileListItem;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

public class FindTeenFragment extends BaseFragment implements FindTeenView {

    public static final String TAG = FindTeenFragment.class.getSimpleName();
    
    private static final int DIALOG_TEEN_ACTIONS = 10012;

    private GridView gridSearchTeen;

    private EditText textSearchTeen;

    private Button btnSearchButton;

    private FindTeenPresenter presenter;

    public FindTeenFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FindTeenPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        textSearchTeen = (EditText) view.findViewById(R.id.editSearchTeen);
        gridSearchTeen = (GridView) view.findViewById(R.id.gridSearchTeen);
        gridSearchTeen.setOnItemLongClickListener(presenter);
        gridSearchTeen.setOnItemClickListener(presenter);
        btnSearchButton = (Button) view.findViewById(R.id.btnSeachTeen);
        btnSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.search(textSearchTeen.getText().toString());
            }
        });
        presenter.setView(this);
        return view;
    }

    @Override
    public void onImagesURLoaded(List<ProfileListItem> teens) {
        gridSearchTeen.setAdapter(new FindUserGridAdapter(teens, getViewContext()));
    }

}
