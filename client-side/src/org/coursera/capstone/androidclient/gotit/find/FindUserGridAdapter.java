package org.coursera.capstone.androidclient.gotit.find;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.R;
import org.coursera.capstone.androidclient.gotit.profile.ProfileListItem;
import org.coursera.capstone.androidclient.gotit.rest.User;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FindUserGridAdapter extends BaseAdapter {

    private List<ProfileListItem> users;

    private LayoutInflater inflater;

    private DisplayImageOptions options;

    public FindUserGridAdapter(List<ProfileListItem> users, Context context) {
        this.users = users;
        inflater = LayoutInflater.from(context);
        options = new DisplayImageOptions.Builder().showImageOnFail(R.drawable.ic_default_profile).cacheInMemory(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.layout_image_profile, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.imageProfile2);
            holder.textView = (TextView) view.findViewById(R.id.textImageProfile2);
            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();
        
        ProfileListItem listItem = users.get(position);
        holder.textView.setText(listItem.name);
        ImageLoader.getInstance().displayImage(listItem.userName, holder.imageView, options);
        return view;
    }

    private static class ViewHolder {
        ImageView imageView;
        TextView textView;
    }
}
