package org.coursera.capstone.androidclient.gotit.find;

import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.GotItView;
import org.coursera.capstone.androidclient.gotit.profile.ProfileListItem;
import org.coursera.capstone.androidclient.gotit.rest.User;

public interface FindTeenView extends GotItView {
    
    public void onImagesURLoaded(List<ProfileListItem> list);

}
