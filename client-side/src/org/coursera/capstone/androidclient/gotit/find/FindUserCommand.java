package org.coursera.capstone.androidclient.gotit.find;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.repository.Repository;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.os.Bundle;
import android.util.Log;

public class FindUserCommand implements ProcessingCommand {

    private static final String TAG = FindUserCommand.class.getSimpleName();

    @Override
    public void process(String requestAction, Bundle incomeParams, Bundle outcomeParams, Repository repository) {
        String userName = incomeParams.getString(KEY_USER_NAME);
        boolean many  = incomeParams.getBoolean(KEY_MANY);
        try {
            List<User> users;
            if(many)
                users = repository.findUsers(userName);
            else{
                User user = repository.findUser(userName);
                users = new ArrayList<User>();
                users.add(user);
            }
            outcomeParams.putSerializable(KEY_USERS, (Serializable) users);
        } catch (Exception er) {
             outcomeParams.putString(KEY_ERROR, er.getMessage());   
        }
    }

}
