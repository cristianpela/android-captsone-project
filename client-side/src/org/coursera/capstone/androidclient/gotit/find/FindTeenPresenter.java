package org.coursera.capstone.androidclient.gotit.find;

import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.common.BasePresenter;
import org.coursera.capstone.androidclient.gotit.common.InteractorIntentServiceHelper;
import org.coursera.capstone.androidclient.gotit.common.ProcessingCommand;
import org.coursera.capstone.androidclient.gotit.profile.ProfileActivity;
import org.coursera.capstone.androidclient.gotit.profile.ProfileListItem;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public class FindTeenPresenter extends BasePresenter<FindTeenView> implements OnClickListener, OnItemLongClickListener, OnItemClickListener {

    public FindTeenPresenter(FindTeenView view) {
        super(view);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onReturnAction(Intent intent) {
        if (!isDormant() && intent.getAction().equals(InteractorIntentServiceHelper.ACTION_SEARCH)) {
            model = intent.getSerializableExtra(ProcessingCommand.KEY_USERS);
            getView().onImagesURLoaded(createListItems());
        }
    }

    public void search(String nameLike) {
        if (nameLike.length() < 3)
            appendError("Search criteria must be at least of 3 characters");
        if (nameLike.matches("[^a-zA-Z0-9]+"))
            appendError("Your search contains illegal characters");
        if (hasErrors())
            showError();
        else {
            debug("Searching: " + nameLike);
            InteractorIntentServiceHelper.PredefinedInteractions.find(nameLike, true);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onReatachView() {
        getView().onImagesURLoaded(createListItems());
    }

    @Override
    public void onClick(View v) {
    }

    private List<ProfileListItem> createListItems() {
        @SuppressWarnings("unchecked")
        List<User> users = (List<User>) model;
        List<ProfileListItem> profileListItems = new ArrayList<ProfileListItem>();
        for (User user : users) {
            profileListItems.add(ProfileListItem.create(user.getUserName(), user.getUserDetails().getFirstName() + " "
                    + user.getUserDetails().getLastName()));
        }
        return profileListItems;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProfileListItem item = (ProfileListItem) parent.getItemAtPosition(position);
        getView().getViewContext().startActivity(
                ProfileActivity.startingIntent(getView().getViewContext(), item.userName));
    }
}
