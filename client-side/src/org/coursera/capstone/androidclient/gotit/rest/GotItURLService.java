package org.coursera.capstone.androidclient.gotit.rest;

import java.util.List;



import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Streaming;
import retrofit.mime.TypedFile;
import static org.coursera.capstone.androidclient.gotit.rest.GotItURLMapping.*;

public interface GotItURLService {

    @POST(SIGN_UP_PATH)
    public User signUp(@Body User user);

    @Multipart
    @POST(AVATAR_PATH)
    public boolean postAvatarData(@Path(USER_NAME_PARAMETER) String userName, @Part(DATA_PARAMETER) TypedFile avatarData);

    @Streaming
    @GET(AVATAR_PATH)
    public Response getAvatarData(@Path(USER_NAME_PARAMETER) String userName);

    @DELETE(AVATAR_PATH)
    public Response deleteAvatarData(@Path(USER_NAME_PARAMETER) String userName);

    @GET(GET_CURRENT_USER_PATH_BY_NAME_PATH)
    public User getUserByName(@Path(USER_NAME_PARAMETER) String userName);

    @GET(FIND_ALL_TEENS_PATH)
    public List<User> findAllTeens();

    @GET(FIND_ALL_TEENS_LIKE_PATH)
    public List<User> findAllTeensLike(@Path(TEEN_LIKE_CRITERIA_PARAMETER) String nameCriteria);

    @GET(REQUEST_FOR_FOLLOWING_PATH)
    public Response requestForFollowingTeen(@Path(USER_NAME_PARAMETER) String teenUserName);

    @GET(CONFIRM_FOR_FOLLOWING_PATH)
    public Response confirmFollower(@Path(USER_NAME_PARAMETER) String followerUserName);

    @GET(GET_ALL_FOLLOWERS_PATH)
    public List<User> getFollowers();

    @GET(GET_ALL_FOLLOWED_TEENS_PATH)
    public List<User> getFollowedTeens();

    @GET(GET_ALL_PENDING_FOLLOWERS_PATH)
    public List<User> getPendingFollowers();

    @GET(GET_ALL_PENDING_FOLLOWED_TEENS_PATH)
    public List<User> getPendingFollowedTeens();

    @GET(REQUEST_FOR_CHECK_IN_PATH)
    public Response requestCheckIn(@Path(USER_NAME_PARAMETER) String teenUserName);

    @GET(ACKNOWLEDGE_CHECK_IN_REQUESTS_PATH)
    public Response acknowledgeCheckInRequests();

    @GET(GET_CHECK_IN_REQUESTS_PATH)
    public CheckInRequest getCheckInRequests();

    @GET(GET_ALL_QUESTIONS_PATH)
    public List<Question> getAllQuestions(@Path(DATE_PARAMETER) long referenceDateTime);

    @POST(POST_CHECK_IN_PATH)
    public long postCheckIn(@Body CheckIn checkIn);

    @GET(GET_TEEN_CHECK_IN_HISTORY_PATH)
    public List<CheckIn> getTeenCheckInHistroy(@Path(DATE_PARAMETER) long referenceDateTime,
            @Path(USER_NAME_PARAMETER) String teenUserName);

    @Multipart
    @POST(POST_GIFT_PHOTO_PATH)
    public Response postGiftPhotoData(@Path(USER_NAME_PARAMETER) String teenUserName,
            @Part(DATA_PARAMETER) TypedFile giftPhotoData);
    
    @Streaming
    @GET(GET_GIFT_PHOTO_PATH)
    public Response getGiftPhotoData(@Path(GIFT_PHOTO_FILE_NAME_PARAMETER) String giftPhotoFileName);
    
    @GET(GET_ALL_GIFT_PHOTOS_PATH)
    public List<Gift> getAllGifts();
    
    @GET(GET_NOTIFICATIONS)
    public List<Notification> getNotifications();


}
