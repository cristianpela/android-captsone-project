package org.coursera.capstone.androidclient.gotit.rest;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public enum NotificationType {

    FOLLOW_REQUEST, CHECK_IN_REQUEST;
    
    public static class Deserializer extends JsonDeserializer<NotificationType>{
        @Override
        public NotificationType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
                JsonProcessingException {
            return NotificationType.valueOf(p.getValueAsString());
        } 
    }
}
