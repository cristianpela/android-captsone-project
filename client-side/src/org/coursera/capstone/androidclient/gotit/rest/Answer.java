package org.coursera.capstone.androidclient.gotit.rest;

public class Answer {

    public static final Answer YES = new Answer("Yes", FeedbackType.POSITIVE);

    public static final Answer NO = new Answer("No", FeedbackType.NEGATIVE);

    public static final Answer PRIVATE = new Answer("Private", null);

    private String body;

    private FeedbackType feedbackType;

    public Answer(String body, FeedbackType feedbackType) {
        this.body = body;
        this.feedbackType = feedbackType;
    }

    public Answer(FeedbackType feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public FeedbackType getFeedbackType() {
        return feedbackType;
    }

    public static Answer createPositiveAnswer(String body) {
        return new Answer(body, FeedbackType.POSITIVE);
    }

    public static Answer createNegativeAnswer(String body) {
        return new Answer(body, FeedbackType.NEGATIVE);
    }
}
