package org.coursera.capstone.androidclient.gotit.rest;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum QuestionGeneratedFeedbackMode {
    AUTO, MANUAL;
    
    public static class Deserializer extends JsonDeserializer<QuestionGeneratedFeedbackMode> {
        @Override
        public QuestionGeneratedFeedbackMode deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
                JsonProcessingException {
            return QuestionGeneratedFeedbackMode.valueOf( p.getValueAsString());
        }
    }
}
