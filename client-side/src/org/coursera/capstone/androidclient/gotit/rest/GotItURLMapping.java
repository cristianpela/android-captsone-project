package org.coursera.capstone.androidclient.gotit.rest;


public final class GotItURLMapping {

    public static final String TOKEN_PATH = "/oauth/token";

    public static final String PUBLIC_ACCESS_PATH = "/public/";

    public static final String PRIVATE_ACCESS_PATH = "/private/";

    public static final String ANY = "**";

    public static final String USER_ID_PARAMETER = "userId";
    
    public static final String USER_NAME_PARAMETER = "userName";

    public static final String DATA_PARAMETER = "data";
    
    public static final String DATE_PARAMETER = "date";
    
    /*
     * 
     * ################## SIGN UP/ UPDATE PROFILE BLOCK ###############
     * 
     * .
     */

   

    public static final String SIGN_UP_PATH = PUBLIC_ACCESS_PATH + "signup/";

    public static final String AVATAR_PATH = SIGN_UP_PATH + "avatar/{" + USER_NAME_PARAMETER + "}";

    public static final String GET_CURRENT_USER_PATH_BY_ID_PATH = PRIVATE_ACCESS_PATH + "id/{" + USER_ID_PARAMETER
            + "}";

    public static final String GET_CURRENT_USER_PATH_BY_NAME_PATH = PRIVATE_ACCESS_PATH + "name/{"
            + USER_NAME_PARAMETER + "}";

    public static final String GET_USER_PATH_BY_ID_PATH = PRIVATE_ACCESS_PATH + "user/id/{" + USER_ID_PARAMETER + "}";

    public static final String GET_USER_PATH_BY_NAME_PATH = PRIVATE_ACCESS_PATH + "user/name/{" + USER_NAME_PARAMETER
            + "}";

    /*
     * 
     * ################## SEARCH TEEN BLOCK ###############
     * 
     * .
     */
    public static final String TEEN_LIKE_CRITERIA_PARAMETER = "teenCriteria";

    public static final String FIND_ALL_TEENS_PATH = PRIVATE_ACCESS_PATH + "teens/";

    public static final String FIND_ALL_TEENS_LIKE_PATH = FIND_ALL_TEENS_PATH + "like/{" + TEEN_LIKE_CRITERIA_PARAMETER
            + "}";

    /*
     * 
     * ################## FOLLOW TEEN/ ACCEPT FOLLOWER BLOCK ###############
     * 
     * .
     */

    public static final String REQUEST_FOR_FOLLOWING_PATH = PRIVATE_ACCESS_PATH + "request/teen/{"
            + USER_NAME_PARAMETER + "}";

    public static final String CONFIRM_FOR_FOLLOWING_PATH = PRIVATE_ACCESS_PATH + "confirm/follower/{"
            + USER_NAME_PARAMETER + "}";

    public static final String GET_ALL_FOLLOWERS_PATH = FIND_ALL_TEENS_PATH + "followers/";

    public static final String GET_ALL_FOLLOWED_TEENS_PATH = FIND_ALL_TEENS_PATH + "followed/";

    public static final String GET_ALL_PENDING_FOLLOWERS_PATH = GET_ALL_FOLLOWERS_PATH + "pending/";

    public static final String GET_ALL_PENDING_FOLLOWED_TEENS_PATH = GET_ALL_FOLLOWED_TEENS_PATH + "pending/";

    /*
     * 
     * ################## CHECKIN BLOCK ##################################
     * 
     * .
     */
    private static final String CHECK_IN_PATH = PRIVATE_ACCESS_PATH + "checkin/";

    public static final String CHECK_IN_ID_PARAMETER = "id";

    public static final String CHECK_IN_DATE_PARAMETER = "date";

    public static final String REQUEST_FOR_CHECK_IN_PATH = CHECK_IN_PATH + "request/{" + USER_NAME_PARAMETER + "}";

    public static final String GET_CHECK_IN_REQUESTS_PATH = CHECK_IN_PATH + "notifications";

    public static final String ACKNOWLEDGE_CHECK_IN_REQUESTS_PATH = CHECK_IN_PATH + "acknowledge";

    public static final String POST_CHECK_IN_PATH = CHECK_IN_PATH + "post/";


    public static final String GET_TEEN_CHECK_IN_HISTORY_PATH = CHECK_IN_PATH + "history/{" + USER_NAME_PARAMETER + "}/{"
            + CHECK_IN_DATE_PARAMETER + "}";
    /*
     * 
     * ################## QUESTION BLOCK ##################################
     * 
     * .
     */
    private static final String QUESTION_PATH = PRIVATE_ACCESS_PATH + "question/";

    public static final String GET_ALL_QUESTIONS_PATH = QUESTION_PATH + "all/{" + DATE_PARAMETER + "}";

    /*
     * 
     * ################## NOTIFICATION BLOCK ##################################
     * 
     * .
     */
    
    public static final String GET_NOTIFICATIONS = PRIVATE_ACCESS_PATH + "notifications/";
    
    
    /*
     * 
     * ################# GIFT PHOTO BLOCK #####################################
     * 
     * 
     * 
     */
  
    public static final String  GIFT_PHOTO_FILE_NAME_PARAMETER = "giftfile";
     
    public static final String POST_GIFT_PHOTO_PATH = PRIVATE_ACCESS_PATH + "gift/post/{"
            + USER_NAME_PARAMETER + "}";
    
    public static final String GET_GIFT_PHOTO_PATH = PRIVATE_ACCESS_PATH + "gift/data/{" + GIFT_PHOTO_FILE_NAME_PARAMETER +"}";
    
    public static final String GET_ALL_GIFT_PHOTOS_PATH = PRIVATE_ACCESS_PATH + "gift/all/";

    private GotItURLMapping() {
    }

}
