package org.coursera.capstone.androidclient.gotit.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

import android.util.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.converter.JacksonConverter;
import retrofit.mime.FormUrlEncodedTypedOutput;

public class GotItConnection {

    private static final String CLIENT_ID = "mobile";
    private static final String SERVER_URL = "https://10.0.2.2:8443";
    private static final String TOKEN_PATH = "/oauth/token";

    private Client client;

    private String accessToken;
    private String userName;
    private String password;

    private GotItURLService restInterface;

    public GotItConnection(Client client) {
        this.client = client;
        OAuthHandler oauthHandler = new OAuthHandler();
        restInterface = new RestAdapter.Builder().setClient(client).setErrorHandler(oauthHandler)
                .setRequestInterceptor(oauthHandler).setEndpoint(SERVER_URL).setLogLevel(LogLevel.BASIC)
                .setConverter(new JacksonConverter(new ObjectMapper())).build().create(GotItURLService.class);
    }

    public String logIn(String userName, String password) throws IOException, SecuredRestException {
        aquireToken(userName, password);
        return accessToken;
    }

    public void logOut() {
        accessToken = null;
    }

    private void aquireToken(String userName, String password) throws IOException {
        FormUrlEncodedTypedOutput to = new FormUrlEncodedTypedOutput();
        to.addField("username", userName);
        to.addField("password", password);
        to.addField("client_id", CLIENT_ID);
        to.addField("client_secret", "");
        to.addField("grant_type", "password");
        String base64Auth = Base64.encodeToString(new String(CLIENT_ID + ":" + "").getBytes("UTF-8"), Base64.NO_WRAP);
        List<Header> headers = new ArrayList<Header>();
        headers.add(new Header("Authorization", "Basic " + base64Auth));
        Request req = new Request("POST", SERVER_URL + TOKEN_PATH, headers, to);
        Response resp = client.execute(req);
        if (resp.getStatus() < 200 || resp.getStatus() > 299) {
            throw new SecuredRestException("Login failure: " + resp.getStatus() + " - " + resp.getReason());
        } else {
            String body = IOUtils.toString(resp.getBody().in());
            accessToken = extractField(body, "access_token");
        }
    }

    private String extractField(String body, String field) {
        JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
        return (jsonObject != null) ? jsonObject.get(field).getAsString() : null;
    }

    private class OAuthHandler implements RequestInterceptor, ErrorHandler {

        @Override
        public void intercept(RequestFacade request) {
            if (accessToken != null)
                request.addHeader("Authorization", "Bearer " + accessToken);
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            Response resp = cause.getResponse();
            if (resp.getStatus() == 401) {
                try {
                    String body = IOUtils.toString(resp.getBody().in());
                    String errorDescription = extractField(body, "error_description");
                    if (errorDescription != null && errorDescription.equals("The access token expired")) {
                        aquireToken(userName, password);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return new SecuredRestException(cause);
        }
    }

    public GotItURLService getRESTInterface() {
        return restInterface;
    }

    public static GotItConnection getInstance() {
        return GotItConnection.OnDemandInstantiator.INSTANCE;
    }

    private static final class OnDemandInstantiator {
        static final GotItConnection INSTANCE = new GotItConnection(new OkClient(
                UnsafeHttpsClient.getUnsafeOkHttpClient()));
    }

    public static String stringifyPath(String path, String... pathVariables) {
        Pattern pattern = Pattern.compile("\\{[0-9a-zA-Z]+\\}");
        Matcher matcher = pattern.matcher(path);

        int pathVarPos = 0;
        while (matcher.find()) {
            path = path.replace(matcher.group(), pathVariables[pathVarPos]);
            pathVarPos++;
        }

        return SERVER_URL + path;
    }

}
