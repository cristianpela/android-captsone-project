package org.coursera.capstone.androidclient.gotit.rest;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Gift implements Serializable {

    private static final long serialVersionUID = -2864365911850579917L;

    private String followerUserName;

    private String followerFullName;

    private String photoFileName;

    private Date date;

    @JsonIgnore
    private boolean locallySaved;

    public Gift() {
    }

    public Gift(String followerUserName, String followerFullName, Date giftDate, String giftFile) {
        this.followerFullName = followerFullName;
        this.followerUserName = followerUserName;
        date = giftDate;
        photoFileName = giftFile;
    }

    public String getFollowerUserName() {
        return followerUserName;
    }

    public void setFollowerUserName(String followerUserName) {
        this.followerUserName = followerUserName;
    }

    public String getFollowerFullName() {
        return followerFullName;
    }

    public void setFollowerFullName(String followerFullName) {
        this.followerFullName = followerFullName;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(String photoFileName) {
        this.photoFileName = photoFileName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isLocallySaved() {
        return locallySaved;
    }

    public void setLocallySaved(boolean locallySaved) {
        this.locallySaved = locallySaved;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((photoFileName == null) ? 0 : photoFileName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Gift other = (Gift) obj;
        if (photoFileName == null) {
            if (other.photoFileName != null)
                return false;
        } else if (!photoFileName.equals(other.photoFileName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Gift [followerUserName=" + followerUserName + ", followerFullName=" + followerFullName
                + ", photoFileName=" + photoFileName + ", date=" + date + "]";
    }
}
