package org.coursera.capstone.androidclient.gotit.rest;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Question implements Serializable {

    private static final long serialVersionUID = -7230034934211242868L;

    private long id;

    private String body;

    private boolean enterable;

    // type is number or string (this is for edit field type constraint)
    private QuestionEnterType enterType = QuestionEnterType.NONE;

    // manual or auto; manual means that the teen should choose if his answer is
    // positive or negative
    private QuestionGeneratedFeedbackMode generatedFeedbackMode = QuestionGeneratedFeedbackMode.MANUAL;

    // default none (if is none and generatedFeedbackMode:auto then reported feedback is
    // positive by default)
    // format is: [x-x] or [x-x] or (x-x]. Multiple intervals will be separated
    // by ';'
    private String negativeFeedbackInterval;

    // default none (if is none and generatedFeedbackMode:auto then reported feedback is
    // positive by default)
    // format is: [x-x] or [x-x] or (x-x]. Multiple intervals will be separated
    // by ';'
    private String positiveFeedbackInterval;

    public Question() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isEnterable() {
        return enterable;
    }

    public void setEnterable(boolean enterable) {
        this.enterable = enterable;
    }

    public QuestionEnterType getEnterType() {
        return enterType;
    }

    @JsonProperty("enterType")
    @JsonDeserialize(using = QuestionEnterType.Deserializer.class)
    public void setEnterType(QuestionEnterType enterType) {
        this.enterType = enterType;
    }

    public QuestionGeneratedFeedbackMode getGeneratedFeedbackMode() {
        return generatedFeedbackMode;
    }

    @JsonProperty("generatedFeedbackMode")
    @JsonDeserialize(using = QuestionGeneratedFeedbackMode.Deserializer.class)
    public void setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode generatedFeedbackMode) {
        this.generatedFeedbackMode = generatedFeedbackMode;
    }

    public String getNegativeFeedbackInterval() {
        return negativeFeedbackInterval;
    }

    public void setNegativeFeedbackInterval(String negativeFeedbackInterval) {
        this.negativeFeedbackInterval = negativeFeedbackInterval;
    }

    public String getPositiveFeedbackInterval() {
        return positiveFeedbackInterval;
    }

    public void setPositiveFeedbackInterval(String positiveFeedbackInterval) {
        this.positiveFeedbackInterval = positiveFeedbackInterval;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((body == null) ? 0 : body.hashCode());
        result = prime * result + ((enterType == null) ? 0 : enterType.hashCode());
        result = prime * result + (enterable ? 1231 : 1237);
        result = prime * result + ((generatedFeedbackMode == null) ? 0 : generatedFeedbackMode.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((negativeFeedbackInterval == null) ? 0 : negativeFeedbackInterval.hashCode());
        result = prime * result + ((positiveFeedbackInterval == null) ? 0 : positiveFeedbackInterval.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Question other = (Question) obj;
        if (body == null) {
            if (other.body != null)
                return false;
        } else if (!body.equals(other.body))
            return false;
        if (enterType != other.enterType)
            return false;
        if (enterable != other.enterable)
            return false;
        if (generatedFeedbackMode != other.generatedFeedbackMode)
            return false;
        if (id != other.id)
            return false;
        if (negativeFeedbackInterval == null) {
            if (other.negativeFeedbackInterval != null)
                return false;
        } else if (!negativeFeedbackInterval.equals(other.negativeFeedbackInterval))
            return false;
        if (positiveFeedbackInterval == null) {
            if (other.positiveFeedbackInterval != null)
                return false;
        } else if (!positiveFeedbackInterval.equals(other.positiveFeedbackInterval))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Qestion [id=" + id + ", body=" + body + ", enterable=" + enterable + ", enterType=" + enterType
                + ", generatedFeedbackMode=" + generatedFeedbackMode + ", negativeFeedbackInterval="
                + negativeFeedbackInterval + ", positiveFeedbackInterval=" + positiveFeedbackInterval + "]";
    }
}
