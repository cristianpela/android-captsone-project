package org.coursera.capstone.androidclient.gotit.repository;

import java.util.Locale;

import org.coursera.capstone.androidclient.gotit.auth.AppAuthenticatorConstants;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbCheckIn;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbUser;

import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

public final class Contracts {

    private Contracts() {
    }

    public static class Db {
        
        public static class TbUser implements GotItBaseColumns {
            public static final String TB_USER = "user";
            public static final String USER_NAME = "user_name";
            public static final String PASSWORD = "password";
            public static final String IS_TEEN = "is_teen";
            public static final String FIRST_NAME = "first_name";
            public static final String LAST_NAME = "last_name";
            public static final String BIRTH_DATE = "birth_date";
            public static final String MRN = "mrn";
            public static final String PROFILE_PICTURE = "picture";
        }

        public static class TbFollower implements GotItBaseColumns {
            public static final String TB_FOLLOWER = "follower";
            public static final String CONFIRMED = "confirmed";
        }

        public static class TbQuestion implements GotItBaseColumns {
            public static final String TB_QUESTION = "question";
            public static final String BODY = "body";
            public static final String ENTERABLE = "enterable";
            public static final String ENTER_TYPE = "enter_type";
            public static final String GEN_FEEDBACK_MODE = "gen_feedback_mode";
            public static final String POSITIVE_INTERVAL = "positive_interval";
            public static final String NEGATIVE_INTERVAL = "negative_interval";
        }

        public static class TbCheckIn implements GotItBaseColumns {
            public static final String TB_CHECKIN = "check_in";
        }

        public static class TbCheckInShared implements GotItBaseColumns {
            public static final String TB_CHECKIN_SHARED = "check_in_shared";
            public static final String CHECKIN_ID = "chekin_id";
        }

        public static class TbFeedback implements GotItBaseColumns {
            public static final String TB_FEEDBACK = "feedback";
            public static final String TYPE = "type";
            public static final String ANSWER = "answer";
            public static final String QUESTION_ID = "question_id";
            public static final String CHECKIN_ID = "checkin_id";
        }

        public static class TbFeedbackShared implements GotItBaseColumns {
            public static final String TB_FEEDBACK_SHARED = "feedback_shared";
            public static final String FEEDBACK_ID = "feedback_id";
        }

        public static class TbNotification implements GotItBaseColumns {
            public static final String TB_NOTIFICATION = "notification";
            public static final String REQUESTER_USER_NAME = "req_user_name";
            public static final String TYPE = "type";
            public static final String BODY = "body";
        }
    }

    public static class URI {

        public static final String AUTHORITY = AppAuthenticatorConstants.AUTHORITY;
        public static final Uri BASE_CONTENT = Uri.parse("content://" + AUTHORITY);

        public static final String ANY_NUMBER = "#";
        public static final String ANY_STRING = "*";

        public static final String DEFAULT_ID = "0";
        public static final int ONE = 1001;
        public static final int MANY = 1002;
        public static final int ONE_BY_CREATION_DATE = 1003;

        public static final int FOLLOWER = 1004;
        public static final int FOLLOWERS = 1005;
        public static final int FOLLOWED = 1006;

        public static final int CHEKIN = 1007;
        public static final int FEEDBACK = 1008;

        // USER URI
        public static final String USER_PATH = TbUser.TB_USER;

        public static String buildPathManyItems(boolean headless, String table) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%sgeneral/%s/many", new Object[] { schema, table });
        }

        public static String buildPathOneItem(boolean headless, String table, String id) {
            String schema = (headless) ? "" : getSchema();
            return String.format(null, "%sgeneral/%s/one/%s", new Object[] { schema, table, id });
        }

        public static String buildPathOneByCreationDate(boolean headless, String table, String id) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%sgeneral/%s/date/%s", new Object[] { schema, table, id });
        }

        /**
         * @param headless
         *            - has a schema or not: must be "false" when adding this
         *            method to the second argument of matchers
         *            {@link UriMatcher#addURI(String, String, int)}
         * @param teenId
         *            at position 2 in URI
         * @param followerId
         *            at position 4 in URI
         * @return
         */
        public static String buildPathOneFollower(boolean headless, String teenId, String followerId) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%scustom/user/%s/follower/%s",
                    new Object[] { schema, teenId, followerId });
        }

        /**
         * @param headless
         *            - has a schema or not: must be "false" when adding this
         *            method to the second argument of matchers
         *            {@link UriMatcher#addURI(String, String, int)}
         * @param teenId
         *            at position 2 in URI
         * @return
         */
        public static String buildPathManyFollowers(boolean headless, String teenId) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%scustom/user/%s/followers", new Object[] { schema, teenId });
        }

        /**
         * @param headless
         *            - has a schema or not: must be "false" when adding this
         *            method to the second argument of matchers
         *            {@link UriMatcher#addURI(String, String, int)}
         * @param followerId
         *            at position 2 in URI
         * @return
         */
        public static String buildPathManyFollowedTeens(boolean headless, String followerId) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%scustom/user/%s/followed", new Object[] { schema, followerId });
        }

        /**
         * @param headless
         *            - has a schema or not: must be "false" when adding this
         *            method to the second argument of matchers
         *            {@link UriMatcher#addURI(String, String, int)}
         * @param teenId
         *            at position 3 in URI
         * @param followerId
         *            at position 5 in URI <br>
         *            Note: if is a self check in, follower id value must be
         *            {@link Contracts.URI#DEFAULT_ID}
         * @return
         */
        public static String buildPathManyCheckIns(boolean headless, String teenId, String followerId) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%scustom/" + TbCheckIn.TB_CHECKIN + "/teen/%s/follower/%s", new Object[] {
                    schema, teenId, followerId });
        }

        /**
         * @param headless
         *            - has a schema or not: must be "false" when adding this
         *            method to the second argument of matchers
         *            {@link UriMatcher#addURI(String, String, int)}
         * @param feedbackId
         *            at position 3 in URI
         * @param followerId
         *            at position 5 in URI<br>
         *            Note: if is a self check in, follower id value must be
         *            {@link Contracts.URI#DEFAULT_ID}
         * @return
         */
        public static String buildPathManyFeedback(boolean headless, String feedbackId, String followerId) {
            String schema = (headless) ? "" : getSchema();
            return String.format(Locale.US, "%scustom/" + TbCheckIn.TB_CHECKIN + "/feedback/%s/follower/%s",
                    new Object[] { schema, feedbackId, followerId });
        }

        public static String getTableNameFromPath(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static String getStringIdFromPath(Uri uri) {
            return uri.getPathSegments().get(3);
        }

        public static String getStringIdFromPath(Uri uri, int pos) {
            return uri.getPathSegments().get(pos);
        }

        public static int getIdFromPath(Uri uri) {
            return Integer.parseInt(getStringIdFromPath(uri));
        }

        public static int getIdFromPath(Uri uri, int pos) {
            return Integer.parseInt(getStringIdFromPath(uri, pos));
        }

        /**
         * "content://" + AUTHORITY won't work on API < 18. Must add a "/" after.
         */
        //FIXME "content://" + AUTHORITY+"/"
        private static String getSchema() {
            return "content://" + AUTHORITY+"/";
        }

    }

}
