package org.coursera.capstone.androidclient.gotit.repository;

import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.USER_PATH;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.buildPathOneItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbFollower;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbUser;
import org.coursera.capstone.androidclient.gotit.repository.Contracts.URI;
import org.coursera.capstone.androidclient.gotit.rest.CheckIn;
import org.coursera.capstone.androidclient.gotit.rest.Gift;
import org.coursera.capstone.androidclient.gotit.rest.GotItConnection;
import org.coursera.capstone.androidclient.gotit.rest.GotItURLMapping;
import org.coursera.capstone.androidclient.gotit.rest.GotItURLService;
import org.coursera.capstone.androidclient.gotit.rest.Notification;
import org.coursera.capstone.androidclient.gotit.rest.Question;
import org.coursera.capstone.androidclient.gotit.rest.SecuredRestException;
import org.coursera.capstone.androidclient.gotit.rest.User;

import retrofit.client.Response;
import retrofit.mime.TypedFile;
import android.accounts.Account;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.shapes.PathShape;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;

public class GotItRepository implements Repository {

    private static final String TAG = GotItRepository.class.getSimpleName();

    private GotItConnection connection = GotItConnection.getInstance();

    private GotItURLService service;

    private Account account;

    private ContentProviderClient clientProvider;

    private Context context;

    public GotItRepository(Account account, ContentProviderClient clientProvider) {
        service = connection.getRESTInterface();
        this.account = account;
        this.clientProvider = clientProvider;
    }

    public GotItRepository(Context context, Account account, ContentProviderClient clientProvider) {
        service = connection.getRESTInterface();
        this.account = account;
        this.clientProvider = clientProvider;
        this.context = context;
    }

    public GotItRepository(ContentProviderClient clientProvider) {
        service = connection.getRESTInterface();
        this.clientProvider = clientProvider;
    }

    @Override
    public String logIn(String userName, String password) throws SecuredRestException, IOException {
        return connection.logIn(userName, password);
    }

    @Override
    public void signUp(User user) throws SecuredRestException, IOException, RemoteException {
        User signedUser = service.signUp(user);
        String imageProfilePath = user.getImageProfilePath();
        if (imageProfilePath != null) {
            String userName = user.getUserName();
            File pictureFile = savePictureProfileLocallyFromPath(userName, user.getImageProfilePath());
            service.postAvatarData(userName, new TypedFile("image/png", pictureFile));
            user.setImageProfilePath(pictureFile.getAbsolutePath());
        }
        user.setModificationDate(signedUser.getModificationDate());
        saveUserLocally(user);
    }

    @Override
    public User findUser(String userName) throws SecuredRestException, RemoteException {
        // TODO(gotit) make this sync with local
        userName = (userName == null) ? account.name : userName;
        return service.getUserByName(userName);
    }

    @Override
    public List<User> findUsers(String nameCriteria) throws SecuredRestException, RemoteException {
        List<User> users = service.findAllTeensLike(nameCriteria);
        // TODO(gotit) remove this? already is a custom image downloader
        for (User user : users) {
            String avatarPath = GotItConnection.stringifyPath(GotItURLMapping.AVATAR_PATH,
                    new String[] { user.getUserName() });
            user.setImageProfilePath(avatarPath);
        }
        return users;
    }

    @Override
    public List<Notification> getNotifications() throws SecuredRestException, RemoteException {
        /*
         * long id = findUserId(account.name); List<Notification> notifications
         * = new ArrayList<Notification>(); Cursor cursor =
         * clientProvider.query(Uri.parse(URI.buildPathManyFollowers(false, "" +
         * id)), null, null, null, null); List<Long> followedIDs = new
         * ArrayList<Long>(); if(cursor != null && cursor.moveToFirst()){ do{
         * long confirmed =
         * cursor.getLong(cursor.getColumnIndexOrThrow(TbFollower.CONFIRMED));
         * if(confirmed == 0){ long followedId =
         * cursor.getLong(cursor.getColumnIndexOrThrow(TbFollower.FOLLOWER_ID));
         * followedIDs.add(followedId); } }while(cursor.moveToNext());
         * cursor.close(); }
         */

        return service.getNotifications();
    }

    @Override
    public void requestFollowing(String teenUserName) throws SecuredRestException, RemoteException,
            FileNotFoundException, IOException {
        service.requestForFollowingTeen(teenUserName);
        User teen = getLocallySavedUser(teenUserName);
        if (teen == null) {
            teen = service.getUserByName(teenUserName);
            Response pictureDataResponse = service.getAvatarData(teenUserName);
            if (pictureDataResponse.getStatus() == 200) {
                File savedPictureFile = savePictureProfileLocallyFromStream(teenUserName, pictureDataResponse.getBody()
                        .in());
                teen.setImageProfilePath(savedPictureFile.getAbsolutePath());
            }
            saveUserLocally(teen);
            addFollower(teenUserName, account.name, false);
        }
    }

    @Override
    public void confirmFollowing(String confirmFollowerUserName) throws SecuredRestException, RemoteException {
        service.confirmFollower(confirmFollowerUserName);
    }

    @Override
    public void acknowledgeCheckInRequests() throws SecuredRestException, RemoteException {
        service.acknowledgeCheckInRequests();
    }

    @Override
    public void requestCheckIn(String teenUserName) throws SecuredRestException, RemoteException {
        service.requestCheckIn(teenUserName);
    }

    @Override
    public List<Question> getAllQuestions() throws SecuredRestException, RemoteException {
        List<Question> questions = new ArrayList<Question>();
        
        
        
        return service.getAllQuestions(new Date(0).getTime());
    }

    @Override
    public List<CheckIn> getTeenCheckInHistroy(String teenUserName) throws SecuredRestException, RemoteException {
        User teen = findUser(teenUserName);
        String fullName = teen.getUserDetails().getFirstName() + " " + teen.getUserDetails().getLastName();
        // TODO(gotit) sync this with local database if have time....
        List<CheckIn> checkIns = service.getTeenCheckInHistroy(new Date(0).getTime(), teenUserName);
        for (CheckIn chk : checkIns)
            chk.setAuthorFullName(fullName);
        return checkIns;
    }

    @Override
    public void postCheckIn(CheckIn checkIn) throws SecuredRestException, RemoteException {
        service.postCheckIn(checkIn);
    }

    private void addFollower(String teenUserName, String followerUserName, boolean confirmed) throws RemoteException {
        long teenId = -1, followerId = -1;

        // getting ids;
        Uri uri = Uri.parse(URI.buildPathManyItems(false, TbUser.TB_USER));
        Cursor cursor = clientProvider.query(uri, new String[] { TbUser._ID, TbUser.USER_NAME }, TbUser.USER_NAME
                + "=? AND " + TbUser.USER_NAME + "=?", new String[] { teenUserName, followerUserName }, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                if (cursor.getString(cursor.getColumnIndexOrThrow(TbUser.USER_NAME)).equals(teenUserName)) {
                    teenId = cursor.getLong(cursor.getColumnIndexOrThrow(TbUser._ID));
                } else
                    followerId = cursor.getLong(cursor.getColumnIndexOrThrow(TbUser._ID));
            } while (cursor.moveToNext());
            cursor.close();
        }
        // adding follower
        if (teenId == -1 || followerId == -1)
            throw new RemoteException();

        ContentValues cv = new ContentValues();
        cv.put(TbFollower.CONFIRMED, (confirmed) ? 1 : 0);
        uri = Uri.parse(URI.buildPathOneFollower(false, "" + teenId, "" + followerId));
        clientProvider.insert(uri, cv);
    }

    private File savePictureProfileLocallyFromPath(String userName, String path) throws FileNotFoundException {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        File pictureFile = savePictureData(userName, bitmap);
        return pictureFile;
    }

    private File savePictureProfileLocallyFromStream(String userName, InputStream is) throws FileNotFoundException {
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        File pictureFile = savePictureData(userName, bitmap);
        return pictureFile;
    }

    private File savePictureData(String userName, Bitmap bitmap) throws FileNotFoundException {
        File dir = context.getDir("AVATAR_DIR", Context.MODE_PRIVATE);
        if (!dir.exists())
            dir.mkdir();
        File pictureFile = new File(dir, userName + ".png");
        if (pictureFile.exists())
            pictureFile.delete();
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, 122, 122);
        FileOutputStream fos = new FileOutputStream(pictureFile);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        return pictureFile;
    }

    public void saveUserLocally(User user) throws RemoteException {
        Uri uri = Uri.parse(buildPathOneItem(false, USER_PATH, URI.DEFAULT_ID));
        ContentValues cv = Convertors.userToContentValues(user);
        clientProvider.insert(uri, cv);
    }

    public User getLocallySavedUser(String userName) throws RemoteException {
        Uri uri = Uri.parse(URI.buildPathManyItems(false, TbUser.TB_USER));
        Cursor cursor = clientProvider.query(uri, null, TbUser.USER_NAME + "=?", new String[] { userName }, null);
        User user = null;
        if (cursor != null && cursor.moveToFirst()) {
            user = Convertors.cursorToUser(cursor);
            cursor.close();
        }
        return user;
    }

    private long findUserId(String userName) throws RemoteException {
        long id = -1;
        Uri uri = Uri.parse(URI.buildPathManyItems(false, TbUser.TB_USER));
        Cursor cursor = clientProvider.query(uri, new String[] { TbUser._ID }, TbUser.USER_NAME + "=?",
                new String[] { userName }, null);
        if (cursor != null && cursor.moveToFirst()) {
            id = cursor.getLong(cursor.getColumnIndexOrThrow(TbUser._ID));
            cursor.close();
        }
        return id;
    }

    @Override
    public InputStream getGiftPhotoData(String photoFileName, boolean locallySaved) throws RemoteException, IOException {
        InputStream is;
        if (locallySaved) {
            is = new FileInputStream(photoFileName);
        } else {
            is = service.getGiftPhotoData(photoFileName).getBody().in();
        }
        return is;
    }

    private String getLocalSavedGiftPhoto(String photoFileName) throws RemoteException {
        String pathColumn = MediaStore.Images.ImageColumns.DATA;
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[] { pathColumn },
                null, null, null);

        String path = null;
        if (cursor != null && cursor.moveToFirst()) {
            do {
                path = cursor.getString(cursor.getColumnIndex(pathColumn));
                if (path.endsWith(photoFileName))
                    break;
                path = null;
            } while (cursor.moveToNext());
            cursor.close();
        }
        return path;
    }

    @Override
    public List<Gift> getAllGifts() throws SecuredRestException, RemoteException {
        List<Gift> gifts = service.getAllGifts();
        for (Gift gift : gifts) {
            String localPath = getLocalSavedGiftPhoto(gift.getPhotoFileName());
            if (localPath != null) {
                gift.setPhotoFileName(localPath);
                gift.setLocallySaved(true);
            }
        }
        return gifts;
    }

    public static ContentProviderClient createClientProvider(Context context) {
        return context.getContentResolver().acquireContentProviderClient(Contracts.URI.AUTHORITY);
    }
    
    @Override
    public void postGiftPhoto(String teenUserName, String photoFileName) {
        service.postGiftPhotoData(teenUserName, new TypedFile("image/png", new File(photoFileName)));
    }

    @Override
    public void sync() throws SecuredRestException, RemoteException {
        //syncing questions
        
    }
}
