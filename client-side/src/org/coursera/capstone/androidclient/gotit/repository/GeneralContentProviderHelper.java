package org.coursera.capstone.androidclient.gotit.repository;

import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.*;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.*;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class GeneralContentProviderHelper {

    protected SQLiteDatabase db;

    protected UriMatcher matcher;

    protected String tableName;

    public GeneralContentProviderHelper(SQLiteDatabase db, String tableName, UriMatcher matcher) {
        this.db = db;
        this.matcher = matcher;
        this.tableName = tableName;
    }

    public Uri insert(Uri uri, ContentValues values) {
        long id = -1;
        int match = matcher.match(uri);
        switch (match) {
        case ONE:
            id = db.insertOrThrow(tableName, null, values);
            // id = (id == -1) ? 0 : id;
            break;
        default:
            break;
        }
        return Uri.parse(buildPathOneItem(false, tableName, "" + id));
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String groupBy,
            String having, String sortOrder) {

        String tableName = getTableNameFromPath(uri);
        Cursor cursor = null;

        int match = matcher.match(uri);
        switch (match) {
        case ONE:
            SQLiteQueryBuilder query = new SQLiteQueryBuilder();
            query.setTables(tableName);
            query.appendWhere(BaseColumns._ID + "=");
            query.appendWhere(getStringIdFromPath(uri));
            cursor = query.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            break;
        case MANY:
            cursor = db.query(tableName, projection, selection, selectionArgs, groupBy, having, sortOrder);
            break;
        default:
            break;
        }
        return cursor;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int id = getIdFromPath(uri);
        int match = matcher.match(uri);
        switch (match) {
        case ONE:
            id = db.update(tableName, values, BaseColumns._ID + "=?", new String[] { Integer.toString(id) });
            break;
        default:
            break;
        }
        return id;
    }

    public int bulkInsert(Uri uri, ContentValues[] values) {
        int numValues = values.length;
        for (int i = 0; i < numValues; i++) {
            insert(uri, values[i]);
        }
        return numValues;
    }

    public static GeneralContentProviderHelper factoryHelper(Uri uri, SQLiteDatabase db, UriMatcher matcher) {
        String tableName = getTableNameFromPath(uri);
        if (tableName.equals(TbUser.TB_USER))
            return new UserContentProviderHelper(db, matcher);
        if (tableName.equals(TbCheckIn.TB_CHECKIN))
            return new CheckInContentProviderHelper(db, tableName, matcher);
        return new GeneralContentProviderHelper(db, tableName, matcher);
    }
}
