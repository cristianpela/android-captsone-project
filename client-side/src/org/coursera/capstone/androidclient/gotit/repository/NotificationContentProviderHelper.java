package org.coursera.capstone.androidclient.gotit.repository;

import android.content.UriMatcher;
import android.database.sqlite.SQLiteDatabase;

public class NotificationContentProviderHelper extends GeneralContentProviderHelper {

    public NotificationContentProviderHelper(SQLiteDatabase db, String tableName, UriMatcher matcher) {
        super(db, tableName, matcher);
    }

}
