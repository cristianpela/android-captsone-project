package org.coursera.capstone.androidclient.gotit.repository;

import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.*;

import org.coursera.capstone.androidclient.gotit.repository.Contracts.URI;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class GotItContentProvider extends ContentProvider {

    private static UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {

        matcher.addURI(AUTHORITY, buildPathOneItem(true, ANY_STRING, ANY_NUMBER), ONE);
        matcher.addURI(AUTHORITY, buildPathManyItems(true, ANY_STRING), MANY);
        matcher.addURI(AUTHORITY, buildPathOneByCreationDate(true, ANY_STRING, ANY_NUMBER), ONE_BY_CREATION_DATE);

        // follower matchers
        matcher.addURI(AUTHORITY, buildPathOneFollower(true, ANY_NUMBER, ANY_NUMBER), FOLLOWER);
        matcher.addURI(AUTHORITY, buildPathManyFollowers(true, ANY_NUMBER), FOLLOWERS);
        matcher.addURI(AUTHORITY, buildPathManyFollowedTeens(true, ANY_NUMBER), FOLLOWED);

        // checkin & feedback matchers
        matcher.addURI(AUTHORITY, buildPathManyCheckIns(true, ANY_NUMBER, ANY_NUMBER), CHEKIN);
        matcher.addURI(AUTHORITY, buildPathManyFeedback(true, ANY_NUMBER, ANY_NUMBER), FEEDBACK);
    }

    private GotItDbHelper dbHelper;

    public GotItContentProvider() {
    }

    @Override
    public boolean onCreate() {
        dbHelper = new GotItDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        GeneralContentProviderHelper helper = GeneralContentProviderHelper.factoryHelper(uri, db, matcher);
        return helper.query(uri, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        GeneralContentProviderHelper helper = GeneralContentProviderHelper.factoryHelper(uri, db, matcher);
        return helper.insert(uri, values);
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        GeneralContentProviderHelper helper = GeneralContentProviderHelper.factoryHelper(uri, db, matcher);
        return helper.bulkInsert(uri, values);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        GeneralContentProviderHelper helper = GeneralContentProviderHelper.factoryHelper(uri, db, matcher);
        return helper.update(uri, values, selection, selectionArgs);
    }

}
