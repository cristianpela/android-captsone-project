package org.coursera.capstone.androidclient.gotit.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.*;

class GotItDbHelper extends SQLiteOpenHelper {
    
    public static final int DB_VERSION = 2;

    public static final String DB_NAME = "gotit.db";

    private static final String CREATE_TB_USER_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbUser.TB_USER + "(" +
            TbUser._ID + " INTEGER NOT NULL PRIMARY KEY, " +
            TbUser.CREATION_DATE + " INTEGER, " +
            TbUser.USER_NAME +" TEXT NOT NULL, " +
            TbUser.PASSWORD +" TEXT NOT NULL, " +
            TbUser.FIRST_NAME + " TEXT NOT NULL, " +
            TbUser.LAST_NAME + " TEXT NOT NULL, " +
            TbUser.IS_TEEN +" INTEGER DEFAULT 0, "+
            TbUser.BIRTH_DATE + " INTEGER, " +
            TbUser.MRN + " INTEGER, " +
            TbUser.PROFILE_PICTURE + " TEXT);";
    
    private static final String CREATE_TB_FOLLOWER_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbFollower.TB_FOLLOWER + "(" +
            TbFollower._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbFollower.CONFIRMED +  " INTEGER DEFAULT 0, " +
            TbFollower.TEEN_ID +  " INTEGER NOT NULL, " +
            TbFollower.FOLLOWER_ID +  " INTEGER NOT NULL);";
    
    private static final String CREATE_TB_QUESTION_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbQuestion.TB_QUESTION + "(" +
            TbQuestion._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbQuestion.CREATION_DATE + " INTEGER, " +
            TbQuestion.BODY +  " TEXT NOT NULL, " +
            TbQuestion.ENTERABLE +  " INTEGER DEFAULT 0, " +
            TbQuestion.ENTER_TYPE + " TEXT, " +
            TbQuestion.GEN_FEEDBACK_MODE + " TEXT, " +
            TbQuestion.POSITIVE_INTERVAL + " TEXT, " +
            TbQuestion.NEGATIVE_INTERVAL + " TEXT);";
    
    private static final String CREATE_TB_CHECKIN_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbCheckIn.TB_CHECKIN + "(" +
            TbCheckIn._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbCheckIn.TEEN_ID +  " INTEGER NOT NULL, " +
            TbCheckIn.CREATION_DATE +  " INTEGER);";
    
    private static final String CREATE_TB_CHECKIN_SHARED_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbCheckInShared.TB_CHECKIN_SHARED + "(" +
            TbCheckInShared._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbCheckInShared.CHECKIN_ID + " INTEGER NOT NULL, "+
            TbCheckInShared.FOLLOWER_ID +  " INTEGER NOT NULL);";
    
    private static final String CREATE_TB_FEEDACK_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbFeedback.TB_FEEDBACK + "(" +
            TbFeedback._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbFeedback.CHECKIN_ID +  " INTEGER NOT NULL, " +
            TbFeedback.QUESTION_ID +  " INTEGER NOT NULL, "+
            TbFeedback.TYPE +  " TEXT, "+
            TbFeedback.ANSWER +  " TEXT);";
    
    private static final String CREATE_TB_FEEDACK_SHARED_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbFeedbackShared.TB_FEEDBACK_SHARED + "(" +
            TbFeedbackShared._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbFeedbackShared.FEEDBACK_ID +  " INTEGER NOT NULL, " +
            TbFeedbackShared.FOLLOWER_ID +  " INTEGER NOT NULL);";
    
    private static final String CREATE_TB_NOTIFICATION_SCRIPT = "CREATE TABLE IF NOT EXISTS " + TbNotification.TB_NOTIFICATION + "(" +
            TbNotification._ID +  " INTEGER NOT NULL PRIMARY KEY, " +
            TbNotification.TEEN_ID +  " INTEGER NOT NULL, " +
            TbNotification.REQUESTER_USER_NAME + "TEXT, " +
            TbNotification.TYPE +  " INTEGER DEFAULT 0);";

    public GotItDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TB_USER_SCRIPT);
        db.execSQL(CREATE_TB_FOLLOWER_SCRIPT);
        db.execSQL(CREATE_TB_QUESTION_SCRIPT);
        db.execSQL(CREATE_TB_CHECKIN_SCRIPT);
        db.execSQL(CREATE_TB_CHECKIN_SHARED_SCRIPT);
        db.execSQL(CREATE_TB_FEEDACK_SCRIPT);
        db.execSQL(CREATE_TB_FEEDACK_SHARED_SCRIPT);
        db.execSQL(CREATE_TB_NOTIFICATION_SCRIPT);
        Log.d("DB", "Local database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TbNotification.TB_NOTIFICATION);
        db.execSQL("DROP TABLE IF EXISTS " + TbFeedbackShared.TB_FEEDBACK_SHARED);
        db.execSQL("DROP TABLE IF EXISTS " + TbFeedback.TB_FEEDBACK);
        db.execSQL("DROP TABLE IF EXISTS " + TbCheckInShared.TB_CHECKIN_SHARED);
        db.execSQL("DROP TABLE IF EXISTS " + TbCheckIn.TB_CHECKIN);
        db.execSQL("DROP TABLE IF EXISTS " + TbQuestion.TB_QUESTION);      
        db.execSQL("DROP TABLE IF EXISTS " + TbFollower.TB_FOLLOWER);
        db.execSQL("DROP TABLE IF EXISTS " + TbUser.TB_USER);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    
    

}
