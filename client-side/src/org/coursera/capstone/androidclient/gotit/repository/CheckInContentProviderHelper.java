package org.coursera.capstone.androidclient.gotit.repository;

import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.*;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.*;

import java.util.ArrayList;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbFeedbackShared;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class CheckInContentProviderHelper extends GeneralContentProviderHelper {

    public CheckInContentProviderHelper(SQLiteDatabase db, String tableName, UriMatcher matcher) {
        super(db, tableName, matcher);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String groupBy,
            String having, String sortOrder) {

        Cursor cursor = super.query(uri, projection, selection, selectionArgs, groupBy, having, sortOrder);

        int match = matcher.match(uri);  
        switch (match) {
        case CHEKIN:
            cursor = queryCheckIns(uri, projection, selection, selectionArgs, groupBy, having, sortOrder);
            break;
        case FEEDBACK:
            cursor = queryFeedbacks(uri, projection, selection, selectionArgs, groupBy, having, sortOrder);
            break;
        }

        return cursor;
    }

    private Cursor queryCheckIns(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String groupBy, String having, String sortOrder) {

        String teenId = getStringIdFromPath(uri, 3);
        String followerId = getStringIdFromPath(uri, 5);

        boolean self = followerId.equals(DEFAULT_ID);

        StringBuilder queryBuilder = new StringBuilder("SELECT chk.* FROM " + TbCheckIn.TB_CHECKIN + " AS chk");
        if (!self){
            queryBuilder.append(", " + TbCheckInShared.TB_CHECKIN_SHARED + " AS chks");
        }
        queryBuilder.append(" WHERE chk." + TbCheckIn.TEEN_ID + "=?");
        if (!self){
            queryBuilder.append(" AND chk." + TbCheckIn._ID + "=chks." + TbCheckInShared.CHECKIN_ID);
            queryBuilder.append(" AND chks." + TbCheckInShared.FOLLOWER_ID + "=?");
        }
        
        String[] args = (self) ? new String[] { teenId } : new String[] { teenId, followerId };
        return db.rawQuery(queryBuilder.toString(), args);
    }

    private Cursor queryFeedbacks(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String groupBy, String having, String sortOrder) {

        String checkInId = getStringIdFromPath(uri, 3);
        String followerId = getStringIdFromPath(uri, 5);
        boolean self = followerId.equals(DEFAULT_ID);

        StringBuilder queryBuilder = new StringBuilder("SELECT feed.*, q." + TbQuestion.BODY + " FROM "
                + TbFeedback.TB_FEEDBACK + " AS feed, " + TbQuestion.TB_QUESTION + " AS q, " + TbCheckIn.TB_CHECKIN
                + " AS chk");
        if (!self)
            queryBuilder.append(", " + TbFeedbackShared.TB_FEEDBACK_SHARED + " AS feedsh");
        queryBuilder.append(" WHERE feed." + TbFeedback.CHECKIN_ID + "=? AND q." + TbQuestion._ID + "=feed."
                + TbFeedback.QUESTION_ID);
        if (!self) {
            queryBuilder.append(" AND feedsh." + TbFeedbackShared.FEEDBACK_ID + "=feed." + TbFeedback._ID);
            queryBuilder.append(" AND feedsh." + TbFeedbackShared.FOLLOWER_ID + "=?");
        }
        System.out.println(queryBuilder);

        String[] args = (self) ? new String[] { checkInId } : new String[] { checkInId, followerId };
        return db.rawQuery(queryBuilder.toString(), args);
    }
}
