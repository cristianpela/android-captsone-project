package org.coursera.capstone.androidclient.gotit.repository;

import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbUser.*;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.TbFollower.*;
import static org.coursera.capstone.androidclient.gotit.repository.Contracts.URI.*;


import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class UserContentProviderHelper extends GeneralContentProviderHelper {

    public UserContentProviderHelper(SQLiteDatabase db, UriMatcher matcher) {
        super(db, TB_USER, matcher);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String groupBy,
            String having, String sortOrder) {

        Cursor cursor = super.query(uri, projection, selection, selectionArgs, groupBy, having, sortOrder);

        int match = matcher.match(uri);
        switch (match) {
        case ONE_BY_CREATION_DATE:
            cursor = returnLastUpdateDate(getIdFromPath(uri), selection, selectionArgs);
            break;
        case FOLLOWERS:
            cursor = queryFollowers(getIdFromPath(uri, 2));
            break;
        case FOLLOWED:
            cursor = queryFollowed(getIdFromPath(uri, 2));
            break;
        default:
            break;
        }

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri resultUri = super.insert(uri, values);
        int match = matcher.match(uri);
        switch (match) {
        case FOLLOWER:
            resultUri = insertFollower(uri, values);
            break;
        default:
            break;
        }
        return resultUri;
    }

    private Uri insertFollower(Uri uri, ContentValues values) {

        int teenId = getIdFromPath(uri, 2);
        int followerId = getIdFromPath(uri, 4);
        
        values.put(TEEN_ID, teenId);
        values.put(FOLLOWER_ID, followerId);
        long result = db.insert(TB_FOLLOWER, null, values);
        followerId = (int) ((result == -1) ? result : followerId);

        return Uri.parse(buildPathOneItem(false, tableName, "" + followerId));
    }

    //TODO(gotit): Get rid of it? Enough using MANY with Selection clause?
    private Cursor returnLastUpdateDate(int id, String selection, String[] selectionArgs) {
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(tableName);
        query.appendWhere(_ID + "=");
        query.appendWhere("" + id);
        return query.query(db, new String[] { CREATION_DATE }, selection, selectionArgs, null, null, null);
    }

    private Cursor queryFollowers(int id) {
        return db.query(TB_FOLLOWER, null, TEEN_ID + "=?", new String[] { "" + id }, null, null, null);
    }

    private Cursor queryFollowed(int id) {
        return db.query(TB_FOLLOWER, null, FOLLOWER_ID + "=?", new String[] { "" + id }, null, null, null);
    }

}
