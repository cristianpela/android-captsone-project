package org.coursera.capstone.androidclient.gotit.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.coursera.capstone.androidclient.gotit.rest.CheckIn;
import org.coursera.capstone.androidclient.gotit.rest.Gift;
import org.coursera.capstone.androidclient.gotit.rest.Notification;
import org.coursera.capstone.androidclient.gotit.rest.Question;
import org.coursera.capstone.androidclient.gotit.rest.SecuredRestException;
import org.coursera.capstone.androidclient.gotit.rest.User;

import android.os.RemoteException;

public interface Repository {

    public String logIn(String userName, String password) throws IOException, SecuredRestException;

    public void signUp(User user) throws IOException, SecuredRestException, RemoteException;

    public User findUser(String userName) throws SecuredRestException, RemoteException;

    public List<User> findUsers(String nameCriteria) throws SecuredRestException, RemoteException;

    public void requestFollowing(String teenUserName) throws SecuredRestException, RemoteException,
            FileNotFoundException, IOException;

    public List<Notification> getNotifications() throws SecuredRestException, RemoteException;

    public void confirmFollowing(String confirmFollowerUserName) throws SecuredRestException, RemoteException;

    public void acknowledgeCheckInRequests() throws SecuredRestException, RemoteException;

    public void requestCheckIn(String teenUserName) throws SecuredRestException, RemoteException;

    public List<Question> getAllQuestions() throws SecuredRestException, RemoteException;

    public List<CheckIn> getTeenCheckInHistroy(String teenUserName) throws SecuredRestException, RemoteException;

    public void postCheckIn(CheckIn checkIn) throws SecuredRestException, RemoteException;
    
    public void postGiftPhoto(String teenUserName, String photoFileName) throws SecuredRestException;
     
    public List<Gift> getAllGifts()  throws SecuredRestException, RemoteException;

    public InputStream getGiftPhotoData(String photoFileName, boolean locallySaved) throws RemoteException, IOException;
    
    public void sync() throws SecuredRestException, RemoteException;

}
