package org.coursera.capstone.androidclient.gotit.repository;

import android.provider.BaseColumns;

public interface GotItBaseColumns extends BaseColumns {

    public static final String CREATION_DATE = "creation_date";
    
    public static final String TEEN_ID = "teen_id";
    
    public static final String FOLLOWER_ID = "follower_id";
}
