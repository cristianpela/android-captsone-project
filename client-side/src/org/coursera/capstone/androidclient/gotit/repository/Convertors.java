package org.coursera.capstone.androidclient.gotit.repository;

import java.util.Date;

import org.coursera.capstone.androidclient.gotit.rest.User;
import org.coursera.capstone.androidclient.gotit.rest.UserDetails;

import static org.coursera.capstone.androidclient.gotit.repository.Contracts.Db.*;
import android.content.ContentValues;
import android.database.Cursor;

public final class Convertors {

    private Convertors() {
    }

    public static ContentValues userToContentValues(User user) {
        ContentValues cv = new ContentValues();
        cv.put(TbUser.CREATION_DATE, user.getModificationDate().getTime());
        cv.put(TbUser.USER_NAME, user.getUserName());
        cv.put(TbUser.PASSWORD, user.getPassword());
        cv.put(TbUser.IS_TEEN, user.isTeen());
        if(user.getImageProfilePath()!= null)
            cv.put(TbUser.PROFILE_PICTURE, user.getImageProfilePath());

        UserDetails ud = user.getUserDetails();
        cv.put(TbUser.FIRST_NAME, ud.getFirstName());
        cv.put(TbUser.LAST_NAME, ud.getLastName());
        cv.put(TbUser.BIRTH_DATE, (user.isTeen()) ? ud.getBirthDate().getTime() : -1);
        cv.put(TbUser.MRN, ud.getMedicalRecordNumber());
        return cv;
    }

    public static User cursorToUser(Cursor cursor) {
        User user = new User();
        user.setId(cursor.getLong(cursor.getColumnIndex(TbUser._ID)));
        user.setModificationDate(new Date(cursor.getLong(cursor.getColumnIndex(TbUser.CREATION_DATE))));
        user.setImageProfilePath(cursor.getString(cursor.getColumnIndex(TbUser.PROFILE_PICTURE)));
        user.setPassword(cursor.getString(cursor.getColumnIndex(TbUser.PASSWORD)));
        user.setUserName(cursor.getString(cursor.getColumnIndex(TbUser.USER_NAME)));
        user.setTeen(cursor.getInt(cursor.getColumnIndex(TbUser.IS_TEEN)) == 1);
        UserDetails ud = new UserDetails();
        ud.setFirstName(cursor.getString(cursor.getColumnIndex(TbUser.FIRST_NAME)));
        ud.setLastName(cursor.getString(cursor.getColumnIndex(TbUser.LAST_NAME)));
        ud.setBirthDate(new Date(cursor.getLong(cursor.getColumnIndex(TbUser.BIRTH_DATE))));
        ud.setMedicalRecordNumber(cursor.getLong(cursor.getColumnIndex(TbUser.MRN)));
        user.setUserDetails(ud);
        return user;
    }
}
