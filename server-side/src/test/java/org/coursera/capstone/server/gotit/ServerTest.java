package org.coursera.capstone.server.gotit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.coursera.capstone.server.gotit.api.Answer;
import org.coursera.capstone.server.gotit.api.CheckIn;
import org.coursera.capstone.server.gotit.api.CheckInRequest;
import org.coursera.capstone.server.gotit.api.Feedback;
import org.coursera.capstone.server.gotit.api.Gift;
import org.coursera.capstone.server.gotit.api.GotItURLService;
import org.coursera.capstone.server.gotit.api.Question;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.integration.GotItConnection;
import org.coursera.capstone.server.gotit.integration.SecuredRestBuilder;
import org.coursera.capstone.server.gotit.integration.SecuredRestException;
import org.coursera.capstone.server.gotit.integration.TestUtils;
import org.coursera.capstone.server.gotit.integration.UnsafeHttpsClient;
import org.coursera.capstone.server.gotit.integration.UnsafeHttpsClient2;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpStatus;

import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.ApacheClient;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.converter.JacksonConverter;
import retrofit.mime.FormUrlEncodedTypedOutput;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServerTest {

    private final String CLIENT_ID = "mobile";
    // private final String READ_ONLY_CLIENT_ID = "mobileReader";

    private final String TEST_URL = "https://localhost:8443";
    private final String TOKEN_PATH = "/oauth/token";

    private Client client = new OkClient(UnsafeHttpsClient2.getUnsafeOkHttpClient());
   // private SecuredRestBuilder restAdapter;
    
    private GotItConnection conn;
    private GotItURLService svc;
    private User user0;

    public ServerTest() {
        conn = new GotItConnection(client);
        //restAdapter = new SecuredRestBuilder().setEndpoint(TEST_URL).setLogLevel(LogLevel.HEADERS).setClient(client)
      //          .setConverter(new JacksonConverter(new ObjectMapper()));
        //svc = restAdapter.build().create(GotItURLService.class);
        svc = conn.getRESTInterface();
    }

    // @Test
    public void A_signUpTest() throws FileNotFoundException, IOException {
        User user = TestUtils.createRandomRESTUser();
        User signedUser = svc.signUp(user);
        assertEquals(user.getUserName(), signedUser.getUserName());
        assertTrue(svc.postAvatarData(signedUser.getUserName(), TestUtils.avatarTypedFile));
        assertAvatarsAreEquals(svc, signedUser, TestUtils.avatarImg);
    }

    @Test
    public void B_test_credentials_and_avatars_creation() throws IOException {
        logInUser0Teen();
        
        svc.getUserByName(TestUtils.user0UserName);
        
        assertEquals(TestUtils.user0UserName, user0.getUserName());
        svc.postAvatarData(user0.getUserName(), TestUtils.avatarTypedFile); // add
                                                                            // avatar
        svc.postAvatarData(user0.getUserName(), TestUtils.avatarTypedFile2); // update
                                                                             // avatar
        assertAvatarsAreEquals(svc, user0, TestUtils.avatarImg2);
        assertEquals(HttpStatus.OK.value(), svc.deleteAvatarData(user0.getUserName()).getStatus()); // delete
                                                                                                    // avatar
    }

    @Test(expected = RetrofitError.class)
    public void C_testing_no_avatar() throws IOException {
        logInUser0Teen();
        svc.getAvatarData(user0.getUserName()); // <=== 404 [NO AVATAR]
    }

   // @Test(expected = RetrofitError.class)
    public void D_testing_sing_up_with_an_existing_username() {
        logOut();
        User user = TestUtils.createRandomRESTUser();
        user.setUserName(TestUtils.user0UserName);
        svc.signUp(user);
    }
    
    @Test(expected = SecuredRestException.class)
    public void D1_login_with_bad_credentials() throws IOException{
        conn.logIn("xxx", "xxx");
    }

    @Test(expected = RetrofitError.class)
    public void E_getting_all_teens_not_logged_in() {
        logOut();
        svc.findAllTeens(); // < 401 UnAuthorized;
    }

    //@Test
    public void F_getting_all_teens() throws IOException {
        logInUser0Teen();
        List<User> teens = svc.findAllTeens();
        assertEquals(2, teens.size());
    }

    @Test
    public void G_getting_all_teens_by_criteria() throws IOException {
        logInUser0Teen();
        String criteria = "user";
        List<User> teens = svc.findAllTeensLike(criteria);
        assertEquals(2, teens.size());
    }

    @Test(expected = RetrofitError.class)
    // < 401 UnAuthorized; only teens have access to #getFollowers()
    public void G1_error_if_getting_followers_logged_as_follower() throws IOException {
        logInUser3Follower();
        svc.getFollowers();
    }

    @Test
    public void H_testing_adding_followers() throws IOException {
        logOut();
        logInUser3Follower();
        assertEquals(HttpStatus.OK.value(), svc.requestForFollowingTeen(TestUtils.user0UserName).getStatus());
        assertEquals(TestUtils.user0UserName, svc.getPendingFollowedTeens().get(0).getUserName());
        logOut();
        logInUser0Teen();
        assertEquals(TestUtils.user4UserName, svc.getPendingFollowers().get(0).getUserName());
        assertEquals(HttpStatus.OK.value(), svc.confirmFollower(TestUtils.user4UserName).getStatus());
        assertEquals(TestUtils.user4UserName, svc.getFollowers().get(0).getUserName());
        assertTrue(svc.getPendingFollowers().isEmpty());
        logOut();
        logInUser3Follower();
        assertTrue(svc.getPendingFollowedTeens().isEmpty());
        assertEquals(TestUtils.user0UserName, svc.getFollowedTeens().get(0).getUserName());

        // adding now user1 to follow user0 and user0 to follow user1
        logInUser1TeenFollower();
        assertEquals(HttpStatus.OK.value(), svc.requestForFollowingTeen(TestUtils.user0UserName).getStatus());
        // assertEquals(TestUtils.user0UserName,
        // svc.getPendingFollowedTeens().get(0).getUserName());

        logInUser0Teen();
        assertEquals(TestUtils.user1UserName, svc.getPendingFollowers().get(0).getUserName());
        assertEquals(HttpStatus.OK.value(), svc.confirmFollower(TestUtils.user1UserName).getStatus());
        // user0 should have now user3 and user1 as followers
        List<User> user0Followers = svc.getFollowers();
        assertEquals(2, user0Followers.size());
        assertEquals(TestUtils.user4UserName, user0Followers.get(0).getUserName());
        assertEquals(TestUtils.user1UserName, user0Followers.get(1).getUserName());

        // request user1 to follow
        assertEquals(HttpStatus.OK.value(), svc.requestForFollowingTeen(TestUtils.user1UserName).getStatus());

        logInUser1TeenFollower();
        assertEquals(TestUtils.user0UserName, svc.getPendingFollowers().get(0).getUserName());
        assertEquals(HttpStatus.OK.value(), svc.confirmFollower(TestUtils.user0UserName).getStatus());
        // user1 should have user1 as follower
        assertEquals(TestUtils.user0UserName, svc.getFollowers().get(0).getUserName());

    }

    @Test
    public void I_test_reminder_checkin_from_a_follower() throws IOException {
        // log in a follower
        logInUser3Follower();
        assertEquals(HttpStatus.OK.value(), svc.requestCheckIn(TestUtils.user0UserName).getStatus());
        // log in a teen
        logInUser0Teen();
        CheckInRequest fbr = svc.getCheckInRequests();
        assertEquals(1, fbr.getCount());
        // should be [user3firstname user3lastname]
        assertEquals(TestUtils.user3FirstName + " " + TestUtils.user3LastName, fbr.getFrom().get(0));
        assertEquals(HttpStatus.OK.value(), svc.acknowledgeCheckInRequests().getStatus());
        assertTrue(svc.getCheckInRequests().getCount() == 0);
    }

    @Test
    public void K_completing_a_checkIn() throws IOException {

        logInUser0Teen();

        List<Question> questions = svc.getAllQuestions(new Date(0).getTime());
        assertTrue(!questions.isEmpty());

        List<String> followers =  allFollowersUserName(svc.getFollowers());

        CheckIn checkIn = new CheckIn();
        Date checkIncreationDate = new Date();
        checkIn.setCreationDate(checkIncreationDate);
        checkIn.setSharedWith(followers);
        checkIn.setAuthor(TestUtils.user0UserName);
        List<Feedback> feedbacks = new ArrayList<Feedback>();

        Question q1 = questions.get(0);
        assertEquals(TestUtils.question1Body, q1.getBody());
        feedbacks.add(createFeedback(q1, Answer.YES, followers.get(0)));

        Question q2 = questions.get(1);
        assertEquals(TestUtils.question2Body, q2.getBody());
        feedbacks.add(createFeedback(q2, Answer.createPositiveAnswer("12PM"), followers.get(0), followers.get(1)));

        Question q3 = questions.get(2);
        assertEquals(TestUtils.question3Body, q3.getBody());
        feedbacks.add(createFeedback(q3, Answer.createNegativeAnswer("Didn't eat"), followers.get(0)));

        checkIn.setFeedback(feedbacks);
        // send to checkins
        long checkInId = svc.postCheckIn(checkIn);
        checkIn.setCreationDate(new Date());
        svc.postCheckIn(checkIn);

        logInUser3Follower();
        long beginingOfTime = new Date(0).getTime();
        List<CheckIn> checkIns = svc.getTeenCheckInHistroy(beginingOfTime, TestUtils.user0UserName);
        // user0 has shared 3 feedback-questions with user3
        assertEquals(3, checkIns.get(0).getFeedback().size());

        logInUser1TeenFollower();
        // user0 has shared 1 feedback-questions with user1
        checkIns = svc.getTeenCheckInHistroy(beginingOfTime, TestUtils.user0UserName);
        // get all checkins newer than 1 jan 1970
        assertEquals(2, checkIns.size());
        assertEquals(1, checkIns.get(0).getFeedback().size());

        checkIns = svc.getTeenCheckInHistroy(checkIncreationDate.getTime(), TestUtils.user0UserName);
        // get all checkins newer than in creation date
        assertEquals(1, checkIns.size());

        logInUser0Teen();
        checkIns = svc.getTeenCheckInHistroy(beginingOfTime, TestUtils.user0UserName);
        // getting "my" checkins
        assertEquals(2, checkIns.size());
        CheckIn ck1 = checkIns.get(0);
        System.out.println(ck1);
    }
    
    @Test
    public void I_testing_gift_photos() throws IOException{
        logInUser3Follower();
        //svc.postAvatarData(TestUtils.user3UserName, TestUtils.avatarTypedFile);
        svc.postGiftPhotoData(TestUtils.user0UserName, TestUtils.giftTypedFile);
        logInUser0Teen();
        List<Gift> gifts = svc.getAllGifts();
        System.out.println(gifts);
        Response response =svc.getGiftPhotoData(gifts.get(0).getPhotoFileName());
        InputStream giftData = response.getBody().in();
        byte[] originalFile = IOUtils.toByteArray(new FileInputStream(TestUtils.giftImg));
        byte[] retrievedFile = IOUtils.toByteArray(giftData);
        assertTrue(Arrays.equals(originalFile, retrievedFile));
    }

    private List<String> allFollowersUserName(List<User> followers) {
        List<String> list = new ArrayList<String>();
        for (User f : followers) {
            list.add(f.getUserName());
        }
        return list;
    }

    public Feedback createFeedback(Question question, Answer answer, String... sharedWith) {
        Feedback feedback = new Feedback(answer);
        feedback.setQuestionId(question.getId());
        if (sharedWith != null)
            feedback.setSharedWith(Arrays.asList(sharedWith));
        return feedback;
    }

    private void logOut() {
        //svc = restAdapter.setAccessToken(null).build().create(GotItURLService.class);
        conn.logOut();
    }

    /**
     * This one is a TEEN (&Follower)
     */
    private void logInUser0Teen() throws IOException {
        /*String accessToken = requestToken(client, TestUtils.user0UserName, TestUtils.password);
        assertTrue(accessToken != null);
        svc = restAdapter.setAccessToken(accessToken).build().create(GotItURLService.class);*/
       
        conn.logIn(TestUtils.user0UserName, TestUtils.password);
        user0 = svc.getUserByName(TestUtils.user0UserName);
    }

    /**
     * This one is a FOLLOWER
     */
    private void logInUser3Follower() throws IOException {
        /*String accessToken = requestToken(client, TestUtils.user3UserName, TestUtils.password);
        assertTrue(accessToken != null);
        svc = restAdapter.setAccessToken(accessToken).build().create(GotItURLService.class);*/
        conn.logIn(TestUtils.user4UserName, TestUtils.password);
    }

    /**
     * This one is a FOLLOWER
     */
    private void logInUser1TeenFollower() throws IOException {
       /* String accessToken = requestToken(client, TestUtils.user1UserName, TestUtils.password);
        assertTrue(accessToken != null);
        svc = restAdapter.setAccessToken(accessToken).build().create(GotItURLService.class);*/
        conn.logIn(TestUtils.user1UserName, TestUtils.password);
    }

    private void assertAvatarsAreEquals(GotItURLService svc, User user0, File avatarImg) throws IOException,
            FileNotFoundException {
        Response response = svc.getAvatarData(user0.getUserName());
        assertEquals(200, response.getStatus());
        InputStream avatarData = response.getBody().in();
        byte[] originalFile = IOUtils.toByteArray(new FileInputStream(avatarImg));
        byte[] retrievedFile = IOUtils.toByteArray(avatarData);
        assertTrue(Arrays.equals(originalFile, retrievedFile));
    }

   /* private String requestToken(Client client, String userName, String password) throws IOException {
        String accessToken = null;
        FormUrlEncodedTypedOutput to = new FormUrlEncodedTypedOutput();
        to.addField("username", userName);
        to.addField("password", password);
        to.addField("client_id", CLIENT_ID);
        to.addField("client_secret", "");
        to.addField("grant_type", "password");
        String base64Auth = BaseEncoding.base64().encode(new String(CLIENT_ID + ":" + "").getBytes());
        List<Header> headers = new ArrayList<Header>();
        headers.add(new Header("Authorization", "Basic " + base64Auth));
        Request req = new Request("POST", TEST_URL + TOKEN_PATH, headers, to);
        Response resp = client.execute(req);
        if (resp.getStatus() < 200 || resp.getStatus() > 299) {
            throw new SecuredRestException("Login failure: " + resp.getStatus() + " - " + resp.getReason());
        } else {
            String body = IOUtils.toString(resp.getBody().in());
            accessToken = new Gson().fromJson(body, JsonObject.class).get("access_token").getAsString();
        }
        return accessToken;
    }*/

}
