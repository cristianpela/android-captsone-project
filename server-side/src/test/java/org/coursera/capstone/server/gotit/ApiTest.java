/**
 * 
 */
package org.coursera.capstone.server.gotit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.coursera.capstone.server.gotit.integration.TestUtils;
import org.coursera.capstone.server.gotit.repository.IntervalFeedbackBuilder;
import org.coursera.capstone.server.gotit.repository.IntervalFeedbackBuilder.Enclosing;
import org.coursera.capstone.server.gotit.util.Resource;
import org.coursera.capstone.server.gotit.util.Resource.FileAccessStrategy;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApiTest {

    public ApiTest() {

    }

    @Test(expected = IntervalFeedbackBuilder.IntervalFeedbackException.class)
    public void test_feedback_interval_when_only1bound() {
        new IntervalFeedbackBuilder().lowerBound(1, Enclosing.EXCLUDED).build();
    }

    @Test(expected = IntervalFeedbackBuilder.IntervalFeedbackException.class)
    public void test_feedback_interval_when_2samebounds() {
        new IntervalFeedbackBuilder().lowerBound(1, Enclosing.EXCLUDED).lowerBound(1, Enclosing.INCLUDED).build();
    }

    @Test(expected = IntervalFeedbackBuilder.IntervalFeedbackException.class)
    public void test_feedback_interval_when_2manybounds() {
        new IntervalFeedbackBuilder().lowerBound(1, Enclosing.EXCLUDED).upperBound(1, Enclosing.INCLUDED)
                .lowerBound(10, Enclosing.EXCLUDED).build();
    }

    @Test(expected = IntervalFeedbackBuilder.IntervalFeedbackException.class)
    public void test_feedback_interval_when_badbounds() {
        new IntervalFeedbackBuilder().lowerBound(10, Enclosing.EXCLUDED).upperBound(1, Enclosing.INCLUDED).build();
    }

    @Test
    public void testInterval() {
        String expected = "(0-100]";
        IntervalFeedbackBuilder interval1 = new IntervalFeedbackBuilder().upperBound(100, Enclosing.INCLUDED)
                .lowerBound(0, Enclosing.EXCLUDED);
        assertEquals(expected, interval1.build());

        expected = "(0-100];[200-300]";
        IntervalFeedbackBuilder interval2 = new IntervalFeedbackBuilder().lowerBound(200, Enclosing.INCLUDED)
                .upperBound(300, Enclosing.INCLUDED);
        String actual = IntervalFeedbackBuilder.concat(new IntervalFeedbackBuilder[] { interval1, interval2 });
        assertEquals(expected, actual);
    }

   /* @Test 
    public void testResource() throws IOException{
        Resource resource = new Resource(Long.toString(1), new FileAccessStrategy(TestUtils.user0UserName, "avatar"));
        resource.save(Files.newInputStream(Paths.get(TestUtils.avatarImg.getAbsolutePath())));
        byte[] readed = resource.load();
        assertNotNull(readed);
        
        resource = Resource.createGiftAccessResource(TestUtils.user0UserName, 1);
        resource.save(Files.newInputStream(Paths.get(TestUtils.avatarImg.getAbsolutePath())));
        readed = resource.load();
        assertNotNull(readed);
        
    }*/
    
    @Test
    public void testEnum(){
        assertEquals(V.V1, V.valueOf("V1"));
    }
    
    
    
    private static enum V{
        V1, V2;
    }
}
