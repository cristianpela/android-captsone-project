package org.coursera.capstone.server.gotit.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.coursera.capstone.server.gotit.api.GotItURLService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.BaseEncoding;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.Client;
import retrofit.client.Header;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.converter.JacksonConverter;
import retrofit.mime.FormUrlEncodedTypedOutput;

public class GotItConnection {

    private static final String CLIENT_ID = "mobile";
    private static final String TEST_URL = "https://localhost:8443";
    private static final String TOKEN_PATH = "/oauth/token";

    private Client client;

    private String accessToken;
    private String userName;
    private String password;

    private RestAdapter.Builder restBuilder;
    private GotItURLService restInterface;

    public GotItConnection(Client client) {
        this.client = client;
        OAuthHandler oauthHandler = new OAuthHandler();
        restBuilder = new RestAdapter.Builder().setClient(client).setErrorHandler(oauthHandler)
                .setRequestInterceptor(oauthHandler).setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
                .setConverter(new JacksonConverter(new ObjectMapper()));
        restInterface = restBuilder.build().create(GotItURLService.class);
    }

    public void logIn(String userName, String password) throws IOException {
        aquireToken(userName, password);
    }

    public void logOut() {
        accessToken = null;
    }

    private void aquireToken(String userName, String password) throws IOException {
        FormUrlEncodedTypedOutput to = new FormUrlEncodedTypedOutput();
        to.addField("username", userName);
        to.addField("password", password);
        to.addField("client_id", CLIENT_ID);
        to.addField("client_secret", "");
        to.addField("grant_type", "password");
        String base64Auth = BaseEncoding.base64().encode(new String(CLIENT_ID + ":" + "").getBytes());
        List<Header> headers = new ArrayList<Header>();
        headers.add(new Header("Authorization", "Basic " + base64Auth));
        Request req = new Request("POST", TEST_URL + TOKEN_PATH, headers, to);
        Response resp = client.execute(req);
        if (resp.getStatus() < 200 || resp.getStatus() > 299) {
            throw new SecuredRestException("Login failure: " + resp.getStatus() + " - " + resp.getReason());
        } else {
            String body = IOUtils.toString(resp.getBody().in());
            accessToken = extractField(body, "access_token");
        }
    }

    private String extractField(String body, String field) {
        JsonObject jsonObject = new Gson().fromJson(body, JsonObject.class);
        return (jsonObject != null) ? jsonObject.get(field).getAsString() : null;
    }

    private class OAuthHandler implements RequestInterceptor, ErrorHandler {
        
        @Override
        public void intercept(RequestFacade request) {
            if (accessToken != null)
                request.addHeader("Authorization", "Bearer " + accessToken);
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            Response resp = cause.getResponse();
            if (resp.getStatus() == 401) {
                try {
                    String body = IOUtils.toString(resp.getBody().in());
                    String errorDescription = extractField(body, "error_description");
                    if (errorDescription != null && errorDescription.equals("The access token expired")) {
                        aquireToken(userName, password);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return cause;
        }
    }

    public GotItURLService getRESTInterface() {
        return restInterface;
    }

}
