package org.coursera.capstone.server.gotit;

import static org.coursera.capstone.server.gotit.integration.TestUtils.saveAll;
import static org.coursera.capstone.server.gotit.integration.TestUtils.setUpRoles;
import static org.coursera.capstone.server.gotit.integration.TestUtils.setUpUsers;
import static org.coursera.capstone.server.gotit.integration.TestUtils.user0UserName;
import static org.coursera.capstone.server.gotit.integration.TestUtils.user1UserName;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.FeedbackType;
import org.coursera.capstone.server.gotit.api.QuestionEnterType;
import org.coursera.capstone.server.gotit.api.QuestionGeneratedFeedbackMode;
import org.coursera.capstone.server.gotit.integration.TestUtils;
import org.coursera.capstone.server.gotit.repository.*;
import org.coursera.capstone.server.gotit.repository.IntervalFeedbackBuilder.Enclosing;
import org.coursera.capstone.server.gotit.service.AccountService;
import org.coursera.capstone.server.gotit.service.QuestionService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@DirtiesContext
@Transactional
@TransactionConfiguration(defaultRollback = true)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class })
public class SpringDataTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private CheckInRepository feedbackRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;

    // @Autowired
    // private FeedbackRequestRepository feedbackRequestRepository;

    @Autowired
    private AccountService signUpService;
    @Autowired
    private QuestionService questionService;

    private String question1Body = UUID.randomUUID().toString();
    private String question2Body = UUID.randomUUID().toString();
    private String question3Body = UUID.randomUUID().toString();

    @Before
    public void setUp() {

        QuestionEntity question1 = new QuestionEntity();
        question1.setBody(question1Body);
        question1.setEnterable(true);
        question1.setEnterType(QuestionEnterType.INT);
        question1.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.AUTO);
        String negativeInterval = IntervalFeedbackBuilder.concat(new IntervalFeedbackBuilder[] {
                new IntervalFeedbackBuilder().lowerBound(Integer.MIN_VALUE, Enclosing.INCLUDED).upperBound(90,
                        Enclosing.EXCLUDED),
                new IntervalFeedbackBuilder().lowerBound(150, Enclosing.EXCLUDED).upperBound(Integer.MAX_VALUE,
                        Enclosing.INCLUDED)

        });
        String positiveInterval = new IntervalFeedbackBuilder().lowerBound(90, Enclosing.INCLUDED)
                .upperBound(150, Enclosing.INCLUDED).build();
        question1.setNegativeFeedbackInterval(negativeInterval);
        question1.setPositiveFeedbackInterval(positiveInterval);

        QuestionEntity question2 = new QuestionEntity();
        question2.setBody(question2Body);
        question2.setEnterType(QuestionEnterType.STRING);
        question2.setEnterable(true);
        question2.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.MANUAL);

        QuestionEntity question3 = new QuestionEntity();
        question3.setBody(question3Body);
        question3.setEnterable(false);
        question3.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.MANUAL);

        saveAll(questionRepository, new QuestionEntity[] { question1, question2, question3 });

        setUpRoles(roleRepository);
        setUpUsers(userRepository, userDetailsRepository);
    }

    @After
    public void tearUp() {
        /*
         * userDetailsRepository.deleteAll(); userRepository.deleteAll();
         * roleRepository.deleteAll(); questionRepository.deleteAll();
         */
    }

    // @Test
    public void testingFeedbackCreation() {
        UserEntity user0 = userRepository.findOne(1L);
        CheckInEntity feedback = new CheckInEntity(user0, true);
        feedbackRepository.save(feedback);

        feedback = feedbackRepository.findOne(0L);
        assertNotNull(feedback);
        user0 = feedback.getAuthor();
        assertNotNull(user0);
        assertEquals(user0UserName, user0.getUserName());
        assertTrue(true);

        FeedbackEntity feedbackbr = new FeedbackEntity();
        feedbackbr.setQuestion(questionRepository.findOne(0L));
        feedbackbr.setFeedbackType(FeedbackType.POSITIVE);
        feedbackbr.setAnswer("101");
        feedbackbr.setShareable(true);
        feedback.getFeedback().add(feedbackbr);
        feedback.setShareable(true);
        feedbackRepository.save(feedback);

        feedback = feedbackRepository.findOne(0L);
        assertNotNull(feedback);
        List<FeedbackEntity> breakdowns = feedback.getFeedback();
        assertTrue(!breakdowns.isEmpty());
        feedbackbr = breakdowns.get(0);
        QuestionEntity question = feedbackbr.getQuestion();
        assertNotNull(question);
        assertEquals(question1Body, question.getBody());
    }

    // @Test
    public void testingFolowers() {
        UserEntity user0 = userRepository.findOne(1L);
        UserEntity user1 = userRepository.findOne(2L);
        UserEntity user2 = userRepository.findOne(3L);
        // UserDetailsEntity user3 = userDetailsRepository.findOne(4L);

        user0._getFollowers().add(user1);
        user1.getFollowedTeens().add(user0);

        user0._getFollowers().add(user2);
        user2.getFollowedTeens().add(user0);

        user1._getFollowers().add(user2);
        user2.getFollowedTeens().add(user1);

        saveAll(userDetailsRepository, new UserEntity[] { user0, user1, user2 });

        user2 = userRepository.findOne(3L);
        List<UserEntity> user2followedTeens = user2.getFollowedTeens();
        assertNotNull(user2followedTeens);
        assertEquals(2, user2followedTeens.size());
        assertEquals(user0UserName, user2followedTeens.get(0).getUserName());
        assertEquals(user1UserName, user2followedTeens.get(1).getUserName());
        assertEquals(2, user2followedTeens.get(0)._getFollowers().size());
        assertEquals(user1UserName, user2followedTeens.get(0)._getFollowers().get(0).getUserName());

        // checking feeedback requests
        user0.getCheckInRequests().add(user1);
        userRepository.save(user0);
        user0 = userRepository.findOne(1L);
        assertEquals(1, user0.getCheckInRequests().size());
        assertEquals(user1.getUserName(), user0.getCheckInRequests().get(0).getUserName());
    }

    // @Test
    public void test_teens_by_criteria() {
        Set<UserEntity> joinAll = new HashSet<UserEntity>();

        List<UserEntity> allLike = userRepository.findAllTeensLike("user", TestUtils.user0UserName);
        joinAll.addAll(allLike);

        List<UserDetailsEntity> allLikeD = userDetailsRepository.findAllTeensLike("user");
        for (UserDetailsEntity ude : allLikeD) {
            UserEntity userEntity = ude.getUser();
            if (userEntity.isTeen() && !userEntity.getUserName().equals(TestUtils.user0UserName))
                joinAll.add(userEntity);
        }

        for (UserEntity ue : joinAll)
            System.out.println(ue);
    }

}
