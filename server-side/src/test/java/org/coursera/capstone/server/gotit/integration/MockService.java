package org.coursera.capstone.server.gotit.integration;

import retrofit.http.GET;

public interface MockService {
	
	@GET("/check")
	public long check();

}
