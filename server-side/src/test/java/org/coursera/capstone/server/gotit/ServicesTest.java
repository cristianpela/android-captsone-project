package org.coursera.capstone.server.gotit;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.api.UserDetails;
import org.coursera.capstone.server.gotit.repository.RoleEntity;
import org.coursera.capstone.server.gotit.repository.RoleRepository;
import org.coursera.capstone.server.gotit.repository.RoleType;
import org.coursera.capstone.server.gotit.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@DirtiesContext
@Transactional
@TransactionConfiguration(defaultRollback = true)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class })
public class ServicesTest {
    
    @Autowired
    private AccountService signUpService;
    
    @Autowired
    private RoleRepository roleRepository;

    public ServicesTest() {
    }
    
    @Test
    public void test_sign_up() throws IllegalAccessException, InvocationTargetException{
        setUpRoles();
        
        String firstName = rand();
        String lastName = rand();
        Date birth = new Date();
        long rmd = 1234678;
        boolean isTeen = true;
        UserDetails userDetails = new UserDetails(firstName, lastName, birth, rmd);
        
        String userName = rand();
        String password = rand();
        User user = new User(isTeen, userName, password, userDetails);
        
        User signedUser = signUpService.signUpOrUpdate(user);
        System.out.println(signedUser);
        assertTrue(signedUser.getUserName().equals(user.getUserName()));
    }

    private void setUpRoles(){
        List<RoleEntity> roles = new ArrayList<RoleEntity>();
        roles.add(new RoleEntity(RoleType.ADMIN));
        roles.add(new RoleEntity(RoleType.FOLLOWER));
        roleRepository.save(roles);
    }
    
    private String rand(){
        return UUID.randomUUID().toString();
    }
}
