package org.coursera.capstone.server.gotit.integration;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.coursera.capstone.server.gotit.api.QuestionEnterType;
import org.coursera.capstone.server.gotit.api.QuestionGeneratedFeedbackMode;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.api.UserDetails;
import org.coursera.capstone.server.gotit.repository.BaseEntity;
import org.coursera.capstone.server.gotit.repository.IntervalFeedbackBuilder;
import org.coursera.capstone.server.gotit.repository.QuestionEntity;
import org.coursera.capstone.server.gotit.repository.QuestionRepository;
import org.coursera.capstone.server.gotit.repository.RoleEntity;
import org.coursera.capstone.server.gotit.repository.RoleRepository;
import org.coursera.capstone.server.gotit.repository.RoleType;
import org.coursera.capstone.server.gotit.repository.UserDetailsEntity;
import org.coursera.capstone.server.gotit.repository.UserDetailsRepository;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.coursera.capstone.server.gotit.repository.IntervalFeedbackBuilder.Enclosing;
import org.springframework.data.repository.CrudRepository;

import retrofit.mime.TypedFile;

public class TestUtils {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
    
    public static final String question9Body = "Were any of these things happening at the time you checked/should have checked your blood sugar: Rushing; feeling tired of diabetes; feeling sick; on the road; really hungry; wanting privacy; busy and didn�t want to stop; without supplies; feeling low; feeling high; having a lot of fun; tired";
    public static final String question8Body = "How was your energy level when you checked/should have checked your blood sugar?";
    public static final String question7Body = "How was your mood when you checked/should have checked your blood sugar?";
    public static final String question6Body = "Where were you when you checked/should have checked your blood sugar?";
    public static final String question5Body = "Who were you with when you checked/should have checked your blood sugar?";
    public static final String question4Body = "Did you administer insulin?";
    public static final String question3Body = "What did you eat at [meal time]?";
    public static final String question2Body = "What time did you check your blood sugar level at [meal time]?";
    public static final String question1Body = "What was your blood sugar level at [meal time/bedtime]";
    
    public static File avatarImg = new File("src/test/resources/avatar.png");
    // TODO: Find a way to not hardcode the mimetype
    public static TypedFile avatarTypedFile = new TypedFile("image/png", avatarImg);
    public static File avatarImg2 = new File("src/test/resources/avatar2.png");
    // TODO: Find a way to not hardcode the mimetype
    public static TypedFile avatarTypedFile2 = new TypedFile("image/png", avatarImg2);
    
    public static File giftImg = new File(("src/test/resources/gift.jpg"));
    public static TypedFile giftTypedFile = new TypedFile("image/png", giftImg);

    public static int teenCount = 3;

    public static String adminUserName = "admin";
    public static String adminFirstName = "admin";
    public static String adminLastName = "admin";
    public static boolean adminIsTeen = false;

    public static String user0UserName = "user0";
    public static String user0FirstName = "Amber";
    public static String user0LastName = "Gordon";
    public static boolean user0IsTeen = true;

    public static String user1UserName = "uzzr1";
    public static String user1FirstName = "Bobby";
    public static String user1LastName = "Powell";
    public static boolean user1IsTeen = true;

    public static String user2UserName = "user2";
    public static String user2FirstName = "Keith";
    public static String user2LastName = "Perkins";
    public static boolean user2IsTeen = true;

    public static String user3UserName = "user3";
    public static String user3FirstName = "Nancy";
    public static String user3LastName = "Francy";
    public static boolean user3IsTeen = false;
    
    public static String user4UserName = "uzzr4";
    public static String user4FirstName = "Douglas";
    public static String user4LastName = "White";
    public static boolean user4IsTeen = false;
    
    public static String user5UserName = "user5";
    public static String user5FirstName = "Ella";
    public static String user5LastName = "Dean";
    public static boolean user5IsTeen = true;

    public static String password = "pass";

    public static RoleEntity roleAdmin = new RoleEntity(RoleType.ADMIN);
    public static RoleEntity roleFollower = new RoleEntity(RoleType.FOLLOWER);
    public static RoleEntity roleTeen = new RoleEntity(RoleType.TEEN);
    public static RoleEntity[] roleBatch = new RoleEntity[] { roleAdmin, roleFollower };

    public static UserEntity admin;
    public static UserEntity user0;
    public static UserEntity user1;
    public static UserEntity user2;
    public static UserEntity user3;
    public static UserEntity user4;
    public static UserEntity user5;

    private static String randomString() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    private TestUtils() {
    }

    public static void setUpRoles(RoleRepository roleRepository) {
        saveAll(roleRepository, roleBatch);
    }
    
    private static Date date(String frmt){
        try {
            return dateFormat.parse(frmt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static void setUpUsers(UserRepository userRepository, UserDetailsRepository userDetailsRepository) {
        UserDetailsEntity adminDetails = new UserDetailsEntity(adminFirstName, adminLastName, new Date() , randomMRN());
        admin = new UserEntity(adminIsTeen, adminUserName, password, adminDetails, roleBatch);
        adminDetails.setUser(admin);

        UserDetailsEntity user0Details = new UserDetailsEntity(user0FirstName, user0LastName, date("10.03.2000"), randomMRN());
        user0 = new UserEntity(user0IsTeen, user0UserName, password, user0Details);
        user0.getRoles().add(roleFollower);
        if (user0.isTeen())
            user0.getRoles().add(roleTeen);
        user0Details.setUser(user0);

        UserDetailsEntity user1Details = new UserDetailsEntity(user1FirstName, user1LastName, date("09.22.1998"), randomMRN());
        user1 = new UserEntity(user1IsTeen, user1UserName, password, user1Details);
        user1.getRoles().add(roleFollower);
        if (user1.isTeen())
            user1.getRoles().add(roleTeen);
        user1Details.setUser(user1);

        UserDetailsEntity user2Details = new UserDetailsEntity(user2FirstName, user2LastName, date("07.02.1997"), randomMRN());
        user2 = new UserEntity(user2IsTeen, user2UserName, password, user2Details);
        user2.getRoles().add(roleFollower);
        if (user2.isTeen())
            user2.getRoles().add(roleTeen);
        user2Details.setUser(user2);

        UserDetailsEntity user3Details = new UserDetailsEntity(user3FirstName, user3LastName,  date("01.12.1999"), randomMRN());
        user3 = new UserEntity(user3IsTeen, user4UserName, password, user3Details);
        user3.getRoles().add(roleFollower);
        if (user3.isTeen())
            user3.getRoles().add(roleTeen);
        user3Details.setUser(user3);
         
        
        UserDetailsEntity user5Details = new UserDetailsEntity(user5FirstName, user5LastName,  date("04.25.2001"), randomMRN());
        user5 = new UserEntity(user5IsTeen, user5UserName, password, user5Details);
        user5.getRoles().add(roleFollower);
        if (user5.isTeen())
            user5.getRoles().add(roleTeen);
        user5Details.setUser(user5);
        
        user0._getFollowers().add(user1);
        user1.getFollowedTeens().add(user0);
        
        user0._getFollowers().add(user3);
        user3.getFollowedTeens().add(user0);
        
        user1._getFollowers().add(user0);
        user0.getFollowedTeens().add(user1);

        saveAll(userRepository, new UserEntity[] { admin, user0, user1, user2, user3, user5});
        saveAll(userDetailsRepository, new UserDetailsEntity[] { adminDetails, user0Details, user1Details,
                user2Details, user3Details, user5Details });
        
       
    }

    public static List<QuestionEntity> setUpQuestions(QuestionRepository questionRepository) {

        List<QuestionEntity> questions = new ArrayList<QuestionEntity>();

        QuestionEntity question1 = new QuestionEntity();
        question1.setBody(question1Body);
        question1.setEnterable(true);
        question1.setEnterType(QuestionEnterType.INT);
        question1.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.AUTO);
        String negativeInterval = IntervalFeedbackBuilder.concat(new IntervalFeedbackBuilder[] {
                new IntervalFeedbackBuilder().lowerBound(Integer.MIN_VALUE, Enclosing.INCLUDED).upperBound(90,
                        Enclosing.EXCLUDED),
                new IntervalFeedbackBuilder().lowerBound(150, Enclosing.EXCLUDED).upperBound(Integer.MAX_VALUE,
                        Enclosing.INCLUDED)

        });
        String positiveInterval = new IntervalFeedbackBuilder().lowerBound(90, Enclosing.INCLUDED)
                .upperBound(150, Enclosing.INCLUDED).build();
        question1.setNegativeFeedbackInterval(negativeInterval);
        question1.setPositiveFeedbackInterval(positiveInterval);
        questions.add(question1);

        QuestionEntity question2 = new QuestionEntity();
        question2.setBody(question2Body);
        question2.setEnterable(true);
        question2.setEnterType(QuestionEnterType.DOUBLE);
        question2.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.MANUAL);
        questions.add(question2);

        QuestionEntity question3 = generateGenericQuestion(question3Body);
        questions.add(question3);

        QuestionEntity question4 = new QuestionEntity();
        question4.setBody(question4Body);
        question4.setEnterable(false);
        question4.setEnterType(QuestionEnterType.NONE);
        question4.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.MANUAL);
        questions.add(question4);

        QuestionEntity question5 = generateGenericQuestion(question5Body);
        questions.add(question5);

        QuestionEntity question6 = generateGenericQuestion(question6Body);
        questions.add(question6);

        QuestionEntity question7 = generateGenericQuestion(question7Body);
        questions.add(question7);

        QuestionEntity question8 = generateGenericQuestion(question8Body);
        questions.add(question8);

        QuestionEntity question9 = generateGenericQuestion(question9Body);
        questions.add(question9);

        questionRepository.save(questions);

        return questions;

    }

    private static QuestionEntity generateGenericQuestion(String body) {
        QuestionEntity question = new QuestionEntity();
        question.setBody(body);
        question.setEnterable(true);
        question.setEnterType(QuestionEnterType.STRING);
        question.setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode.MANUAL);
        return question;
    }

    @SuppressWarnings({ "unchecked" })
    public static void saveAll(@SuppressWarnings("rawtypes") CrudRepository repo, BaseEntity... entities) {
        repo.save(Arrays.asList(entities));
    }

    public static User createRandomRESTUser() {
        UserDetails userDetails = new UserDetails(randomString(), randomString(), new Date(), randomMRN());
        User user = new User(randomTeen(), randomString(), randomString(), userDetails);
        return user;
    }

    private static boolean randomTeen() {
        boolean isTeen = Math.random() < 0.5;
        if (isTeen)
            teenCount++;
        return isTeen;
    }

    private static long randomMRN() {
        return ThreadLocalRandom.current().nextInt(100000, 1000000);
    }
}
