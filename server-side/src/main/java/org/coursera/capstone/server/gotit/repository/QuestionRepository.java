package org.coursera.capstone.server.gotit.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<QuestionEntity, Long> {
    
    public List<QuestionEntity> findByModificationDateAfter(Date afterDate);

}
