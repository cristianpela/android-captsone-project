package org.coursera.capstone.server.gotit.api;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@JsonFormat(shape= JsonFormat.Shape.STRING)
public enum QuestionEnterType {
    STRING, INT, DOUBLE, NONE;
    
    public static class Deserializer extends JsonDeserializer<QuestionEnterType>{
        @Override
        public QuestionEnterType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException,
                JsonProcessingException {
            return QuestionEnterType.valueOf(p.getValueAsString());
        } 
    }
}
