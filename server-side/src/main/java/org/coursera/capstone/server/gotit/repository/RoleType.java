package org.coursera.capstone.server.gotit.repository;

public enum RoleType {

    ADMIN, FOLLOWER, TEEN;

    public static final String hasRoleExpression(RoleType roleType) {
        return "hasRole(" + roleType + ")";
    }
    
    public static final String hasTeenRoleExpression = "hasRole('TEEN')";
   
}
