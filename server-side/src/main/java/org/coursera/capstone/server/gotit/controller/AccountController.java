package org.coursera.capstone.server.gotit.controller;

import static org.coursera.capstone.server.gotit.api.GotItURLMapping.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;

import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    public AccountController() {
        super();
    }

    @RequestMapping(value = SIGN_UP_PATH, method = RequestMethod.POST)
    public User signUpOrUpdate(@RequestBody User user, Principal principal) throws IllegalAccessException,
            InvocationTargetException {
       if (isFreshAccount(user) && accountService.getUserByName(user.getUserName()) != null)
            throw new IllegalAccessException();
        checkAuthenticity(user.getUserName(), principal);
        return accountService.signUpOrUpdate(user);
    }

    @RequestMapping(value = AVATAR_PATH, method = RequestMethod.POST)
    public boolean postAvatarData(@PathVariable(USER_NAME_PARAMETER) String userName,
            @RequestParam(DATA_PARAMETER) MultipartFile file, Principal principal) throws IOException,
            IllegalAccessException, InvocationTargetException {
        checkAuthenticity(userName, principal);
        return accountService.saveAvatarData(userName, file.getInputStream());
    }

    @RequestMapping(value = AVATAR_PATH)
    public byte[] getAvatarData(@PathVariable(USER_NAME_PARAMETER) String userName, Principal principal)
            throws IllegalAccessException, InvocationTargetException, IOException {
        checkAuthenticity(userName, principal);
        return accountService.loadAvatarData(userName);
    }

    @RequestMapping(value = AVATAR_PATH, method = RequestMethod.DELETE)
    public void deleteAvatarData(@PathVariable(USER_NAME_PARAMETER) String userName, Principal principal)
            throws IllegalAccessException, InvocationTargetException, IOException {
        checkAuthenticity(userName, principal);
        accountService.deleteAvatarData(userName);
    }

    @RequestMapping(value = GET_CURRENT_USER_PATH_BY_NAME_PATH)
    public User getCurrentUserByName(@PathVariable(USER_NAME_PARAMETER) String userName, Principal principal)
            throws IllegalAccessException, InvocationTargetException {
       // checkAuthenticity(userName, principal);
        User user = accountService.getUserByName(userName);
        if(!principal.getName().equals(userName))
            user.setPassword(null);
        return user;
    }


    private void checkAuthenticity(String userName, Principal principal) throws IllegalAccessException {
        if (principal != null && !principal.getName().equals(userName))
            throw new IllegalAccessException();
    }

    // @TODO(gotit) find a better way to deal with fresh accounts ID = -1 flag. It
    // annoys jpa's foreign key constraints.;
    private boolean isFreshAccount(User user) {
        if (user.getId() == -1) {
            user.setId(0);
            return true;
        }
        return false;
    }
}
