package org.coursera.capstone.server.gotit.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.coursera.capstone.server.gotit.api.Question;
import org.coursera.capstone.server.gotit.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.coursera.capstone.server.gotit.api.GotItURLMapping.*;

@RestController
public class QuestionController {
    
    @Autowired
    private QuestionService questionService;

    public QuestionController() {
    }

    @RequestMapping(GET_ALL_QUESTIONS_PATH)
    public List<Question> getAllQuestions(@PathVariable(DATE_PARAMETER) long laterThanDate) throws IllegalAccessException, InvocationTargetException{
        return questionService.getAllQuestions(laterThanDate);
    }
}
