package org.coursera.capstone.server.gotit.api;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Notification {

    private NotificationType type;
    
    private Date date;
    
    private String fromUserName;
    
    private String fromFullName;
    
    public Notification() {
    }

    public NotificationType getType() {
        return type;
    }

    @JsonProperty("notificationType")
    @JsonDeserialize(using = NotificationType.Deserializer.class)
    public void setType(NotificationType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getFromFullName() {
        return fromFullName;
    }

    public void setFromFullName(String fromFullName) {
        this.fromFullName = fromFullName;
    }
    
    

}
