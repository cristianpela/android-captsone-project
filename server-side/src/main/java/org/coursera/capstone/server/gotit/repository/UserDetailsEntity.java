package org.coursera.capstone.server.gotit.repository;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USER_DETAILS")
@AttributeOverride(name = "id", column = @Column(name = "USER_DETAIL_ID"))
public class UserDetailsEntity extends BaseEntity {

	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@Column(name = "FIRST_NAME")
	private String firstName; 
	
	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "MRN")
	private long medicalRecordNumber;

	@OneToOne
	@JoinColumn(name = "USER_ID")
	private UserEntity user;

	public UserDetailsEntity() {

	}

	public UserDetailsEntity(String firstName, String lastName, Date birthDate,
			long medicalRecordNumber) {
		this.birthDate = birthDate;
		this.firstName = firstName;
		this.lastName = lastName;
		this.medicalRecordNumber = medicalRecordNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	
	public Date getBirthDate() {
		return birthDate;
	}

	public long getMedicalRecordNumber() {
		return medicalRecordNumber;
	}

	
	

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}
	
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMedicalRecordNumber(long medicalRecordNumber) {
		this.medicalRecordNumber = medicalRecordNumber;
	}

    @Override
    public String toString() {
        return "UserDetailsEntity [birthDate=" + birthDate + ", firstName=" + firstName + ", lastName=" + lastName
                + ", medicalRecordNumber=" + medicalRecordNumber + "]";
    }

}
