package org.coursera.capstone.server.gotit.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionCatchControllerAdvice {
    // TODO(gotit): custom runtime exceptions
    public ExceptionCatchControllerAdvice() {
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({ IllegalAccessException.class })
    public void handleUnauthorizedExceptions() {
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({ IOException.class })
    public void handleNotFoundResourceExceptions() {
    }

}
