package org.coursera.capstone.server.gotit.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
@AttributeOverride(name = "id", column = @Column(name = "USER_ID"))
public class UserEntity extends BaseEntity {

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "USER_ROLE", joinColumns = { @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID") }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID") })
    private Set<RoleEntity> roles = new HashSet<RoleEntity>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "FOLLOWER", joinColumns = { @JoinColumn(name = "TEEN_ID", referencedColumnName = "USER_ID") }, inverseJoinColumns = { @JoinColumn(name = "FOLLOWER_ID", referencedColumnName = "USER_ID") })
    private List<UserEntity> _followers = new ArrayList<UserEntity>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "_followers")
    private List<UserEntity> followedTeens = new ArrayList<UserEntity>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "PENDING_FOLLOWER", joinColumns = { @JoinColumn(name = "TEEN_ID", referencedColumnName = "USER_ID") }, inverseJoinColumns = { @JoinColumn(name = "FOLLOWER_ID", referencedColumnName = "USER_ID") })
    private List<UserEntity> pendingFollowers = new ArrayList<UserEntity>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "pendingFollowers")
    private List<UserEntity> pendingFollowedTeens = new ArrayList<UserEntity>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "CHECK_IN_REQUEST", joinColumns = { @JoinColumn(name = "TEEN_ID", referencedColumnName = "USER_ID") }, inverseJoinColumns = { @JoinColumn(name = "FOLLOWER_ID", referencedColumnName = "USER_ID") })
    private List<UserEntity> checkInRequests = new ArrayList<UserEntity>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_DETAIL_ID")
    private UserDetailsEntity userDetails;

    @Column(name = "USER_NAME", unique = true)
    private String userName;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "IS_TEEN")
    private boolean teen;

    public UserEntity() {
    }

    public UserEntity(boolean isTeen, String userName, String password, UserDetailsEntity userDetails,
            RoleEntity... roles) {
        this.teen = isTeen;
        this.userName = userName;
        this.password = password;
        this.userDetails = userDetails;
        this.roles.addAll(Arrays.asList(roles));
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setTeen(boolean teen) {
        this.teen = teen;
    }

    public boolean isTeen() {
        return teen;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public List<UserEntity> _getFollowers() {
        return _followers;
    }

    public void set_Followers(List<UserEntity> _followers) {
        this._followers = _followers;
    }

    public List<UserEntity> getFollowedTeens() {
        return followedTeens;
    }

    public void setFollowedTeens(List<UserEntity> followedTeens) {
        this.followedTeens = followedTeens;
    }

    public List<UserEntity> getPendingFollowers() {
        return pendingFollowers;
    }

    public void setPendingFollowers(List<UserEntity> pendingFollowers) {
        this.pendingFollowers = pendingFollowers;
    }

    public List<UserEntity> getPendingFollowedTeens() {
        return pendingFollowedTeens;
    }

    public void setPendingFollowedTeens(List<UserEntity> pendingFollowedTeens) {
        this.pendingFollowedTeens = pendingFollowedTeens;
    }

    public UserDetailsEntity getUserDetailsEntity() {
        return userDetails;
    }

    public void setUserDetailsEntity(UserDetailsEntity userDetails) {
        this.userDetails = userDetails;
    }

    public List<UserEntity> getCheckInRequests() {
        return checkInRequests;
    }

    public void setCheckInRequests(List<UserEntity> checkInRequests) {
        this.checkInRequests = checkInRequests;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserEntity other = (UserEntity) obj;
        if (userName == null) {
            if (other.userName != null)
                return false;
        } else if (!userName.equals(other.userName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder followers = new StringBuilder();
        followers.append("Followers:\n");
        for(UserEntity f : _followers){
            followers.append(f.getUserName());
        }
        followers.append(" %% ");
        followers.append("Following:\n");
        for(UserEntity f : followedTeens){
            followers.append(f.getUserName());
        }
        
        followers.append(" %% ");
        followers.append("Pending followers:\n");
        for(UserEntity f : pendingFollowers){
            followers.append(f.getUserName());
        }
        
        followers.append(" %% ");
        followers.append("Pending following:\n");
        for(UserEntity f : pendingFollowedTeens){
            followers.append(f.getUserName());
        }
        
        return super.toString() + "\nroles=" + roles + ", userName=" + userName + ", password=" + password
                + ", teen=" + teen + "] : \n\t\t" + followers.toString();
    }

}
