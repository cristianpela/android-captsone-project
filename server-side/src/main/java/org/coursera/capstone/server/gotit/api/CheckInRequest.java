package org.coursera.capstone.server.gotit.api;

import java.util.List;

public class CheckInRequest {

    private int count;
    
    private List<String> from;

    public CheckInRequest(int count, List<String> from) {
        this.count = count;
        this.from = from;
    }

    public CheckInRequest() {
        super();
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<String> getFrom() {
        return from;
    }

    public void setFrom(List<String> from) {
        this.from = from;
    }
   
}
