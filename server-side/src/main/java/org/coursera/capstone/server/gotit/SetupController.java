package org.coursera.capstone.server.gotit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.GotItURLMapping;
import org.coursera.capstone.server.gotit.integration.TestUtils;
import org.coursera.capstone.server.gotit.repository.CheckInRepository;
import org.coursera.capstone.server.gotit.repository.QuestionEntity;
import org.coursera.capstone.server.gotit.repository.QuestionRepository;
import org.coursera.capstone.server.gotit.repository.RoleRepository;
import org.coursera.capstone.server.gotit.repository.UserDetailsEntity;
import org.coursera.capstone.server.gotit.repository.UserDetailsRepository;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SetupController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private CheckInRepository checkInRepository;

    private DateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy HH:mm");

    public SetupController() {
    }

    @RequestMapping(GotItURLMapping.PUBLIC_ACCESS_PATH + "init")
    @Transactional
    public String setup() {
        TestUtils.setUpRoles(roleRepository);
        TestUtils.setUpUsers(userRepository, userDetailsRepository);
        outputList(TestUtils.setUpQuestions(questionRepository));
        return "<h1>Data has been initialized. Click <a href ='https://localhost:8443/public/all'>here</a> to see the data.</h1>";
    }

    @RequestMapping(GotItURLMapping.PUBLIC_ACCESS_PATH + "all")
    @Transactional
    public String showAllData() {
        StringBuilder o = new StringBuilder();
        o.append("<html><head>"+css()+"</head><body>");
        o.append("<center><h1>Got It - Database Initial Data</h1></center>");
        o.append(htmlUsers());
        o.append("<br>");
        o.append(htmlQuestions());
        o.append("</body></html>");
        return o.toString();
    }

    private String htmlQuestions() {
        StringBuilder o = new StringBuilder();
        o.append("<h2>Questions</h2>");
        o.append("<table>");
        o.append(tableHeader("ID", "Creation", "Body", "Enterable", "Type"));
        Iterable<QuestionEntity> questions = questionRepository.findAll();
        for (QuestionEntity q : questions) {
            o.append(tableRow("" + q.getId(), dateFormat.format(q.getModificationDate()), q.getBody(),
                    "" + q.isEnterable(), q.getEnterType().toString()));
        }
        o.append("</table>");
        return o.toString();
    }

    private String htmlUsers() {
        StringBuilder o = new StringBuilder();
        o.append("<h2>Users</h2>");
        o.append("<table>");
        o.append(tableHeader("ID", "Creation", "UserName", "Password", "Role", "First Name", "LastName", "Birth Date",
                "MRN", "Followers", "Following"));
        Iterable<UserEntity> users = userRepository.findAll();
        for (UserEntity user : users) {
            UserDetailsEntity ud = user.getUserDetailsEntity();
            StringBuilder fBuilder = new StringBuilder();
            List<UserEntity> followers = user._getFollowers();
            for (UserEntity follower : followers) {
                fBuilder.append(follower.getUserName());
            }
            String stringFollowers = fBuilder.toString();
            fBuilder.setLength(0);
            List<UserEntity> following = user.getFollowedTeens();
            for (UserEntity followee : following) {
                fBuilder.append(followee.getUserName());
            }
            String stringFollowing = fBuilder.toString();
            o.append(tableRow("" + user.getId(), dateFormat.format(user.getModificationDate()), user.getUserName(),
                    user.getPassword(), user.getRoles().toString(), ud.getFirstName(), ud.getLastName(),
                    dateFormat.format(ud.getBirthDate()), "" + ud.getMedicalRecordNumber(), stringFollowers,
                    stringFollowing));
        }
        o.append("</table>");
        return o.toString();
    }

    private String css() {
        String css = "table, td, th {border: 1px solid green;} th{background-color: #90EE90; color: blue;}";
        return "<style>"+css+"</style>";
    }

    private String tableHeader(String... headers) {
        StringBuilder o = new StringBuilder();
        o.append("<tr>");
        for (String header : headers) {
            o.append("<th>" + header + "</th>");
        }
        o.append("</tr>");
        return o.toString();
    }

    private String tableRow(String... columns) {
        StringBuilder o = new StringBuilder();
        o.append("<tr>");
        for (String col : columns) {
            o.append("<td>" + col + "</td>");
        }
        o.append("</tr>");
        return o.toString();
    }

    private <T> String outputList(Iterable<T> list) {
        StringBuilder outAll = new StringBuilder();
        outAll.append(dateFormat.format(new Date())).append("<br>");
        for (T item : list) {
            outAll.append(item.toString()).append("<br>");
        }
        return outAll.toString();
    }

}
