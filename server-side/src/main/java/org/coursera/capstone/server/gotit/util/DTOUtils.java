package org.coursera.capstone.server.gotit.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.coursera.capstone.server.gotit.api.Question;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.api.UserDetails;
import org.coursera.capstone.server.gotit.repository.QuestionEntity;
import org.coursera.capstone.server.gotit.repository.UserEntity;

public class DTOUtils {

    private DTOUtils() {
    }

    public static List<User> transferUsers(Iterable<UserEntity> userEntities) throws IllegalAccessException,
            InvocationTargetException {
        List<User> users = new ArrayList<>();
        for (UserEntity userEntity : userEntities) {
            users.add(transferUser(userEntity));
        }
        return users;
    }

    public static User transferUser(UserEntity userEntity) throws IllegalAccessException, InvocationTargetException {
        User user = new User();
        BeanUtils.copyProperties(user, userEntity);
        user.setPassword(null);// no password will be sent back;
        UserDetails teenDetails = new UserDetails();
        BeanUtils.copyProperties(teenDetails, userEntity.getUserDetailsEntity());
        user.setUserDetails(teenDetails);
        return user;

    }

    public static List<Question> transferQuestions(Iterable<QuestionEntity> questionEntities)
            throws IllegalAccessException, InvocationTargetException {
        List<Question> questions = new ArrayList<Question>();
        for (QuestionEntity questionEntity : questionEntities) {
            Question question = new Question();
            BeanUtils.copyProperties(question, questionEntity);
            questions.add(question);
        }
        return questions;
    }

}
