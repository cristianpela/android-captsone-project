package org.coursera.capstone.server.gotit.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.CheckIn;
import org.coursera.capstone.server.gotit.api.CheckInRequest;
import org.coursera.capstone.server.gotit.api.Feedback;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.repository.CheckInEntity;
import org.coursera.capstone.server.gotit.repository.CheckInRepository;
import org.coursera.capstone.server.gotit.repository.FeedbackEntity;
import org.coursera.capstone.server.gotit.repository.QuestionEntity;
import org.coursera.capstone.server.gotit.repository.QuestionRepository;
import org.coursera.capstone.server.gotit.repository.UserDetailsEntity;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.coursera.capstone.server.gotit.util.DTOUtils;
import org.coursera.capstone.server.gotit.util.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckInService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private CheckInRepository checkInRepository;

    public CheckInService() {

    }

    @Transactional
    public void requestCheckIn(String teenUserName, String principalFollowerUserName) {
        UserEntity teenEntity = userRepository.findByUserName(teenUserName);
        UserEntity followerEntity = userRepository.findByUserName(principalFollowerUserName);
        teenEntity.getCheckInRequests().add(followerEntity);
        userRepository.save(teenEntity);
    }

    @Transactional
    public CheckInRequest getCheckInRequests(String principalTeenUserName) {
        UserEntity teenEntity = userRepository.findByUserName(principalTeenUserName);
        List<UserEntity> checkInRequestsEntities = teenEntity.getCheckInRequests();
        CheckInRequest checkInRequests = new CheckInRequest(checkInRequestsEntities.size(), new ArrayList<String>());
        for (UserEntity chcke : checkInRequestsEntities) {
            UserDetailsEntity userDetailsEntity = chcke.getUserDetailsEntity();
            checkInRequests.getFrom().add(userDetailsEntity.getFirstName() + " " + userDetailsEntity.getLastName());
        }
        return checkInRequests;
    }

    @Transactional
    public void acknowledgeCheckInRequests(String principalTeenUserName) {
        UserEntity teenEntity = userRepository.findByUserName(principalTeenUserName);
        teenEntity.getCheckInRequests().clear();
        userRepository.save(teenEntity);
    }

    @Transactional
    public long postCheckIn(CheckIn checkIn, String principalTeenUserName) {
        UserEntity teenEntity = userRepository.findByUserName(principalTeenUserName);
        CheckInEntity checkInEntity = new CheckInEntity();
        checkInEntity.setAuthor(teenEntity);
        //checkInEntity.setCreationDate(checkIn.getCreationDate());
        checkInEntity.setShareable(checkIn.isShareable());
        transferSharedWithToDAO(checkIn.getSharedWith(), checkInEntity.getSharedWith());
        for (Feedback feedback : checkIn.getFeedback()) {
            FeedbackEntity feedbackEntity = new FeedbackEntity();
            QuestionEntity questionEntity = questionRepository.findOne(feedback.getQuestionId());
            feedbackEntity.setQuestion(questionEntity);
            feedbackEntity.setAnswer(feedback.getAnswer());
            feedbackEntity.setFeedbackType(feedback.getFeedbackType());
            transferSharedWithToDAO(feedback.getSharedWith(), feedbackEntity.getSharedWith());
            checkInEntity.getFeedback().add(feedbackEntity);
            // feedbackEntity.setCheckIn(checkInEntity);
        }
     
        return checkInRepository.save(checkInEntity).getId();
    }

    @Transactional
    public List<CheckIn> getTeenCheckInHistory(long newerThanDate, String teenUserName, String principalFollowerUserName)
            throws IllegalAccessException, InvocationTargetException {
        boolean isTeenUserNameCurrentPrincipal = teenUserName.equals(principalFollowerUserName);
        UserEntity teenEntity = userRepository.findByUserName(teenUserName);
        List<CheckIn> checkIns = new ArrayList<CheckIn>();
        List<CheckInEntity> checkInEntities = checkInRepository.findByAuthorAndLaterThanDate(teenEntity, new Date(
                newerThanDate));
        transferCheckIn(checkIns, checkInEntities, principalFollowerUserName, isTeenUserNameCurrentPrincipal);
        return checkIns;
    }

    private void transferCheckIn(List<CheckIn> checkIns, List<CheckInEntity> checkInEntities,
            String sharedWithFollowerUserName, boolean isTeenUserNameCurrentPrincipal) throws IllegalAccessException,
            InvocationTargetException {
        //UserEntity sharedWithFollowerEntity = userRepository.findByUserName(sharedWithFollowerUserName);
        //User sharedWithFollowerUser = DTOUtils.transferUser(sharedWithFollowerEntity);
        for (CheckInEntity checkInEntity : checkInEntities) {
            List<UserEntity> checkInEntitySharedWith = checkInEntity.getSharedWith();
            if (isTeenUserNameCurrentPrincipal
                    || isSharedWithFollower(checkInEntitySharedWith, sharedWithFollowerUserName)) {
                CheckIn checkIn = new CheckIn();
                checkIn.setCreationDate(checkInEntity.getModificationDate());
                checkIn.setAuthor(checkInEntity.getAuthor().getUserName());
                if (isTeenUserNameCurrentPrincipal)
                    checkIn.getSharedWith().addAll(getSharedFollowerNames(checkInEntity.getSharedWith()));
                else
                    checkIn.getSharedWith().add(sharedWithFollowerUserName);

                List<FeedbackEntity> feedbackEntities = checkInEntity.getFeedback();
                for (FeedbackEntity feedbackEntity : feedbackEntities) {
                    List<UserEntity> feedbackEntitySharedWith = feedbackEntity.getSharedWith();
                    if (isTeenUserNameCurrentPrincipal
                            || isSharedWithFollower(feedbackEntitySharedWith, sharedWithFollowerUserName)) {
                        Feedback feedback = new Feedback();
                        feedback.setQuestionId(feedbackEntity.getQuestion().getId());
                        feedback.setAnswer(feedbackEntity.getAnswer());
                        feedback.setFeedbackType(feedbackEntity.getFeedbackType());
                        if (isTeenUserNameCurrentPrincipal)
                            feedback.getSharedWith().addAll(getSharedFollowerNames(feedbackEntity.getSharedWith()));
                        else
                            feedback.getSharedWith().add(sharedWithFollowerUserName);
                        checkIn.getFeedback().add(feedback);
                    }
                }
                checkIns.add(checkIn);
            }
        }
    }
    
    private List<String> getSharedFollowerNames(List<UserEntity> sharedWith){
        List<String> sharedFollowerUserNames = new ArrayList<String>();
        for (UserEntity userEntity : sharedWith) {
            sharedFollowerUserNames.add(userEntity.getUserName());
        }
        return sharedFollowerUserNames;
    }

    private boolean isSharedWithFollower(List<UserEntity> sharedWith, String sharedWithFollower) {
        for (UserEntity userEntity : sharedWith) {
            if (userEntity.getUserName().equals(sharedWithFollower)) {
                return true;
            }
        }
        return false;
    }

    private void transferSharedWithToDAO(List<String> sharedWithUserNames, List<UserEntity> sharedWith) {
        for (String sharedWithUserName : sharedWithUserNames) {
            UserEntity followerEntity = userRepository.findByUserName(sharedWithUserName);
            sharedWith.add(followerEntity);
        }
    }
}
