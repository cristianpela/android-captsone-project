package org.coursera.capstone.server.gotit.api;

import java.util.Date;

public class Gift {
    
    private String followerUserName;

    private String followerFullName;

    private String photoFileName;

    private Date date;

    public Gift() {
    }

    public Gift(String followerUserName, String followerFullName, Date giftDate, String giftFile) {
        this.followerFullName = followerFullName;
        this.followerUserName = followerUserName;
        date = giftDate;
        photoFileName = giftFile;
    }

    public String getFollowerUserName() {
        return followerUserName;
    }

    public void setFollowerUserName(String followerUserName) {
        this.followerUserName = followerUserName;
    }

    public String getFollowerFullName() {
        return followerFullName;
    }

    public void setFollowerFullName(String followerFullName) {
        this.followerFullName = followerFullName;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(String photoFileName) {
        this.photoFileName = photoFileName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Gift [followerUserName=" + followerUserName + ", followerFullName=" + followerFullName
                + ", photoFileName=" + photoFileName + ", date=" + date + "]";
    }
}
