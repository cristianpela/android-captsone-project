package org.coursera.capstone.server.gotit.api;

import java.util.Date;

public class UserDetails {

    private String firstName;

    private String lastName;

    private Date birthDate;

    private long medicalRecordNumber;

    public UserDetails() {
    }

    public UserDetails(String firstName, String lastName, Date birthDate, long medicalRecordNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public long getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(long medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }


    @Override
    public String toString() {
        return "UserDetails [firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate
                + ", medicalRecordNumber=" + medicalRecordNumber + "]";
    }

}
