package org.coursera.capstone.server.gotit.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.repository.UserDetailsEntity;
import org.coursera.capstone.server.gotit.repository.UserDetailsRepository;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.coursera.capstone.server.gotit.util.DTOUtils;
import org.coursera.capstone.server.gotit.util.RepositoryUtils;
import org.coursera.capstone.server.gotit.util.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FollowerService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserDetailsRepository userDetailsRepository;

    public FollowerService() {
    }
    
    @Transactional
    public List<User> findAllTeens(String exceptCurrentUserName) throws IllegalAccessException,
            InvocationTargetException {
        List<UserEntity> teenEntities = userRepository.findAllTeens(exceptCurrentUserName);
        return DTOUtils.transferUsers(teenEntities);
    }

    @Transactional
    public List<User> findAllTeensLike(String nameCriteria, String exceptPrincipalUserName)
            throws IllegalAccessException, InvocationTargetException {
        Set<UserEntity> joinedQueries = new HashSet<UserEntity>();

        List<UserEntity> allLike = userRepository.findAllTeensLike(nameCriteria.toUpperCase(), exceptPrincipalUserName);
        joinedQueries.addAll(allLike);

        List<UserDetailsEntity> allLikeD = userDetailsRepository.findAllTeensLike(nameCriteria.toUpperCase());
        for (UserDetailsEntity ude : allLikeD) {
            UserEntity userEntity = ude.getUser();
            if (userEntity.isTeen() && !userEntity.getUserName().equals(exceptPrincipalUserName))
                joinedQueries.add(userEntity);
        }
        return DTOUtils.transferUsers(joinedQueries);
    }

    @Transactional
    public void requestForFollowing(String teenUserName, String principalFollowerUserName) {
        UserEntity teenEntity = userRepository.findByUserName(teenUserName);
        UserEntity followerEntity = userRepository.findByUserName(principalFollowerUserName);
        teenEntity.getPendingFollowers().add(followerEntity);
        followerEntity.getPendingFollowedTeens().add(teenEntity);
        RepositoryUtils.saveAll(userRepository, teenEntity, followerEntity);
    }

    @Transactional
    public List<User> getFollowedTeens(String principalFollowerUserName) throws IllegalAccessException,
            InvocationTargetException {
        UserEntity follower = userRepository.findByUserName(principalFollowerUserName);
        return DTOUtils.transferUsers(follower.getFollowedTeens());
    }

    @Transactional
    public List<User> getPendingFollowedTeens(String principalFollowerUserName) throws IllegalAccessException,
            InvocationTargetException {
        UserEntity follower = userRepository.findByUserName(principalFollowerUserName);
        return DTOUtils.transferUsers(follower.getPendingFollowedTeens());
    }
    
    public void postGiftPhoto(InputStream data, String teenUserName, String principalFollowerUserName)
            throws IOException {
        Resource.createGiftAccessResource(teenUserName, Resource.createGiftFileName(principalFollowerUserName)).save(data);
    }
}
