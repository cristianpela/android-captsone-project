package org.coursera.capstone.server.gotit.auth;

import java.util.Set;

import org.coursera.capstone.server.gotit.repository.RoleEntity;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByUserName(username);
		Set<RoleEntity> roles = userEntity.getRoles();
		return User.create(userEntity.getUserName(), userEntity.getPassword(),
				getAuthorities(roles));
	}

	private String[] getAuthorities(Set<RoleEntity> roles) {
		String[] authorities = new String[roles.size()];
		int index = 0;
		for (RoleEntity r : roles) {
			authorities[index++] = r.getType().toString();
		}
		return authorities;
	}
}
