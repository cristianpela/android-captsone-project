package org.coursera.capstone.server.gotit.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CHECK_IN")
@AttributeOverride(name = "id", column = @Column(name = "CHECK_IN_ID"))
public class CheckInEntity extends BaseEntity {
    
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private UserEntity author;

    @ManyToMany (fetch = FetchType.LAZY)
    @JoinTable(name = "CHECK_IN_SHARED_WITH", joinColumns ={ @JoinColumn(name = "CHECK_IN_ID", referencedColumnName ="CHECK_IN_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName ="USER_ID")})
    private List<UserEntity> sharedWith = new ArrayList<UserEntity>();

    @Column(name = "SHAREABLE")
    private boolean shareable;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "FEEDBACK_BR_ID")
    private List<FeedbackEntity> feedback = new ArrayList<FeedbackEntity>();

    public CheckInEntity() {
    }

    public CheckInEntity(UserEntity author, boolean shareable) {
        this.author = author;
        this.shareable = shareable;
    }
  
    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public boolean isShareable() {
        return shareable;
    }

    public void setShareable(boolean shareable) {
        this.shareable = shareable;
    }

    public List<FeedbackEntity> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<FeedbackEntity> feedback) {
        this.feedback = feedback;
    }

    public List<UserEntity> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(List<UserEntity> sharedWith) {
        this.sharedWith = sharedWith;
    }

    @Override
    public String toString() {
        List<String> shared = new ArrayList<String>();
        for(UserEntity ue : sharedWith){
            shared.add(ue.getUserName());
        }
        return "CheckInEntity [creationDate=" + getModificationDate() + ", author=" + author.getUserName() + ", sharedWith=" + shared
                + ", shareable=" + shareable + ", feedback=" + feedback + "]";
    }
}
