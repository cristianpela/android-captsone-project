package org.coursera.capstone.server.gotit.repository;

public class IntervalFeedbackBuilder {

    public enum Enclosing {
        EXCLUDED, INCLUDED;
    }

    private int lowerBoundsCount, upperBoundsCount;
    private int lowerBoundValue, upperBoundValue;
    private Enclosing lowerEnclosing, upperEnclosing;

    public IntervalFeedbackBuilder() {
        lowerBoundsCount = upperBoundsCount = 0;
    }

    public IntervalFeedbackBuilder lowerBound(int value, Enclosing enclosing) {
        lowerBoundValue = value;
        lowerEnclosing = enclosing;
        checkValidity(++lowerBoundsCount);
        return this;
    }

    public IntervalFeedbackBuilder upperBound(int value, Enclosing enclosing) {
        upperBoundValue = value;
        upperEnclosing = enclosing;
        checkValidity(++upperBoundsCount);
        return this;
    }

    private void checkValidity(int boundsCount) {
        if (boundsCount > 1 || (lowerBoundsCount + upperBoundsCount > 2))
            throw new IntervalFeedbackException("Only 2 bounds are allowed (low and high)");
        if ((lowerBoundsCount + upperBoundsCount == 2) && (upperBoundValue <= lowerBoundValue))
            throw new IntervalFeedbackException("Upperbound must be higher than lowerbound");
    }

    public String build() {
        if (lowerBoundsCount + upperBoundsCount < 2)
            throw new IntervalFeedbackException("Interval must have a low and upper bound");
        String lowerEncl = (lowerEnclosing == Enclosing.EXCLUDED) ? "(" : "[";
        String upperEncl = (upperEnclosing == Enclosing.EXCLUDED) ? ")" : "]";
        return lowerEncl + lowerBoundValue + "-" + upperBoundValue + upperEncl;
    }

    public static String concat(IntervalFeedbackBuilder... builders) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < builders.length; i++) {
            stringBuilder.append(builders[i].build() + ((i < builders.length - 1) ? ";" : ""));
        }
        return stringBuilder.toString();
    }

    public static class IntervalFeedbackException extends RuntimeException {
        private static final long serialVersionUID = 969609666649364354L;

        public IntervalFeedbackException() {
            super();
        }

        public IntervalFeedbackException(String arg0) {
            super(arg0);
        }

    }
}
