package org.coursera.capstone.server.gotit.controller;

import static org.coursera.capstone.server.gotit.api.GotItURLMapping.*;

import java.io.IOException;
import java.security.Principal;

import org.coursera.capstone.server.gotit.api.CheckIn;
import org.coursera.capstone.server.gotit.api.CheckInRequest;
import org.coursera.capstone.server.gotit.repository.RoleType;
import org.coursera.capstone.server.gotit.service.CheckInService;
import org.coursera.capstone.server.gotit.service.TeenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@PreAuthorize(RoleType.hasTeenRoleExpression)
public class CheckInController {

    @Autowired
    private CheckInService checkInService;

    public CheckInController() {
    }

    @RequestMapping(ACKNOWLEDGE_CHECK_IN_REQUESTS_PATH)
    public void acknowledgeCheckInRequests(Principal principal) {
        checkInService.acknowledgeCheckInRequests(principal.getName());
    }

    @RequestMapping(GET_CHECK_IN_REQUESTS_PATH)
    public CheckInRequest getCheckInRequests(Principal principal) {
        return checkInService.getCheckInRequests(principal.getName());
    }

    @RequestMapping(value = POST_CHECK_IN_PATH, method = RequestMethod.POST)
    public long postCheckIn(@RequestBody CheckIn checkIn, Principal principal) {
        return checkInService.postCheckIn(checkIn, principal.getName());
    }
 
}
