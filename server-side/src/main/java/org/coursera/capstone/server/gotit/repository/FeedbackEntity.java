package org.coursera.capstone.server.gotit.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.coursera.capstone.server.gotit.api.FeedbackType;

@Entity
@Table(name = "FEEDBACK")
@AttributeOverride(name = "id", column = @Column(name = "FEEDBACK_ID"))
public class FeedbackEntity extends BaseEntity {

    @Column(name = "IS_SHARED")
    private boolean shareable;

    @ManyToOne
    @JoinColumn(name = "CHECK_IN_ID")
    private CheckInEntity checkIn;

    @ManyToOne
    @JoinColumn(name = "QUESTION_ID")
    private QuestionEntity question;

    @Column(name = "ANSWER")
    private String answer;

    // positive or negative
    @Enumerated
    @Column(name = "TYPE")
    private FeedbackType type;

    @ManyToMany (fetch = FetchType.LAZY)
    @JoinTable(name = "FEEDBACK_SHARED_WITH", joinColumns ={ @JoinColumn(name = "FEEDBACK_ID", referencedColumnName ="FEEDBACK_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName ="USER_ID")})
    private List<UserEntity> sharedWith = new ArrayList<UserEntity>();

    public FeedbackEntity() {
    }

    public boolean isShareable() {
        return shareable;
    }

    public void setShareable(boolean shared) {
        this.shareable = shared;
    }

    public void setQuestion(QuestionEntity question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public FeedbackType getFeedbackType() {
        return type;
    }

    public void setFeedbackType(FeedbackType type) {
        this.type = type;
    }

    public void setCheckIn(CheckInEntity checkIn) {
        this.checkIn = checkIn;
    }

    public CheckInEntity getCheckIn() {
        return checkIn;
    }

    public QuestionEntity getQuestion() {
        return question;
    }

    public List<UserEntity> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(List<UserEntity> sharedWith) {
        this.sharedWith = sharedWith;
    }

    @Override
    public String toString() {
        List<String> shared = new ArrayList<String>();
        for(UserEntity ue : sharedWith){
            shared.add(ue.getUserName());
        }
        return "FeedbackEntity [shareable=" + shareable +", question=" + question
                + ", answer=" + answer + ", type=" + type + ", sharedWith=" + shared + "]";
    }

    
}
