package org.coursera.capstone.server.gotit.repository;

import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<RoleEntity, Long> {

    public RoleEntity findByType(RoleType type);
    
}
