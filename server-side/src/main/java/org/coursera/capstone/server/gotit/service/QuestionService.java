package org.coursera.capstone.server.gotit.service;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.Question;
import org.coursera.capstone.server.gotit.repository.QuestionEntity;
import org.coursera.capstone.server.gotit.repository.QuestionRepository;
import org.coursera.capstone.server.gotit.util.DTOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;
    
    public QuestionService() {
    }
    
    @Transactional
    public List<Question> getAllQuestions(long afterLongDate) throws IllegalAccessException, InvocationTargetException {
        Iterable<QuestionEntity> questionEntities = questionRepository.findByModificationDateAfter(new Date(afterLongDate));
        return DTOUtils.transferQuestions(questionEntities);
    }
    

}
