package org.coursera.capstone.server.gotit.controller;

import static org.coursera.capstone.server.gotit.api.GotItURLMapping.*;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.List;

import org.coursera.capstone.server.gotit.api.CheckIn;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.service.CheckInService;
import org.coursera.capstone.server.gotit.service.FollowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FollowerController {

    @Autowired
    private FollowerService followerService;

    @Autowired
    private CheckInService checkInService;

    public FollowerController() {
    }

    @RequestMapping(FIND_ALL_TEENS_PATH)
    public List<User> findAllTeens(Principal principal) throws IllegalAccessException, InvocationTargetException {
        return followerService.findAllTeens(principal.getName());
    }

    @RequestMapping(FIND_ALL_TEENS_LIKE_PATH)
    public List<User> findAllTeensLike(@PathVariable(TEEN_LIKE_CRITERIA_PARAMETER) String nameCriteria,
            Principal principal) throws IllegalAccessException, InvocationTargetException {
        return followerService.findAllTeensLike(nameCriteria, principal.getName());
    }

    @RequestMapping(REQUEST_FOR_FOLLOWING_PATH)
    public void requestForFollowingTeen(@PathVariable(USER_NAME_PARAMETER) String teenUserName, Principal principal) {
        followerService.requestForFollowing(teenUserName, principal.getName());
    }

    @RequestMapping(GET_ALL_FOLLOWED_TEENS_PATH)
    public List<User> getFollowedTeens(Principal principal) throws IllegalAccessException, InvocationTargetException {
        return followerService.getFollowedTeens(principal.getName());
    }

    @RequestMapping(GET_ALL_PENDING_FOLLOWED_TEENS_PATH)
    public List<User> getPendingFollowedTeens(Principal principal) throws IllegalAccessException,
            InvocationTargetException {
        return followerService.getPendingFollowedTeens(principal.getName());
    }

    @RequestMapping(REQUEST_FOR_CHECK_IN_PATH)
    public void requestCheckIn(@PathVariable(USER_NAME_PARAMETER) String teenUserName, Principal principal) {
        checkInService.requestCheckIn(teenUserName, principal.getName());
    }

    @RequestMapping(value = GET_TEEN_CHECK_IN_HISTORY_PATH)
    public List<CheckIn> getTeenCheckInHistory(@PathVariable(CHECK_IN_DATE_PARAMETER) long newerThanDateTime,
            @PathVariable(USER_NAME_PARAMETER) String teenUserName, Principal principal) throws IllegalAccessException, InvocationTargetException {
        return checkInService.getTeenCheckInHistory(newerThanDateTime, teenUserName, principal.getName());
    }

    @RequestMapping(value = POST_GIFT_PHOTO_PATH, method = RequestMethod.POST)
    public void postGiftPhotoData(@PathVariable(USER_NAME_PARAMETER) String teenUserName,
            @RequestParam(DATA_PARAMETER) MultipartFile file, Principal principal) throws IOException {
        followerService.postGiftPhoto(file.getInputStream(), teenUserName, principal.getName());
    }
}
