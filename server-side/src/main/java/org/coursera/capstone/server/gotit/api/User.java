package org.coursera.capstone.server.gotit.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {

    private long id = -1;

    private String userName;

    private String password;

    private boolean teen;

    private UserDetails userDetails;

    private Date recentFeedbackDate;

    private Date modificationDate;

    private List<String> followers = new ArrayList<String>();
    
    private List<String> following = new ArrayList<String>();

    private List<String> currentPendingFollowers = new ArrayList<String>();

    public User() {
    }

    public User(boolean teen, String name, String password, UserDetails userDetails) {
        this.teen = teen;
        this.userName = name;
        this.password = password;
        this.userDetails = userDetails;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String name) {
        this.userName = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public boolean isTeen() {
        return teen;
    }

    public void setTeen(boolean teen) {
        this.teen = teen;
    }

    public Date getRecentFeedbackDate() {
        return recentFeedbackDate;
    }

    public void setRecentFeedbackDate(Date recentFeedbackDate) {
        this.recentFeedbackDate = recentFeedbackDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public List<String> getFollowing() {
        return following;
    }

    public void setFollowing(List<String> following) {
        this.following = following;
    }

    public List<String> getFollowers() {
        return followers;
    }

    public void setFollowers(List<String> followers) {
        this.followers = followers;
    }
    
    public List<String> getCurrentPendingFollowers() {
        return currentPendingFollowers ;
    }

    public void setCurrentPendingFollowers(List<String> currentPendingFollowers) {
        this.currentPendingFollowers = currentPendingFollowers;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", name=" + userName + ", password=" + password + "\n]";
    }

}
