package org.coursera.capstone.server.gotit.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Resource {

    public static final String AVATAR_DIR = "avatar";
    public static final String GIFT_DIR = "gift";

    public static class FileAccessStrategy {

        private String fileKey;
        private String directoryKey;

        public FileAccessStrategy(String fileKey, String directoryKey) {
            this.fileKey = fileKey;
            this.directoryKey = directoryKey;
        }

        public Path directory() {
            return Paths.get(directoryKey);
        }

        /**
         * The file name is generated after a unique key
         * 
         * @return generated file name
         */
        public String fileName() {
            return fileKey + ".png";
        }

    }

    private static final Path root = Paths.get("src", "main", "resources", "img");

    private Path userPath, targetPath, filePath;

    public Resource(String userDirectory, FileAccessStrategy fileAccess) throws IOException {
        userPath = root.resolve(Paths.get(userDirectory));
        if (!Files.exists(userPath)) {
            Files.createDirectories(userPath);
        }
        targetPath = userPath.resolve(fileAccess.directory());
        if (!Files.exists(targetPath)) {
            Files.createDirectories(targetPath);
        }
        filePath = targetPath.resolve(fileAccess.fileName());
    }

    public void save(InputStream data) throws IOException {
        Files.copy(data, filePath, StandardCopyOption.REPLACE_EXISTING);
    }

    public byte[] load() throws IOException {
        return Files.readAllBytes(filePath);
    }

    public void delete() throws IOException {
        Files.delete(filePath);
    }

    public static Resource createAvatarAccessResource(String userName) throws IOException {
        return new Resource(userName, new FileAccessStrategy(userName, AVATAR_DIR));
    }

    public static Resource createGiftAccessResource(String teenUserName, String giftFileName) throws IOException {
        return new Resource(teenUserName, new FileAccessStrategy(giftFileName, GIFT_DIR));
    }

    public static String createGiftFileName(String followerUserName) {
        return followerUserName + "_" + new Date().getTime();
    }

    public static String[] getAllGifts(String teenUserName) throws IOException {
        File giftDir = root.resolve(Paths.get(teenUserName, GIFT_DIR)).toFile();
        if (giftDir.exists()) {
            String[] giftFilesName = giftDir.list();
            Arrays.sort(giftFilesName);
            return giftFilesName;
        } else
            throw new IOException("Gift directory not found");
    }
}
