package org.coursera.capstone.server.gotit.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Feedback {

    private long questionId;

    private FeedbackType feedbackType;

    private String answer;

    private List<String> sharedWith = new ArrayList<String>();

    public Feedback() {

    }

    public Feedback(Answer constructedAnswer) {
        this.answer = constructedAnswer.getBody();
        this.feedbackType = constructedAnswer.getFeedbackType();
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    @JsonDeserialize(using = FeedbackType.Deserializer.class)
    public FeedbackType getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(FeedbackType feedbackType) {
        this.feedbackType = feedbackType;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public List<String> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(List<String> sharedWith) {
        this.sharedWith = sharedWith;
    }

    @Override
    public String toString() {
        return "Feedback [questionId=" + questionId + ", feedbackType=" + feedbackType + ", answer=" + answer
                + ", sharedWith=" + sharedWith + "]\n";
    }

}
