package org.coursera.capstone.server.gotit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    //@Query("SELECT u FROM UserEntity u WHERE u.userName=?1")
    public UserEntity findByUserName(String userName);

    @Query("SELECT u FROM UserEntity u WHERE u.teen=true AND NOT u.userName=?1")
    public List<UserEntity> findAllTeens(String exceptUserName);

    //@Query("SELECT u FROM UserEntity u, UserDetailsEntity ud WHERE u.teen=true AND NOT u.userName=?2 AND u.userName LIKE %?1% OR ud.firstName LIKE %?1% OR ud.lastName LIKE %?1%")
    @Query("SELECT u FROM UserEntity u WHERE u.teen=true AND NOT u.userName=?2 AND UPPER(u.userName) LIKE %?1%")
    //@Query(value = "SELECT * FROM USER u, USER_DETAILS ud WHERE u.is_teen = TRUE AND NOT u.user_name=?2 AND u.user_id = ud.user_id"
      //      + " AND u.user_name LIKE %?1% AND ud.first_name LIKE %?1% "/*OR ud.last_name LIKE %?1%"*/, nativeQuery = true)
    public List<UserEntity> findAllTeensLike(String nameCriteria, String exceptCurrentUserName);

}
