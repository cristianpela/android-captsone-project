package org.coursera.capstone.server.gotit.repository;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "ROLE")
@AttributeOverride(name = "id", column = @Column(name = "ROLE_ID"))
public class RoleEntity extends BaseEntity {

    @Enumerated
	@Column(name = "TYPE")
	private RoleType type;

	public RoleEntity() {
		super();
	}

	public RoleEntity(RoleType type) {
		this.type = type;
	}

	public RoleType getType() {
		return type;
	}

	public void setType(RoleType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Role [type=" + type + "]";
	}
}
