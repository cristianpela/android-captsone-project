package org.coursera.capstone.server.gotit.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CheckInRepository extends CrudRepository<CheckInEntity, Long> {

    public List<CheckInEntity> findByAuthor(UserEntity author);
    
    @Query("SELECT ci from CheckInEntity AS ci WHERE ci.author =?1 AND ci.modificationDate > ?2")
    public List<CheckInEntity> findByAuthorAndLaterThanDate(UserEntity author, Date creationDate);
}
