package org.coursera.capstone.server.gotit.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckIn {
    
    private Date creationDate;
    
    private boolean shareable;
    
    private List<String> sharedWith;
    
    private List<Feedback> feedback;
    
    private String author;
    
    public CheckIn(){
        sharedWith = new ArrayList<String>();
        feedback = new ArrayList<Feedback>();
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<String> getSharedWith() {
        return sharedWith;
    }

    public void setSharedWith(List<String> sharedWith) {
        this.sharedWith = sharedWith;
    }

    public List<Feedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<Feedback> feedback) {
        this.feedback = feedback;
    }
    
    public boolean isShareable() {
        return shareable;
    }

    public void setShareable(boolean shareable) {
        this.shareable = shareable;
    }
   
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "CheckIn [creationDate=" + creationDate + ", sharedWith=" + sharedWith + ", feedback=\n" + feedback + "]\n";
    }
  
}
