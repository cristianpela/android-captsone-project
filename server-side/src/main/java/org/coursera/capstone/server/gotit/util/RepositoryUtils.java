package org.coursera.capstone.server.gotit.util;

import java.util.Arrays;

import org.coursera.capstone.server.gotit.repository.BaseEntity;
import org.springframework.data.repository.CrudRepository;

public class RepositoryUtils {

    private RepositoryUtils() {
    }

    @SuppressWarnings({ "unchecked" })
    public static void saveAll(@SuppressWarnings("rawtypes") CrudRepository repo, BaseEntity... entities) {
        repo.save(Arrays.asList(entities));
    }
}
