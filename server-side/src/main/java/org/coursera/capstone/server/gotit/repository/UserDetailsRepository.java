package org.coursera.capstone.server.gotit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserDetailsRepository extends
		CrudRepository<UserDetailsEntity, Long> {

	//@Query(value = "SELECT ud.* FROM USER_DETAILS ud, FOLLOWER f WHERE ud.USER_DETAIL_ID=f.FOLLOWER_ID AND f.FOLLOWER_ID=?1", nativeQuery = true)
	//public List<UserDetailsEntity> findFollowedTeens(long id);
    
    @Query("SELECT ud FROM UserDetailsEntity ud WHERE UPPER(ud.firstName) LIKE %?1% OR UPPER(ud.lastName) LIKE %?1%")
    public List<UserDetailsEntity> findAllTeensLike(String nameCriteria);
    
}
