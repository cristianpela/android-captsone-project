package org.coursera.capstone.server.gotit.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.coursera.capstone.server.gotit.api.Gift;
import org.coursera.capstone.server.gotit.api.Notification;
import org.coursera.capstone.server.gotit.api.NotificationType;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.repository.UserDetailsEntity;
import org.coursera.capstone.server.gotit.repository.UserDetailsRepository;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.coursera.capstone.server.gotit.util.DTOUtils;
import org.coursera.capstone.server.gotit.util.RepositoryUtils;
import org.coursera.capstone.server.gotit.util.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeenService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    public TeenService() {
    }

    @Transactional
    public void confirmFollower(String principalTeenUserName, String followerUserName) {
        UserEntity teenEntity = userRepository.findByUserName(principalTeenUserName);
        UserEntity followerEntity = userRepository.findByUserName(followerUserName);
        teenEntity.getPendingFollowers().remove(followerEntity);
        followerEntity.getPendingFollowedTeens().remove(teenEntity);
        teenEntity._getFollowers().add(followerEntity);
        followerEntity.getFollowedTeens().add(teenEntity);
        RepositoryUtils.saveAll(userRepository, teenEntity, followerEntity);
    }

    @Transactional
    public List<User> getPendingFollowers(String principalTeenUserName) throws IllegalAccessException,
            InvocationTargetException {
        UserEntity teen = userRepository.findByUserName(principalTeenUserName);
        return DTOUtils.transferUsers(teen.getPendingFollowers());
    }

    @Transactional
    public List<User> getFollowers(String principalTeenUserName) throws IllegalAccessException,
            InvocationTargetException {
        UserEntity teen = userRepository.findByUserName(principalTeenUserName);
        return DTOUtils.transferUsers(teen._getFollowers());
    }

    public List<Notification> getNotifications(String principalTeenUserName) {
        UserEntity teen = userRepository.findByUserName(principalTeenUserName);
        List<Notification> notifications = new ArrayList<Notification>();
        List<UserEntity> pendingFollowers = teen.getPendingFollowers();
        for (UserEntity pf : pendingFollowers) {
            Notification notif = new Notification();
            notif.setType(NotificationType.FOLLOW_REQUEST);
            notif.setDate(pf.getModificationDate());
            notif.setFromUserName(pf.getUserName());
            notif.setFromFullName(pf.getUserDetailsEntity().getFirstName() + " "
                    + pf.getUserDetailsEntity().getLastName());
            notifications.add(notif);
        }

        List<UserEntity> checkinRequests = teen.getCheckInRequests();
        for (UserEntity cr : checkinRequests) {
            Notification notif = new Notification();
            notif.setType(NotificationType.CHECK_IN_REQUEST);
            notif.setDate(cr.getModificationDate());
            notif.setFromUserName(cr.getUserName());
            notif.setFromFullName(cr.getUserDetailsEntity().getFirstName() + " "
                    + cr.getUserDetailsEntity().getLastName());
            notifications.add(notif);
        }
        return notifications;
    }

    public byte[] getGiftPhotoData(String principalTeenUserName, String giftPhotoFileName) throws IOException {
        return Resource.createGiftAccessResource(principalTeenUserName, giftPhotoFileName).load();
    }

    @Transactional
    public List<Gift> getAllGifts(String principalTeenUserName) throws IOException {

        String[] giftFiles = Resource.getAllGifts(principalTeenUserName);

        UserEntity userEntity = null;
        String fullName = null;

        List<Gift> gifts = new ArrayList<Gift>();
        for (String giftFile : giftFiles) {
            // looks like this: <username>_<time>.<ext>
            String[] parts = giftFile.split("[_.]");
            if (userEntity == null || !parts[0].equals(userEntity.getUserName())) {
                userEntity = userRepository.findByUserName(parts[0]);
                if (userEntity != null) {
                    UserDetailsEntity userDetailsEntity = userEntity.getUserDetailsEntity();
                    fullName = userDetailsEntity.getFirstName() + " " + userDetailsEntity.getLastName();
                }
            }
            Date giftDate = new Date(Long.parseLong(parts[1]));
            gifts.add(new Gift(userEntity.getUserName(), fullName, giftDate, giftFile));
        }
        return gifts;
    }

}
