package org.coursera.capstone.server.gotit.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.Principal;
import java.util.List;

import org.apache.commons.io.filefilter.NotFileFilter;
import org.coursera.capstone.server.gotit.api.Gift;
import org.coursera.capstone.server.gotit.api.GotItURLMapping;
import org.coursera.capstone.server.gotit.api.Notification;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.repository.RoleType;
import org.coursera.capstone.server.gotit.service.TeenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.coursera.capstone.server.gotit.api.GotItURLMapping.*;

@RestController
@PreAuthorize(RoleType.hasTeenRoleExpression)
public class TeenController {

    @Autowired
    private TeenService teenService;

    public TeenController() {
    }

    @RequestMapping(CONFIRM_FOR_FOLLOWING_PATH) 
    public void confirmFollower(@PathVariable(USER_NAME_PARAMETER) String followerUserName, Principal principal){
        teenService.confirmFollower(principal.getName(), followerUserName);
    }
    
    @RequestMapping(GET_ALL_FOLLOWERS_PATH)
    public List<User> getFollowers(Principal principal) throws IllegalAccessException, InvocationTargetException{
        return teenService.getFollowers(principal.getName());
    }
    
    @RequestMapping(GET_ALL_PENDING_FOLLOWERS_PATH)
    public List<User> getPendingFollowers(Principal principal) throws IllegalAccessException, InvocationTargetException{
        return teenService.getPendingFollowers(principal.getName());
    }
    
    @RequestMapping(GotItURLMapping.GET_NOTIFICATIONS)
    public List<Notification> getNotifications(Principal principal) throws IllegalAccessException{
        return teenService.getNotifications(principal.getName());
    }
    
    @RequestMapping(GET_GIFT_PHOTO_PATH)
    public byte[] getGiftPhotoData(@PathVariable(GIFT_PHOTO_FILE_NAME_PARAMETER) String giftPhotoFileName,
            Principal principal) throws IOException {
        return teenService.getGiftPhotoData(principal.getName(), giftPhotoFileName);
    }
    
    @RequestMapping(GET_ALL_GIFT_PHOTOS_PATH)
    public List<Gift> getAllGifts(Principal principal) throws IOException{
        return teenService.getAllGifts(principal.getName());
    }
    
    
}

