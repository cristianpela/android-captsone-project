package org.coursera.capstone.server.gotit.repository;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.coursera.capstone.server.gotit.api.QuestionEnterType;
import org.coursera.capstone.server.gotit.api.QuestionGeneratedFeedbackMode;

@Entity
@Table(name = "QUESTION")
@AttributeOverride(name = "id", column = @Column(name = "QUESTION_ID"))
public class QuestionEntity extends BaseEntity{

    @Lob
	@Column(name = "BODY")
	private String body;

	@Column(name = "ENTERABLE")
	private boolean enterable;

	//type is number or string (this is for field type constraint)
	@Enumerated
	@Column(name = "ENTER_TYPE")
	private QuestionEnterType enterType;

	// manual or auto; manual means that the teen should choose if his answer is positive or negative
	@Enumerated
	@Column(name = "GEN_FEED_MODE")
	private QuestionGeneratedFeedbackMode generatedFeedbackMode;

	// default none (if is none and generatedFeedbackMode:auto then feedback is positive by default)
	@Column(name = "NEG_FEED_INTERVAL")
	private String negativeFeedbackInterval;

	// default none (if is none and generatedFeedbackMode:auto then feedback is positive by default)
	@Column(name = "POS_FEED_INTERVAL")
	private String positiveFeedbackInterval;
	
	public QuestionEntity() {
	}

	public QuestionEntity(String body, boolean enterable,
			QuestionEnterType enterType, QuestionGeneratedFeedbackMode generatedFeedbackMode,
			String negativeFeedbackInterval, String positiveFeedbackInterval) {
		this.body = body;
		this.enterable = enterable;
		this.enterType = enterType;
		this.generatedFeedbackMode = generatedFeedbackMode;
		this.negativeFeedbackInterval = negativeFeedbackInterval;
		this.positiveFeedbackInterval = positiveFeedbackInterval;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public boolean isEnterable() {
		return enterable;
	}

	public void setEnterable(boolean enterable) {
		this.enterable = enterable;
	}

	public QuestionEnterType getEnterType() {
		return enterType;
	}

	public void setEnterType(QuestionEnterType enterType) {
		this.enterType = enterType;
	}

	public QuestionGeneratedFeedbackMode getGeneratedFeedbackMode() {
		return generatedFeedbackMode;
	}

	public void setGeneratedFeedbackMode(QuestionGeneratedFeedbackMode generatedFeedbackMode) {
		this.generatedFeedbackMode = generatedFeedbackMode;
	}

	public String getNegativeFeedbackInterval() {
		return negativeFeedbackInterval;
	}

	public void setNegativeFeedbackInterval(String negativeFeedbackInterval) {
		this.negativeFeedbackInterval = negativeFeedbackInterval;
	}

	public String getPositiveFeedbackInterval() {
		return positiveFeedbackInterval;
	}

	public void setPositiveFeedbackInterval(String positiveFeedbackInterval) {
		this.positiveFeedbackInterval = positiveFeedbackInterval;
	}
	
	@Override
	public String toString() {
		return super.toString()+"\nQuestionEntity [id=" + getId() + ", body=" + body + ", enterable="
				+ enterable + ", enterType=" + enterType
				+ ", generatedFeedbackMode=" + generatedFeedbackMode
				+ ", negativeFeedbackInterval=" + negativeFeedbackInterval
				+ ", positiveFeedbackInterval=" + positiveFeedbackInterval
				+ "]";
	}
	
}
