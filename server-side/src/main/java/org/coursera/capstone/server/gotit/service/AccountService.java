package org.coursera.capstone.server.gotit.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.coursera.capstone.server.gotit.api.User;
import org.coursera.capstone.server.gotit.api.UserDetails;
import org.coursera.capstone.server.gotit.repository.RoleEntity;
import org.coursera.capstone.server.gotit.repository.RoleRepository;
import org.coursera.capstone.server.gotit.repository.RoleType;
import org.coursera.capstone.server.gotit.repository.UserDetailsEntity;
import org.coursera.capstone.server.gotit.repository.UserDetailsRepository;
import org.coursera.capstone.server.gotit.repository.UserEntity;
import org.coursera.capstone.server.gotit.repository.UserRepository;
import org.coursera.capstone.server.gotit.util.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Autowired
    private RoleRepository roleRepository;

    public AccountService() {
    }

    @Transactional
    public User signUpOrUpdate(User user) throws IllegalAccessException, InvocationTargetException {
        RoleEntity userRole = roleRepository.findByType(RoleType.FOLLOWER);
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userEntity, user);
        UserDetailsEntity userDetailsEntity = new UserDetailsEntity();
        BeanUtils.copyProperties(userDetailsEntity, user.getUserDetails());
        userEntity.setUserDetailsEntity(userDetailsEntity);
        userEntity.getUserDetailsEntity().setUser(userEntity);// bi-directional
        userEntity.getRoles().add(userRole);
        userEntity = userRepository.save(userEntity);
        // userDetailsRepository.save(userDetailsEntity);

        User signedUser = new User();
        signedUser.setId(userEntity.getId());
        signedUser.setUserName(userEntity.getUserName());
        signedUser.setPassword(userEntity.getPassword());
        signedUser.setModificationDate(userEntity.getModificationDate());
        return signedUser;
    }

    public boolean saveAvatarData(String userName, InputStream in) {
        try {
            Resource.createAvatarAccessResource(userName).save(in);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public byte[] loadAvatarData(String userName) throws IOException {
        return Resource.createAvatarAccessResource(userName).load();
    }

    public void deleteAvatarData(String userName) throws IOException {
        Resource.createAvatarAccessResource(userName).delete();
    }

    @Transactional
    public User getUserByName(String userName) throws IllegalAccessException, InvocationTargetException {
        UserEntity userEntity = userRepository.findByUserName(userName);
        return (userEntity != null) ? createRESTUSer(userEntity) : null;
    }

    private User createRESTUSer(UserEntity userEntity) throws IllegalAccessException, InvocationTargetException {
        User user = new User();
        BeanUtils.copyProperties(user, userEntity);
        for (UserEntity followerEntity : userEntity._getFollowers()) {
            user.getFollowers().add(
                    followerEntity.getUserName() + ";" + followerEntity.getUserDetailsEntity().getFirstName() + " "
                            + followerEntity.getUserDetailsEntity().getLastName());
        }
        for (UserEntity followedTeenEntity : userEntity.getFollowedTeens()) {
            user.getFollowing().add(
                    followedTeenEntity.getUserName() + ";" + followedTeenEntity.getUserDetailsEntity().getFirstName() + " "
                            + followedTeenEntity.getUserDetailsEntity().getLastName());
        }
        for(UserEntity pendingFollowerEntity : userEntity.getPendingFollowers()){
            user.getCurrentPendingFollowers().add(
                    pendingFollowerEntity.getUserName() + ";" + pendingFollowerEntity.getUserDetailsEntity().getFirstName() + " "
                            + pendingFollowerEntity.getUserDetailsEntity().getLastName());
        }
        UserDetails userDetails = new UserDetails();
        BeanUtils.copyProperties(userDetails, userEntity.getUserDetailsEntity());
        user.setUserDetails(userDetails);
        return user;
    }

}
